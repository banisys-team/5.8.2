<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function child()
    {
        return $this->hasMany(Category::class, 'parent');
    }

    public function childrenRecursive()
    {
        return $this->child()->with('childrenRecursive');
    }

    public function parent()
    {
        return $this->hasMany(Category::class, 'id','parent');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'category_product');
    }

}
