<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $guarded = [];

    public function colors()
    {
        return $this->hasMany(Color::class, 'product_id');
    }

    public function sizes()
    {
        return $this->hasMany(Size::class, 'product_id');
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class, 'product_id');
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'product_id');
    }

    public function exists()
    {
        return $this->hasMany(Exist::class, 'product_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function specifications()
    {
        return $this->hasMany(Specification::class, 'product_id');
    }

    public function suggests()
    {
        return $this->hasMany(Suggest::class, 'product_id');
    }

    public function cats()
    {
        return $this->belongsToMany(Category::class,
            'category_product','product_id','cat_id');
    }


}
