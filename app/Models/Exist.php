<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exist extends Model
{
    protected $guarded = [];

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }



}
