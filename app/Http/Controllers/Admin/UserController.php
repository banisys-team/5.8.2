<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function crud()
    {
        return view('admin.user.crud');
    }

    public function store(Request $request)
    {
        $user = User::where('mobile', $request['mobile'])->first();

        if (empty($user)) {
            User::create([
                'mobile' => $request['mobile'],
                'name' => $request['name'],
                'manually' => 1,
            ]);
        } else {
            return response()->json(0);
        }
    }

    public function fetch()
    {
        $users = User::where('manually', 1)->orderBy('created_at', 'desc')->paginate(30);

        return response()->json($users);
    }

    public function delete($user_id)
    {
        User::where('id', $user_id)->delete();
    }
}
