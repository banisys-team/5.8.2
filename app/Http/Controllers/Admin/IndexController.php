<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Index;
use App\Models\Key;
use App\Models\Slider;
use App\Models\Complaint;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class IndexController extends Controller
{
    public function showComplaint()
    {
        return view('admin.complaint.index');
    }

    public function fetchComplaint()
    {
        $complaints = Complaint::orderBy('created_at', 'desc')->paginate(50);

        return response()->json($complaints, 200);
    }

    public function showTicketComplaint($id)
    {
        $ticket = Complaint::where('id', $id)->pluck('ticket')->first();

        return response()->json($ticket, 200);
    }

    public function sliderCrud()
    {
        return view('admin.slider.create');
    }

    public function sliderStore(Request $request)
    {
        $urls = json_decode($request->urls);
        $index = 0;
        foreach ($urls as $url) {

            $random = Str::random(3);
            $imageName = $random . time() . '.' . $request->pics[$index]->getClientOriginalExtension();
            $request->pics[$index]->move(env('STORE_PATH')('images/slider'), $imageName);
            Slider::create([
                'url' => $url->url,
                'image' => $imageName
            ]);
            $index++;
        }

        return response()->json(['key' => 'value'], 200);
    }

    public function slideFetch()
    {
        $slider = Slider::get();

        return response()->json($slider);
    }

    public function sliderDelete($id)
    {
        $slide = Slider::find($id);

        $string_1 = 'images/slider/' . $slide->image;
        unlink($string_1);
        $slide->delete();

    }

    public function sliderProductStore(Request $request)
    {
        $catId = Category::where('name', $request['cat_name'])->pluck('id')->first();

        Key::where('key', $request['priority'])->update([
            'value' => $catId
        ]);

        return response()->json(1, 200);
    }

    public function sliderProductFetch()
    {
        $catId1 = Key::where('key', 'index_slider1')->pluck('value')->first();
        $catId2 = Key::where('key', 'index_slider2')->pluck('value')->first();
        $catId3 = Key::where('key', 'index_slider3')->pluck('value')->first();

        $catName1 = Category::where('id', $catId1)->pluck('name')->first();
        $catName2 = Category::where('id', $catId2)->pluck('name')->first();
        $catName3 = Category::where('id', $catId3)->pluck('name')->first();

        return response()->json([
            'slider_pro1' => $catName1,
            'slider_pro2' => $catName2,
            'slider_pro3' => $catName3,
        ]);
    }

    public function sliderProductDelete($key_id)
    {
        Key::where('key', $key_id)->update([
            'value' => null
        ]);

        return response()->json(1, 200);
    }

    public function indexCrud()
    {
        return view('admin.index.crud');
    }

}
