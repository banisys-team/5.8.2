<?php

namespace App\Http\Controllers\Admin;

use App\Models\Key;
use App\Models\Track;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    public function crud()
    {
        return view('admin.setting.crud');
    }

    public function fetch()
    {
        $name = Key::where('key', 'setting_name')->pluck('value')->first();
        $tell = Key::where('key', 'setting_tell')->pluck('value')->first();
        $mobile = Key::where('key', 'setting_mobile')->pluck('value')->first();
        $instagram = Key::where('key', 'setting_instagram')->pluck('value')->first();
        $telegram = Key::where('key', 'setting_telegram')->pluck('value')->first();
        $address = Key::where('key', 'setting_address')->pluck('value')->first();
        $about = Key::where('key', 'setting_about')->pluck('value')->first();
        $logo = Key::where('key', 'setting_logo')->pluck('value')->first();

        return response()->json([
            'name' => $name,
            'tell' => $tell,
            'mobile' => $mobile,
            'instagram' => $instagram,
            'telegram' => $telegram,
            'address' => $address,
            'about' => $about,
            'image' => $logo,
        ]);
    }

    public function store(Request $request)
    {
        $key = Key::where('key', 'setting_logo')->first();
        if (!empty($request['image'])) {
            if ($request['image'] != $key->value) {
                try {
                    unlink('images/' . $key->value);
                } catch (\Exception $e) {
                }
                $logoName = time() . '.' . $request['image']->getClientOriginalExtension();
                $request->image->move(env('STORE_PATH')('images'), $logoName);
                Key::where('key', 'setting_logo')->update(['value' => $logoName]);
            }
        }

        Key::where('key', 'setting_name')->update(['value' => $request['name']]);
        Key::where('key', 'setting_tell')->update(['value' => $request['tell']]);
        Key::where('key', 'setting_mobile')->update(['value' => $request['mobile']]);
        Key::where('key', 'setting_instagram')->update(['value' => $request['instagram']]);
        Key::where('key', 'setting_telegram')->update(['value' => $request['telegram']]);
        Key::where('key', 'setting_address')->update(['value' => $request['address']]);
        Key::where('key', 'setting_about')->update(['value' => $request['about']]);


        return response()->json(1, 200);
    }

    public function deliveryShow()
    {
        return view('admin.setting.delivery');
    }

    public function deliveryFetch()
    {
        $number = Key::where('key', 'delivery_price_num')->pluck('value')->first();
        $min = Key::where('key', 'delivery_price_min')->pluck('value')->first();
        $max = Key::where('key', 'delivery_price_max')->pluck('value')->first();

        return response()->json([
            'number' => $number,
            'min' => $min,
            'max' => $max,
        ]);
    }

    public function deliveryStore(Request $request)
    {
        Key::where('key', 'delivery_price_num')->update([
            'value' => $request['number']
        ]);

        Key::where('key', 'delivery_price_min')->update([
            'value' => $request['min']
        ]);

        Key::where('key', 'delivery_price_max')->update([
            'value' => $request['max']
        ]);
    }
}
