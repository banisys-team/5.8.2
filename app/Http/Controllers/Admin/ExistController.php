<?php

namespace App\Http\Controllers\Admin;

use App\Models\Size;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Effect_spec;
use App\Models\Effect_value;
use App\Models\Exist;
use App\Models\Product;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use App\Models\Off;
use Illuminate\Support\Facades\Log;
use App\Models\Effect_price;


class ExistController extends Controller
{
    public function index()
    {
        return view('admin.exist.index');
    }

    public function set($id)
    {
        $product = Product::find($id);

        return view('admin.exist.set', compact('product'));
    }

    public function fetchColors($id)
    {
        $colors = Color::where('product_id', $id)->get(['name', 'id']);

        return response()->json($colors);
    }

    public function fetchEffects($id)
    {
        $product = Product::find($id);

        $effects = Effect_spec::where('cat_id', $product->cat_id)->where('brand_id', $product->brand_id)
            ->with('effect_price')->get();

        return response()->json($effects);
    }

    public function getSlug($id)
    {
        $slug = Product::where('id', $id)->pluck('slug')->first();

        return response()->json($slug);
    }

    public function fetchExist($id)
    {
        $exists = Exist::where('product_id', $id)->with(['color', 'size'])->get();

        return response()->json($exists);
    }

    public function delete($id)
    {
        Exist::find($id)->delete();

        return response()->json(1, 200);
    }

    public function productCode()
    {
        return view('admin.exist.product_code');
    }

    public function productCodeSearch(Request $request)
    {
        $split = str_split($request['code'], 4);

        $product = Product::where('id', intval($split[0]))->pluck('name')->first();
        $color = Color::where('id', intval($split[1]))->pluck('name')->first();
        $size = Size::where('id', intval($split[2]))->pluck('name')->first();


        return response()->json([
            'product' => $product,
            'color' => $color,
            'size' => $size,
        ]);
    }

    public function changeNum(Request $request)
    {
        if ($request['num'] == 0) {
            Exist::where('product_code', $request['code'])->delete();
        } else {
            Exist::where('product_code', $request['code'])->update(['num' => $request['num']]);
        }

        return response()->json(1, 200);
    }

    public function fetchEffectPrice($id)
    {
        $product = Product::where('id', $id)->first();

        $effectPrice = Effect_price::where('cat_id', $product->cat_id)
            ->where('brand_id', $product->brand_id)->pluck('name')->first();


        return response()->json($effectPrice);
    }

    public function fetchEffectSpec($id)
    {
        $product = Product::where('id', $id)->first();

        $effectSpec = Effect_spec::where('cat_id', $product->cat_id)
            ->where('brand_id', $product->brand_id)->get();

        return response()->json($effectSpec);
    }

    public function storeNumWithoutSize($request)
    {
        $pid = sprintf("%'.04d", $request['product_id']);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        Exist::create([
            'product_code' => $pCode,
            'product_id' => $request['product_id'],
            'size_id' => 0,
            'color_id' => $request['color_id'],
            'num' => $request['num'],
        ]);

        return response()->json(1);
    }

    public function storeNumWithoutColor($request)
    {
        $pid = sprintf("%'.04d", $request['product_id']);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        Exist::create([
            'product_code' => $pCode,
            'product_id' => $request['product_id'],
            'size_id' => $request['size_id'],
            'color_id' => 0,
            'num' => $request['num'],
        ]);

        return response()->json(1);
    }

    public function storeNumWithoutNothing($request)
    {
        $pid = sprintf("%'.04d", $request['product_id']);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        Exist::create([
            'product_code' => $pCode,
            'product_id' => $request['product_id'],
            'size_id' => 0,
            'color_id' => 0,
            'num' => $request['num'],
        ]);

        return response()->json(1);
    }

    public function colorsFetch($product_id)
    {
        $colors = Color::where('product_id', $product_id)->get();

        return response()->json($colors);
    }

    public function sizesFetch($product_id)
    {
        $sizes = Size::where('product_id', $product_id)->get();

        return response()->json($sizes);
    }

    public function storeNum(Request $request)
    {
        $pid = sprintf("%'.04d", $request['product_id']);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        $ext = Exist::where('product_code', $pCode)->first();

        if ($ext) {
            $request['code'] = $pCode;
            return $this->changeNum($request);
        }

        if ($request['size_id'] == 'size not set' && $request['color_id'] == 'color not set') {
            return $this->storeNumWithoutNothing($request);
        }

        if ($request['size_id'] == 'size not set') {
            return $this->storeNumWithoutSize($request);
        }

        if ($request['color_id'] == 'color not set') {
            return $this->storeNumWithoutColor($request);
        }

        Exist::create([
            'product_code' => $pCode,
            'product_id' => $request['product_id'],
            'size_id' => $request['size_id'],
            'color_id' => $request['color_id'],
            'num' => $request['num'],
        ]);

        return response()->json(1);
    }
}
