<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    public function fetchall()
    {
        $categories = Category::where('parent', NULL)->with('childrenRecursive')->get();

        return response()->json($categories);
    }

    public function update(Request $request)
    {
        if ($request->file('image')) {
            if ($request['parent'] != 'undefined' && $request['parent'] != '0' && $request['parent'] != '') {
                $prnt = $request['parent'];
            } else {
                $prnt = null;
            }
            $category = Category::find($request['id']);
            $image = 'uploads/category/' . $category->image;
            unlink($image);

            $imageName = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/category'), $imageName);
            $category->name = $request['name'];
            $category->description = $request['description'];
            $category->parent = $request['parent'];
            $category->type = $request['type'];
            $category->image = $imageName;
            $category->save();
        } else {
            if ($request['parent'] != 'undefined' && $request['parent'] != '0' && $request['parent'] != '') {
                $prnt = $request['parent'];
            } else {
                $prnt = null;
            }
            $category = Category::find($request['id']);
            $category->name = $request['name'];
            $category->description = $request['description'];
            $category->type = $request['type'];
            $category->parent = $prnt;
            $category->save();
        }

        return response()->json(['success' => 'success'], 200);
    }

    public function fetchRootCat($type)
    {
        $roots = Category::where('parent', null)->where('type', $type)->get();

        return response()->json($roots);
    }

    public function fetchRootChild($id, $type)
    {
        $childs = Category::where('parent', $id)->where('type', $type)->with('childrenRecursive')->get();

        return response()->json($childs);
    }

    public function edit(Request $request)
    {
        $rules = [
            'name' => ['max:50'],
        ];

        $customMessages = [
            'name.max' => 'حداکثر 50 کاراکتر',
        ];

        $this->validate($request, $rules, $customMessages);

        $category = Category::find($request['id']);
        $category->name = $request['name'];
        $category->save();

        return response()->json(['key' => 'value']);
    }

    public function priorityEdit(Request $request)
    {
        $category = Category::find($request['id']);
        $category->priority = $request['priority'];
        $category->save();

        return response()->json(1, 200);

    }

    public function searchBrand($brand)
    {
        $result = Brand::where('name', 'like', '%' . $brand . '%')
            ->orWhere('name_f', 'like', '%' . $brand . '%')->get();

        return response()->json($result);
    }

    public function rootFetch()
    {
        $roots = Category::where('parent', null)->get();

        return response()->json($roots);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => ['max:50'],

        ];

        $customMessages = [
            'name.max' => 'حداکثر 50 کاراکتر',
        ];

        $this->validate($request, $rules, $customMessages);

        if ($request['parent'] == 'null') {
            $parent = null;
        } else {
            $parent = Category::where('id', $request['parent'])->pluck('id')->first();
        }

        Category::create([
            'name' => $request['name'],
            'parent' => $parent,
            'priority' => $request['priority'],
        ]);

        return response()->json(1, 200);
    }

    public function fetchList()
    {
        $categories = Category::whereNull('parent')->with('childrenRecursive')->orderBy('priority','asc')->get();

        return response()->json($categories);
    }

    public function fetch()
    {
        $categories = Category::with('childrenRecursive')->get();

        return response()->json($categories);
    }

    public function delete($catId)
    {
        $xx = Category::whereNotNull('parent')->get('parent');
        $y = [];
        foreach ($xx as $x) array_push($y, $x->parent);

        $parent = array_unique($y);

        $children = Category::whereNotIn('id', $parent)->get();

        $flag = false;
        foreach ($children as $child) {
            if ($child->id == $catId) $flag = true;
        }

        $pivot = DB::table('category_product')->where('cat_id', $catId)->first();

        if (!empty($pivot)) $flag = false;

        if (!$flag) return response()->json('cant');
        Category::find($catId)->delete();
    }
}
