<?php

namespace App\Http\Controllers\Admin;

use App\Models\Track;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class TrackController extends Controller
{
    public function index()
    {
        return view('admin.track.index');
    }

    public function store(Request $request)
    {
        $pdfName = time() . '.' . $request['pdf']->getClientOriginalExtension();
        $request['pdf']->move(public_path('images/pdf'), $pdfName);

        Track::create([
            'date' => $request['date'],
            'url' => $pdfName
        ]);
    }

    public function fetchAll()
    {
        $tracks = Track::orderBy('created_at', 'desc')->paginate(50);

        return response()->json($tracks);
    }

    public function delete($id)
    {
        $track = Track::find($id);
        $pdf = 'images/pdf/' . $track->url;
        if (file_exists($pdf)) {
            unlink($pdf);
        }
        $track->delete();
        return response()->json(['key' => 'value'], 200);
    }
}
