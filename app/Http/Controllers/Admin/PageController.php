<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Key;
use Illuminate\Http\Request;


class PageController extends Controller
{
    public function edit()
    {
        return view('admin.page.edit');
    }

    public function update(Request $request)
    {
        Key::where('key', 'return')->update([
            'value' => $request['return'],
        ]);

        Key::where('key', 'rules')->update([
            'value' => $request['rules'],
        ]);

        Key::where('key', 'delivery')->update([
            'value' => $request['delivery'],
        ]);
    }

    public function fetch()
    {
        $return = Key::where('key', 'return')->pluck('value')->first();
        $rules = Key::where('key', 'rules')->pluck('value')->first();
        $delivery = Key::where('key', 'delivery')->pluck('value')->first();


        return response()->json([
            'return' => $return,
            'rules' => $rules,
            'delivery' => $delivery,
        ]);
    }
}
