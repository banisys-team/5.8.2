<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    public function store1(Request $request)
    {
        $imageName = time() . '.webp';
        $img = Image::make($request['image']->path());
        $img->encode('webp')->save(env('STORE_PATH')('images/slider/') . $imageName);

        Slider::create([
            'url' => $request['url'],
            'image' => $imageName,
            'type' => 0,
        ]);
    }

    public function store2(Request $request)
    {
        $imageName = time() . '.webp';
        $img = Image::make($request['image']->path());
        $img->encode('webp')->save(env('STORE_PATH')('images/slider/') . $imageName);

        Slider::create([
            'url' => $request['url'],
            'image' => $imageName,
            'type' => 1,
        ]);
    }

    public function slideFetch()
    {
        $slider = Slider::get();

        return response()->json($slider);
    }

    public function delete($id)
    {
        $slide = Slider::find($id);

        $string_1 = 'images/slider/' . $slide->image;
        unlink($string_1);
        $slide->delete();

    }

}
