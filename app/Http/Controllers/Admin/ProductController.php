<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Color;
use App\Models\Exist;
use App\Models\Gallery;
use App\Models\Key;
use App\Models\Product;
use App\Models\Size;
use App\Models\Spec_value;
use App\Models\Specification;
use App\Models\Suggest;
use GuzzleHttp\Client;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public $result = array();

    public function fetch()
    {
        $products = Product::orderBy('priority', 'desc')->with(['cats', 'brand'])->paginate(50);

        return response()->json($products);
    }

    public function fetchValue($pro)
    {
        $values = Spec_value::where('product_id', $pro)->get();
        return response()->json($values);
    }

    public function search(Request $request)
    {
        $data = [];
        if (isset($request['name'])) {
            $data = Product::where('name', 'like', '%' . $request->name . '%')->with(['cats', 'brand'])
                ->orderBy('priority', 'desc')->paginate(50);
        }

        if (isset($request['cat_id'])) {
            $category = Category::where('id', $request['cat_id'])->with('childrenRecursive')->first();
            self::child($category);
            if (count($this->result) == 0) array_push($this->result, $request['cat_id']);

            $ripoProductIds = [];
            foreach ($this->result as $catId) {
                $productIds = DB::table('category_product')->where('cat_id', $catId)
                    ->pluck('product_id');

                foreach ($productIds as $productId) array_push($ripoProductIds, $productId);
            }
            $data = Product::whereIn('id', $ripoProductIds)->with(['cats', 'brand'])
                ->orderBy('priority', 'desc')->paginate(50);
        }

        if (isset($request['brand'])) {
            $brandId = Brand::where('name_f', 'like', '%' . $request['brand'] . '%')->pluck('id')->first();
            $data = Product::where('brand_id', $brandId)->with(['cats', 'brand'])
                ->orderBy('priority', 'desc')->paginate(50);
        }

        if (isset($request['shamsi_c'])) {
            $data = Product::where('shamsi_c', 'like', $request['shamsi_c'] . '%')->with(['cats', 'brand'])
                ->orderBy('priority', 'desc')->paginate(50);
        }

        return response()->json($data);
    }

    public function child($categories)
    {
        foreach ($categories->childrenRecursive as $item) {
            self::child($item);
            array_push($this->result, $item->id);
        }
    }

    public function getCatId($name)
    {
        $catId = Category::where('name', $name)->pluck('id')->first();

        return response()->json($catId);
    }

    public function editPrice(Request $request)
    {

        $product = Product::where('id', $request['id'])->first();

        $product->price = $request->price;
        $product->discount = $request->discount;

        $product->save();

        return response()->json(['key' => 'value'], 200);
    }

    public function searchName(Request $request)
    {
        $product = Product::where('name', 'like', '%' . $request->name . '%')->first();

        return response()->json($product);
    }

    public function suggestFetch()
    {
        $suggests = Suggest::with('product')->get();

        return response()->json($suggests);
    }

    public function suggestDelete(Suggest $id)
    {
        $id->delete();

        return response()->json(['key' => 'value']);
    }

    public function fetchRootCat()
    {
        $roots = Category::where('parent', null)->get();

        return response()->json($roots);
    }

    public function fetchRootChild($id)
    {
        $children = Category::where('parent', $id)->with('childrenRecursive')->get();

        return response()->json($children);
    }

    public function fetchAll()
    {
        $brand = Brand::orderBy('created_at', 'desc')->get();

        return response()->json($brand);
    }

    public function searchBrand($brand)
    {
        $result = Brand::where('name_f', 'like', '%' . $brand . '%')->get();

        return response()->json($result);

    }

    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'max:40'],
            'cat' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'image' => 'required',
        ];

        $customMessages = [
            'name.required' => 'نام الزامی است',
            'name.max' => 'حداکثر 40 کاراکتر',
        ];

//        $this->validate($request, $rules, $customMessages);

        $productExist = Product::where('name', $request['name'])->first();

        if (!empty($productExist)) {
            return response()->json($productExist->id);
        }

        $admin_id = auth('admin')->user()->id;

        $random1 = Str::random(2);
        $imageName = $random1 . time() . '.webp';
        $img = Image::make($request['image']->path());
        $img->encode('webp')->save(env('STORE_PATH')('images/product/') . $imageName);

        $slug = str_replace(" ", "-", $request['name']);
        $brandId = Brand::where('name_f', $request['brand'])->pluck('id')->first();
        $status = ($request['status'] == 0) ? $status = 0 : $status = 1;

        $product = Product::create([
            'name' => $request['name'],
            'brand_id' => $brandId,
            'price' => $request['price'],
            'discount' => $request['discount'],
            'short_desc' => $request['short_desc'],
            'status' => $status,
            'image' => $imageName,
            'slug' => $slug,
            'admin_id' => $admin_id,
            'tbl_size' => $request['tbl_size'],
        ]);
        $product->update([
            'shamsi_c' => Verta::instance($product->created_at)->format('Y/n/j'),
            'priority' => $product->id,
        ]);

        foreach ($request['cats'] as $cat) {
            DB::table('category_product')->insert([
                'cat_id' => $cat,
                'product_id' => $product->id,
            ]);
        }

        $colors = json_decode($request['colors']);
        foreach ($colors as $color) {
            if (!empty($color->code)) {
                $code = $color->code;
            } else {
                $code = '#000000';
            }
            Color::create([
                'name' => $color->name,
                'code' => $code,
                'product_id' => $product->id,
            ]);
        }

        $specifications = json_decode($request['specifications']);
        foreach ($specifications as $specification) {
            Specification::create([
                'key' => $specification->key,
                'value' => $specification->value,
                'product_id' => $product->id,
            ]);
        }

        $sizes = json_decode($request['sizes']);
        foreach ($sizes as $size) {
            Size::create([
                'name' => $size->name,
                'product_id' => $product->id,
            ]);
        }

        if (!empty($request->pics)) {
            foreach ($request->pics as $pic) {
                $random2 = Str::random(3);
                $imageName2 = $random2 . time() . '.webp';
                $img = Image::make($pic->path());
                $img->encode('webp')->save(env('STORE_PATH')('images/gallery/') . $imageName2);
                Gallery::create([
                    'image' => $imageName2,
                    'product_id' => $product->id,
                ]);
            }
        }
        try {
            $this->offPooshStore($product->id);
        } catch (\Exception $e) {
        }

        return response()->json($product->id);
    }

    public function offPooshStore($productId)
    {
        $product = Product::with(['colors', 'sizes', 'galleries', 'cats.parentRecursive', 'brand', 'specifications'])
            ->find($productId)->toJson();

        $client = new Client();
        $client->request('POST', 'https://off-poosh.ir/api/product/store', [
            'json' => [
                'product' => $product,
                'url' => config()->get('app.url'),
                'brand' => config()->get('app.name'),
            ]
        ]);
    }

    public function fetchProduct($id)
    {
        $product = Product::where('id', $id)->with(['cats', 'brand'])->first();

        return response()->json($product);
    }

    public function fetchBrands($product_id)
    {
        $catId = Product::where('id', $product_id)->pluck('cat_id')->first();

        $brandIds = DB::table('brand_category')->where('cat_id', $catId)->get();

        $array = array();

        foreach ($brandIds as $brandId) {
            array_push($array, $brandId->brand_id);
        }

        $brands = Brand::whereIn('id', $array)->get();

        return response()->json($brands);
    }

    public function fetchGallery($pro)
    {
        $gallery = Gallery::where('product_id', $pro)->get();

        return response()->json($gallery);
    }

    public function specificationsFetch($product_id)
    {
        $specifications = Specification::where('product_id', $product_id)->get();

        return response()->json($specifications);
    }

    public function specificationsDelete($id)
    {
        Specification::find($id)->delete();

        return response()->json(1, 200);
    }

    public function colorsFetch($product_id)
    {
        $colors = Color::where('product_id', $product_id)->get();

        return response()->json($colors);
    }

    public function sizesFetch($product_id)
    {
        $sizes = Size::where('product_id', $product_id)->get();

        return response()->json($sizes);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => ['required', 'max:40'],
            'cat_name' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'image' => 'required',
        ];

        $customMessages = [
            'name.required' => 'نام الزامی است',
            'name.max' => 'حداکثر 40 کاراکتر',
        ];

//        $this->validate($request, $rules, $customMessages);

        $product = Product::find($id);
        $oldSlug = $product->slug;

        if (!empty($request->image)) {
            if ($request->image != $product->image) {

                $img = 'images/product/' . $product->image;
                try {
                    unlink($img);
                } catch (\Exception $e) {
                }

                $random1 = Str::random(2);
                $imageName = $random1 . time() . '.webp';
                $img = Image::make($request['image']->path());
                $img->encode('webp')->save(env('STORE_PATH')('images/product/') . $imageName);
                $product->image = $imageName;
            }
        }

        $slug = str_replace(" ", "-", $request['name']);

        $status = ($request['status'] == 0) ? 0 : 1;
        $brandId = Brand::where('name_f', $request['brand_name'])->pluck('id')->first();

        $product->brand_id = $brandId;
        $product->name = $request['name'];
        $product->slug = $slug;
        $product->price = $request['price'];
        $product->discount = $request['discount'];
        $product->short_desc = $request['short_desc'];
        $product->tbl_size = $request['tbl_size'];
        $product->status = $status;

        DB::table('category_product')->where('product_id', $product->id)->delete();
        foreach ($request['cats'] as $cat) {
            DB::table('category_product')->insert([
                'cat_id' => $cat,
                'product_id' => $product->id,
            ]);
        }

        if (!empty($request['colors'])) {
            $colorCheck = Exist::where('product_id', $id)->where('color_id', 0)->first();
            if (!empty($colorCheck)) {
                Exist::where('product_id', $id)->delete();
            }
            $colors = json_decode($request['colors']);
            foreach ($colors as $color) {
                $code = (!empty($color->code)) ? $color->code : '#000000';
                Color::updateOrCreate([
                    'name' => $color->name,
                    'code' => $code,
                    'product_id' => $product->id,
                ], [
                    'name' => $color->name,
                    'code' => $code,
                    'product_id' => $product->id,
                ]);
            }
        }

        if (!empty($request['sizes'])) {
            $sizeCheck = Exist::where('product_id', $id)->where('size_id', 0)->first();
            if (!empty($sizeCheck)) {
                Exist::where('product_id', $id)->delete();
            }
            $sizes = json_decode($request['sizes']);
            foreach ($sizes as $size) {
                Size::updateOrCreate([
                    'name' => $size->name,
                    'product_id' => $product->id,
                ], [
                    'name' => $size->name,
                    'product_id' => $product->id,
                ]);
            }
        }

        $specifications = json_decode($request['specifications']);
        foreach ($specifications as $specification) {
            Specification::updateOrCreate([
                'key' => $specification->key,
                'value' => $specification->value,
                'product_id' => $product->id,
            ], [
                'key' => $specification->key,
                'value' => $specification->value,
                'product_id' => $product->id,
            ]);
        }

        if (isset($request->pics)) {
            foreach ($request->pics as $pic) {
                $random2 = Str::random(3);
                $imageName2 = $random2 . time() . '.webp';
                $img = Image::make($pic->path());
                $img->encode('webp')->save(env('STORE_PATH')('images/gallery/') . $imageName2);
                Gallery::create([
                    'image' => $imageName2,
                    'product_id' => $product->id,
                ]);
            }
        }
        $product->save();
        try {
            $this->offPooshUpdate($oldSlug, $product->slug);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
    }

    public function offPooshUpdate($oldSlug, $newSlug)
    {
        $product = Product::where('slug', $newSlug)
            ->with(['colors', 'sizes', 'galleries', 'cats.parentRecursive', 'brand', 'specifications'])->first();

        $client = new Client();
        $client->request('POST', 'https://off-poosh.ir/api/product/update', [
            'json' => [
                'product' => $product->toJson(),
                'old_slug' => $oldSlug,
                'url' => config()->get('app.url'),
                'brand' => config()->get('app.name'),
            ]
        ]);
    }

    public function deleteGallery($gallery_id)
    {
        $gallery = Gallery::find($gallery_id);
//        unlink('images/gallery/' . $gallery->image);
        $gallery->delete();

        return response()->json(['key' => 'value'], 200);
    }

    public function colorDelete($color_id)
    {
        Cart::where('size_id', $color_id)->delete();
        Exist::where('color_id', $color_id)->delete();
        Color::find($color_id)->delete();

        return response()->json(1, 200);
    }

    public function sizeDelete($size_id)
    {
        Cart::where('size_id', $size_id)->delete();
        Exist::where('size_id', $size_id)->delete();
        Size::find($size_id)->delete();

        return response()->json(1, 200);
    }

    public function delete($product_id)
    {
        DB::transaction(function () use ($product_id) {
            $product = Product::find($product_id);
            $image = 'images/product/' . $product->image;
            $rr = file_exists($image);
            if ($rr == 1) unlink($image);

            foreach ($product->galleries as $gallery) {
                $g = 'images/gallery/' . $gallery->image;
                $rrr = file_exists($g);
                if ($rrr == 1) unlink($g);
            }
            DB::table('category_product')->where('product_id', $product_id)->delete();
            $product->galleries()->delete();
            $product->colors()->delete();
            $product->sizes()->delete();
            $product->carts()->delete();
            $product->exists()->delete();
            $product->specifications()->delete();
            $product->suggests()->delete();
            try {
                $this->offPooshDelete($product_id);
            } catch (\Exception $e) {
            }
            $product->delete();
        });
    }

    public function offPooshDelete($productId)
    {
        $client = new Client();
        $client->request('POST', 'https://off-poosh.ir/api/product/delete', [
            'json' => [
                'slug' => Product::where('id', $productId)->pluck('slug'),
            ]
        ]);
    }

    public function suggestAdd($product_id)
    {
        $suggest = Suggest::where('product_id', $product_id)->first();
        if (empty($suggest)) {
            Suggest::create(['product_id' => $product_id]);
        }

        return response()->json(1, 200);
    }

    public function SuggestSearchName(Request $request)
    {
        $product = Product::where('name', 'like', '%' . $request->name . '%')->first();

        return response()->json($product);
    }

    public function SuggestTimerFetch()
    {
        $time = Key::where('key', 'suggest timer')->pluck('value')->first();

        return response()->json($time);
    }

    public function SuggestTimeStore($time)
    {
        Key::where('key', 'suggest timer')->update([
            'value' => $time
        ]);

        return response()->json(1, 200);
    }

    public function breadcrumb(Request $request)
    {
        $breadcrumbs = [];
        foreach ($request['catIds'] as $catId) {
            $cat = Category::where('id', $catId)->with('parentRecursive')->first()->toArray();
            array_push($breadcrumbs, $cat);
        }

        return response()->json($breadcrumbs);
    }

    public function checkExist()
    {
        $products = Product::get();

        foreach ($products as $product) {
            $exist = Exist::where('product_id', $product['id'])->first();
            if (empty($exist)) {
                $product->update(['status' => 2]);
                try {
                    $this->offPooshChangeStatus($product->slug);
                } catch (\Exception $e) {
                }
            }
        }
    }

    public function offPooshChangeStatus($slug)
    {
        $client = new Client();
        $client->request('POST', 'https://off-poosh.ir/api/product/change_status', [
            'json' => [
                'slug' => $slug,
            ]
        ]);
    }

    public function priorityUpdate(Request $request)
    {
        Product::find($request['product_id'])->update([
            'priority' => $request['priority']
        ]);
    }

    public function rechargeFetch()
    {
        $products = Product::where('status', 2)->orderBy('priority', 'desc')
            ->with(['cats', 'brand'])->paginate(50);

        return response()->json($products);
    }

}
