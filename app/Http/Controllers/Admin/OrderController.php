<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exist;
use App\Models\Key;
use App\Models\Note;
use App\Models\Order;
use App\Models\Order_value;
use App\Models\Track;
use App\Models\User;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    public function index()
    {
        return view('admin.order.index');
    }

    public function fetch()
    {
        $orders = Order::orderBy('created_at', 'desc')->with(['user'])->paginate(100);

        return response()->json($orders);
    }

    public function fetchNotConfirm()
    {
        $orders = Order::whereNull('confirm')->orderBy('created_at', 'desc')->with('order_values')
            ->with('user')
            ->paginate(7);
        return response()->json($orders);
    }

    public function fetchConfirm()
    {
        $orders = Order::whereNotNull('confirm')->orderBy('created_at', 'desc')->with('order_values')
            ->with('user')
            ->paginate(7);
        return response()->json($orders);
    }

    public function changeStatus(Request $request, $id)
    {
        $order = Order::where('id', $id)->with('order_values')->first();

//        if($request['status'] == 1){
//            foreach ($order->order_values as $item) {
//                $exist = Exist::where('product_code', $item->product_code)->first();
//                $exist->num = $exist->num - $item->number;
//                $exist->save();
//            }
//        }

        $order->status = $request['status'];
        $order->save();

        return response()->json(['key' => 'value'], 200);
    }

    public function search(Request $request)
    {
        $data = null;
        if (isset($request->id)) {
            $data = Order::where('id', $request->id)->with('order_values')->with('user')
                ->orderBy('created_at', 'desc')->paginate(30);
        }

        if (isset($request->name)) {
            $data = Order::where('name', $request->name)->with('order_values')->with('user')
                ->orderBy('created_at', 'desc')->paginate(30);
        }

        if (isset($request->mobile)) {
            $user = User::where('mobile', $request->mobile)->first();
            if (isset($user)) {
                $data = Order::where('user_id', $user->id)->with('order_values')->with('user')
                    ->orderBy('created_at', 'desc')->paginate(30);
            }
        }

        if (isset($request->shamsi_c)) {
            $data = Order::where('shamsi_c', 'like', $request->shamsi_c . '%')->with('order_values')->with('user')
                ->orderBy('created_at', 'desc')->paginate(500);
        }

        if (isset($request->status)) {

            $data = Order::where('status', $request->status)->with('order_values')->with('user')
                ->orderBy('created_at', 'desc')->paginate(30);
        }

        if (isset($request->shamsiless)) {
            $explode = explode("/", $request->shamsiless);
            $date = Verta::getGregorian($explode[0], $explode[1], $explode[2]);
            $date = implode("-", $date);

            if (isset($date)) {
                $data = Order::whereDate('created_at', '<', $date)->with('order_values')->with('user')
                    ->orderBy('created_at', 'desc')->paginate(30);
            }
        }

        if (isset($request->shamsimore)) {
            $explode = explode("/", $request->shamsimore);
            $date = Verta::getGregorian($explode[0], $explode[1], $explode[2]);
            $date = implode("-", $date);

            if (isset($date)) {
                $data = Order::whereDate('created_at', '>', $date)->with('order_values')->with('user')
                    ->orderBy('created_at', 'desc')->paginate(30);
            }
        }

        return response()->json($data);
    }

    public function delete($id)
    {
        $order = Order::where('id', $id)->first();

        $order->order_values()->delete();
        $order->delete();

        return response()->json(['key' => 'value'], 200);
    }

    public function factor()
    {
        return view('site.factor');
    }

    public function fetchOrder($order_id)
    {
        $order = Order::where('id', $order_id)->with(['order_values','user','track'])->first();

        return response()->json($order);
    }

    public function fetchOrderValue($id)
    {
        $order_values = Order_value::where('order_id', $id)->with('color')->with('product')->get();
        $order_values = $order_values->groupBy('cart_id');

        return response()->json($order_values);
    }

    public function sumPrice($id)
    {
        $orders = Order_value::where('order_id', $id)->get();
        $orders = $orders->groupBy('cart_id');
        $sum = 0;
        foreach ($orders as $order) {
            $price = $order[0]->number * $order[0]->price;
            $sum = $price + $sum;
        }

        return response()->json($sum);
    }

    public function factorConfirm($id)
    {
        Order::where('id', $id)->update(['confirm' => 1]);
        $orderId = Order::where('id', $id)->pluck('id')->first();
        $OrderValues = Order_value::where('order_id', $orderId)->get(['product_code', 'number']);

        foreach ($OrderValues as $OrderValue) {
            $num = Exist::where('product_code', $OrderValue->product_code)->pluck('num')->first();
            $result = $num - $OrderValue->number;
            Exist::where('product_code', $OrderValue->product_code)->update(['num' => $result]);
        }

        return response()->json(['key' => 'value'], 200);
    }

    public function fetchNumber($id)
    {
        $sum = Order_value::where('order_id', $id)->sum('number');

        return response()->json($sum);
    }

    public function userReserve()
    {
        return view('admin.order.users');
    }

    public function userReserveFetch()
    {
//        $users = User::where('wanted', 1)->with(['orders' => function ($query) {
//                $query->where('reserve', 2);
//            }])->orderBy('created_at', 'desc')->paginate(100);
        $users = User::where('wanted', 1)->with('factors_wanted')->orderBy('created_at', 'desc')->paginate(100);

        return response()->json($users);
    }

    public function changeReserveStatus(Request $request, $id)
    {
        if ($request['status'] == 2 || $request['status'] == 4) {
            Order::where('user_id', $id)->where('reserve', 2)->update([
                'reserve' => 3,
                'status' => $request['status'],
            ]);
            User::find($id)->update([
                'wanted' => null,
                'status_order' => null
            ]);
        } else {
            User::find($id)->update([
                'status_order' => $request['status']
            ]);
            Order::where('user_id', $id)->where('reserve', 2)->update([
                'status' => $request['status'],
            ]);
        }

        return response()->json(['key' => 'value'], 200);
    }

    public function historyReserve()
    {
        return view('admin.order.history_reserve');
    }

    public function reserveSearch(Request $request)
    {
        $userId = User::where('mobile', $request['mobile'])->pluck('id')->first();

        $orders = Order::where('user_id', $userId)->whereNotNull('reserve_g')->get();

        return response()->json($orders->groupBy('reserve_g'));
    }

    public function settingFetch()
    {
        $logo = Key::where('key', 'setting_logo')->first();
        $name = Key::where('key', 'setting_name')->first();

        return response()->json([
            'logo' => $logo,
            'name' => $name,
        ]);
    }

    public function noteFetch($order_id)
    {
        $note = Note::where('order_id', $order_id)->first();

        return response()->json($note);
    }

    public function noteStore(Request $request)
    {
        $note = Note::where('order_id', $request['order_id'])->first();

        if (empty($note)) {
            $note = Note::create([
                'order_id' => $request['order_id'],
                'note' => $request['note'],
            ]);
            $note->shamsi_u = Verta::instance($note->updated_at)->format('Y/n/j');
        } else {
            $note->note = $request['note'];
            $note->save();
            $note->shamsi_u = Verta::instance($note->updated_at)->format('Y/n/j');

        }
        $note->save();
        return response()->json(1, 200);
    }

    public function postTrackStore(Request $request)
    {
        $track = Track::where('order_id', $request['order_id'])->first();

        if (empty($track)) {
            Track::create([
                'order_id' => $request['order_id'],
                'number' => $request['number'],
            ]);
        } else {
            $track->update([
                'number' => $request['number']
            ]);
        }
    }

    public function postTrackFetch($order_id)
    {
        $track = Track::where('order_id', $order_id)->first();

        return response()->json($track);
    }

}
