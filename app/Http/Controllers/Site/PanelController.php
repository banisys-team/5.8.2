<?php

namespace App\Http\Controllers\Site;

use App\Models\Holder;
use App\Models\Size;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Color;
use App\Models\Exist;
use App\Models\Off;
use App\Models\Order;
use App\Models\Order_value;
use App\Models\Product;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PanelController extends Controller
{
    public function orders()
    {
        return view('site.panel.order');
    }

    public function offs()
    {
        return view('front.panel.off');
    }

    public function fetchOrders()
    {
        $user = Auth::user()->id;

        $orders = Order::where('user_id', $user)
            ->where(function ($query) {
                $query->where('reserve', '!=', 1);
            })->orderBy('created_at', 'desc')->paginate(30);

        return response()->json($orders);
    }

    public function account()
    {
        return view('site.panel.account');
    }

    public function store(Request $request)
    {
        auth()->user()->update([
            'name' => $request['name'],
            'state' => $request['state'],
            'city' => $request['city'],
            'address' => $request['address'],
            'postal_code' => $request['postal_code'],
        ]);
    }

    public function fetchUser()
    {
        return response()->json(
            auth()->user()
        );
    }

    public function fetchOffs()
    {
        $user = Auth::user();
        $now = Carbon::now();
        $orders = Order::where('user_id', $user->id)->get();

        if (isset($orders[0]->id)) {
            $sum = 0;
            foreach ($orders as $order) {
                $sum = $sum + $order->sum_final;
            }

            $offs = Off::where('min', '<', $sum)->whereDate('e_date', '>', $now)->get();

            foreach ($offs as $key => $off) {
                foreach ($orders as $order) {
                    if ($off->code == $order->off_code) {
                        unset($offs[$key]);
                    }
                }
            }
        } else {
            $offs = Off::where('min', 0)->whereDate('e_date', '>', $now)->get();
        }

        return response()->json($offs);
    }

    public function storeOrder($Authority, $RefID)
    {
        $order = Order::where('refid', $RefID)->first();
        if (!empty($order)) return false;

        $holder = Holder::where('authority', $Authority)->first();
        $user = User::find($holder->user_id);
        auth()->login($user);

        $order = Order::create([
            'name' => $holder->name,
            'final_total' => $holder->final_total,
            'delivery' => $holder->delivery,
            'vat' => $holder->vat,
            'state' => $holder->state,
            'city' => $holder->city,
            'address' => $holder->address,
            'description' => $holder->description,
            'reserve' => $holder->reserve,
            'user_id' => $user->id,
            'status' => 0,
            'refid' => $RefID,
        ]);
        $order->update(['shamsi_c' => Verta::instance($order->created_at)->format('Y/n/j')]);

        $carts = Cart::where('cookie', $user->cookie_cart)->get();

        if ($order->reserve == 2) {
            $random = rand(10000, 99999);
            $orders = Order::where('user_id', $user->id)->where('reserve', 1)->orWhere('reserve', 2)->get();
            foreach ($orders as $item) {
                $item->update(['reserve' => 2]);
                $item->reserve_g = $random;
            }
            $user->update(['wanted' => 1]);
        }

        foreach ($carts as $cart) {
            $product = Product::find($cart->product_id);
            $sizeName = Size::where('id', $cart->size_id)->pluck('name')->first();
            $colorName = Color::where('id', $cart->color_id)->pluck('name')->first();

            $OrderValue = Order_value::create([
                'product_code' => $cart->product_code,
                'product_name' => $product->name,
                'size' => $sizeName,
                'color' => $colorName,
                'number' => $cart->number,
                'discount' => $product->discount,
                'order_id' => $order->id,
                'cart_id' => $cart->id,
                'price' => $product->price,
            ]);

            $exist = Exist::where('product_code', $OrderValue->product_code)->first();
            $exist->update(['num' => $exist->num - $OrderValue->number]);
            if ($exist->num == 0) $exist->delete();

        }

        foreach ($carts as $cart) $cart->delete();

        $user->update([
            'name' => $holder->name,
            'state' => $holder->state,
            'city' => $holder->city,
            'address' => $holder->address,
        ]);

    }

    public function storeOrderManually(Request $request)
    {
        $user = User::where('mobile', $request['mobile'])->first();

        if (empty($user)) {
            $user = User::create([
                'manually' => 1,
                'name' => $request['name'],
                'mobile' => $request['mobile'],
                'state' => $request['state'],
                'city' => $request['city'],
                'address' => $request['address'],
            ]);
        } else {
            $user->update([
                'name' => $request['name'],
                'state' => $request['state'],
                'city' => $request['city'],
                'address' => $request['address'],
            ]);
        }


        $order = Order::create([
            'name' => $request['name'],
            'final_total' => $request['final_total'],
            'delivery' => $request['delivery'],
            'state' => $request['state'],
            'city' => $request['city'],
            'address' => $request['address'],
            'description' => $request['description'],
            'user_id' => $user->id,
            'status' => 0,
            'manually' => 1,
        ]);
        $order->update(['shamsi_c' => Verta::instance($order->created_at)->format('Y/n/j')]);

        $carts = Cart::where('cookie', $_COOKIE['cart'])->get();
        foreach ($carts as $cart) {
            $product = Product::find($cart->product_id);
            $sizeName = Size::where('id', $cart->size_id)->pluck('name')->first();
            $colorName = Color::where('id', $cart->color_id)->pluck('name')->first();

            $OrderValue = Order_value::create([
                'product_code' => $cart->product_code,
                'product_name' => $product->name,
                'size' => $sizeName,
                'color' => $colorName,
                'number' => $cart->number,
                'discount' => $product->discount,
                'order_id' => $order->id,
                'cart_id' => $cart->id,
                'price' => $product->price,
            ]);
            $exist = Exist::where('product_code', $OrderValue->product_code)->first();
            $exist->update(['num' => $exist->num - $OrderValue->number]);
            if ($exist->num == 0) $exist->delete();
        }

        foreach ($carts as $cart) $cart->delete();
    }
}
