<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use SoapClient;

class ReserveController extends Controller
{
    public function reserveJust()
    {
        $order = Order::where('refid', session()->get('refid'))->first();
        Auth::loginUsingId($order->user_id, TRUE);
        $order->reserve = 1;
        $order->save();

        return view('front.panel.reserve');
    }

    public function reserve()
    {
        return view('site.panel.reserve');
    }

    public function fetchReserve()
    {
        $user = Auth::user()->id;
        $orders = Order::where('user_id', $user)->where('reserve', 1)
            ->orderBy('created_at', 'desc')->get();

        return response()->json($orders);
    }

    public function wanted()
    {
        $user = Auth::user();
        $orders = Order::where('user_id', $user->id)->where('reserve', 1)->get();

        foreach ($orders as $order) {
            $order->reserve_shamsi = Verta::instance(Carbon::now())->format('Y/n/j');
            $order->save();
        }

        $random = rand(10000, 99999);
        foreach ($orders as $order) {
            $order->reserve = 2;
            $order->reserve_g = $random;
            $order->refid_delivery = session()->get('refid');
            $order->save();
        }

        $user->wanted = 1;
        $user->save();


        return response()->json(['key' => 'value'], 200);
    }

    public function redirectToZarinpalDelivery()
    {
        $user = auth()->user();

        $orders = Order::where('user_id', $user->id)->where('reserve', 1)->with('order_values')->get();

        $price = 14000;
        $number = 0;
        foreach ($orders as $order) {
            foreach ($order->order_values as $item) {
                $number += $item->number;
            }
        }

        if ($number > 4) {
            $price = 16000;
        }

        $MerchantID = '7e0c3e5e-77d3-421d-ae62-f8e64310c080';
        $Amount2 = $price;
        $Description = 'نام شرکت';
        $CallbackURL = url('/order/return/zarinpal/delivery');
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);


        session()->put('sum_final', $Amount2);


        $result = $client->PaymentRequest([
            'MerchantID' => $MerchantID,
            'Amount' => $Amount2,
            'Description' => $Description,
            'CallbackURL' => $CallbackURL,
        ]);
        session()->put('authority', $result->Authority);

        if ($result->Status == 100) {
            return redirect()->away('https://www.zarinpal.com/pg/StartPay/' . $result->Authority);
        } else {
            echo 'ERR: ' . $result->Status;
        }
    }

    public function returnFromZarinpal()
    {
        return view('site.return_zarinpal_delivery');
    }

    public function orderReserveNumFetch()
    {
        $user = auth()->user();
        $orders = Order::where('user_id', $user->id)->where('reserve', 1)->with('order_values')->get();

        $number = 0;
        foreach ($orders as $order) {
            foreach ($order->order_values as $item) {
                $number += $item->number;
            }
        }

        return response()->json($number);
    }

    public function reserveExistCheck()
    {
        $order = Order::where('user_id', auth()->user()->id)->where('reserve', 1)->first();
        if (isset($order->id)) {
            return response()->json(1);
        } else {
            return response()->json(0);
        }

    }


}
