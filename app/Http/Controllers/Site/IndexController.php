<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Exist;
use App\Models\Gallery;
use App\Models\Key;
use App\Models\Product;
use App\Models\Complaint;
use App\Models\Slider;
use App\Models\Spec_value;
use App\Models\Suggest;
use App\Models\Track;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IndexController extends Controller
{
    public $result = array();

    public function index()
    {
        $siteName = Key::where('key', 'setting_name')->pluck('value')->first();

        return view('site.index', [
            'site_name' => $siteName
        ]);
    }

    public function fetchProducts()
    {
        $products = Product::where('status', 1)->take(16)->get();

        return response()->json($products);
    }

    public function fetchCat($slug)
    {
        $productId = Product::where('slug', $slug)->pluck('id');
        $catIds = DB::table('category_product')->where('product_id', $productId)->pluck('cat_id');

        $cats = Category::whereIn('id', $catIds)->with('parentRecursive')->get();

        return response()->json($cats);

    }

    public function fetchCat2($slug)
    {
        $cat = Category::where('id', $slug)->with('parentRecursive')->get();

        return response()->json($cat);
    }

    public function cart()
    {
        if (isset($_COOKIE['cart'])) {
            $carts = Cart::where('cookie', $_COOKIE['cart'])->get();
            if (!empty($carts)) {
                foreach ($carts as $cart) {
                    $exist = Exist::where('product_id', $cart['product_id'])
                        ->where('size_id', $cart['size_id'])
                        ->where('color_id', $cart['color_id'])->first();
                    if (empty($exist)) {
                        $cart->delete();
                    }
                }
            }
        }

        return view('site.cart');
    }

    public function detail($slug)
    {
        return view('site.detail');
    }

    public function fetch($slug)
    {
        $product = Product::where('slug', $slug)->with(['brand', 'sizes', 'colors', 'exists', 'galleries','specifications'])->first();

        return response()->json($product);
    }

    public function fetchGalleries($slug)
    {
        $productId = Product::where('slug', $slug)->pluck('id')->first();
        $galleries = Gallery::where('product_id', $productId)->get();

        return response()->json($galleries);
    }

    public function fetchColor($slug)
    {
        $product = Product::where('slug', $slug)->first();
        $colors = $product->colors;
        return response()->json($colors);
    }

    public function storeCart(Request $request)
    {
        if ($request['size_id'] == null && $request['color_id'] == null) {
            return $this->storeCartWithoutNothing($request);
        }

        if ($request['size_id'] == null) {
            return $this->storeCartWithoutSize($request);
        }

        if ($request['color_id'] == null) {
            return $this->storeCartWithoutColor($request);
        }

        $pid = sprintf("%'.04d", $request['product_id']);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        if (isset($_COOKIE['cart'])) {
            $cart = Cart::where('cookie', $_COOKIE['cart'])->where('product_id', $request['product_id'])
                ->where('size_id', $request['size_id'])->where('color_id', $request['color_id'])->first();

            if (empty($cart)) {
                Cart::create([
                    'product_code' => $pCode,
                    'product_id' => $request['product_id'],
                    'cookie' => $_COOKIE['cart'],
                    'size_id' => $request['size_id'],
                    'color_id' => $request['color_id'],
                    'number' => $request['number'],
                ]);
            } else {
                $cart->update([
                    'number' => $request['number']
                ]);
            }
        } else {

            $cookie = time() . rand(1, 1000);
            setcookie('cart', $cookie, time() + 60 * 60 * 24 * 365, '/');
            Cart::create([
                'product_code' => $pCode,
                'product_id' => $request['product_id'],
                'cookie' => $cookie,
                'size_id' => $request['size_id'],
                'color_id' => $request['color_id'],
                'number' => $request['number'],
            ]);

            return response()->json(1, 200);
        }

        return response()->json(1, 200);
    }

    public function storeCartWithoutNothing($request)
    {
        $pid = sprintf("%'.04d", $request['product_id'] ?? 0);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        if (isset($_COOKIE['cart'])) {
            $cart = Cart::where('cookie', $_COOKIE['cart'])->where('product_id', $request['product_id'])->first();
            if (empty($cart)) {
                Cart::create([
                    'product_code' => $pCode,
                    'product_id' => $request['product_id'],
                    'cookie' => $_COOKIE['cart'],
                    'number' => $request['number'],
                    'size_id' => 0,
                    'color_id' => 0,
                ]);
            } else {
                $cart->update([
                    'number' => $request['number']
                ]);
            }
            return response()->json(1);

        } else {
            $cookie = time() . rand(1, 1000);
            setcookie('cart', $cookie, time() + 60 * 60 * 24 * 365, '/');
            Cart::create([
                'product_code' => $pCode,
                'product_id' => $request['product_id'],
                'cookie' => $cookie,
                'number' => $request['number'],
                'size_id' => 0,
                'color_id' => 0,
            ]);
            return response()->json(1);
        }
    }

    public function storeCartWithoutColor($request)
    {
        $pid = sprintf("%'.04d", $request['product_id']);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        if (isset($_COOKIE['cart'])) {
            $cart = Cart::where('cookie', $_COOKIE['cart'])->where('product_id', $request['product_id'])
                ->where('size_id', $request['size_id'])->first();

            if (empty($cart)) {
                Cart::create([
                    'product_code' => $pCode,
                    'product_id' => $request['product_id'],
                    'cookie' => $_COOKIE['cart'],
                    'size_id' => $request['size_id'],
                    'number' => $request['number'],
                    'color_id' => 0,
                ]);
            } else {
                $cart->update([
                    'number' => $request['number']
                ]);
            }
        } else {
            $cookie = time() . rand(1, 1000);
            setcookie('cart', $cookie, time() + 60 * 60 * 24 * 365, '/');
            Cart::create([
                'product_code' => $pCode,
                'size_id' => $request['size_id'],
                'product_id' => $request['product_id'],
                'cookie' => $cookie,
                'number' => $request['number'],
                'color_id' => 0,
            ]);

            return response()->json(1);
        }

        return response()->json(['key' => 'value'], 200);
    }

    public function storeCartWithoutSize($request)
    {
        $pid = sprintf("%'.04d", $request['product_id']);
        $cid = sprintf("%'.04d", $request['color_id'] ?? 0);
        $sid = sprintf("%'.04d", $request['size_id'] ?? 0);
        $pCode = "{$pid}{$cid}{$sid}";

        if (isset($_COOKIE['cart'])) {
            $cart = Cart::where('cookie', $_COOKIE['cart'])->where('product_id', $request['product_id'])
                ->where('color_id', $request['color_id'])->first();

            if (empty($cart)) {
                Cart::create([
                    'product_code' => $pCode,
                    'product_id' => $request['product_id'],
                    'cookie' => $_COOKIE['cart'],
                    'color_id' => $request['color_id'],
                    'number' => $request['number'],
                    'size_id' => 0,
                ]);
            } else {
                $cart->update([
                    'number' => $request['number']
                ]);
            }
            return response()->json(1);

        } else {
            $cookie = time() . rand(1, 1000);
            setcookie('cart', $cookie, time() + 60 * 60 * 24 * 365, '/');
            Cart::create([
                'product_code' => $pCode,
                'color_id' => $request['color_id'],
                'product_id' => $request['product_id'],
                'cookie' => $cookie,
                'number' => $request['number'],
                'size_id' => 0,
            ]);
            return response()->json(1);
        }
    }

    public function fetchCart()
    {
        $carts = [];
        if (isset($_COOKIE['cart'])) {
            $carts = Cart::where('cookie', $_COOKIE['cart'])->with(['product', 'size', 'color'])
                ->get();

            foreach ($carts as $cart) {
                $existNum = Exist::where('product_id', $cart['product_id'])
                    ->where('size_id', $cart['size_id'])
                    ->where('color_id', $cart['color_id'])->pluck('num')->first();

                $cart['exist_num'] = $existNum;

                if ($cart['number'] > $existNum) {
                    $cart->update([
                        'number' => $existNum
                    ]);
                }
            }
        }

        return response()->json($carts);
    }

    public function cartTotal(Request $request)
    {
        $cart = Cart::where('id', $request['cart_id'])->first();
        $cart->update([
            'number' => $request['number']
        ]);
    }

    public function sumTotal()
    {
        $carts = Cart::where('cookie', $_COOKIE['cart'])->get();

        $sum = 0;
        foreach ($carts as $cart) {
            $sum = $cart->total + $sum;
        }

        return response()->json($sum);
    }

    public function sumPrice()
    {
        $carts = Cart::where('cookie', $_COOKIE['cart'])->get();

        $sum = 0;
        foreach ($carts as $cart) {
            $price = $cart->number * $cart->price;
            $sum = $price + $sum;
        }

        return response()->json($sum);
    }

    public function deleteCart($id)
    {
        Cart::where('id', $id)->delete();
    }

    public function fetchValue($slug)
    {
        $pro = Product::where('slug', $slug)->pluck('id')->first();
        $values = Spec_value::where('product_id', $pro)->get();
        return response()->json($values);
    }

    public function storeComment(Request $request)
    {
        $user = Auth::user();
        $comment = Comment::create([
            'body' => $request['body'],
            'user_id' => $user->id,
            'product_id' => $request['id'],
        ]);
        $comment->shamsi_c = Verta::instance($comment->created_at)->format('Y/n/j');
        $comment->save();

        return response()->json(['key' => 'value'], 200);
    }

    public function storeReplyComment(Request $request)
    {
        $product_id = Product::where('slug', $request['slug'])->pluck('id')->first();

        $user = Auth::user();

        $comment = Comment::create([
            'body' => $request['reply'],
            'user_id' => $user->id,
            'product_id' => $product_id,
            'parent' => $request['parent'],
        ]);

        $comment->shamsi_c = Verta::instance($comment->created_at)->format('Y/n/j');

        $comment->save();

        return response()->json(['key' => 'value'], 200);
    }

    public function fetchParentComment($slug)
    {
        $product_id = Product::where('slug', $slug)->pluck('id')->first();
        $comments = Comment::where('product_id', $product_id)->whereNull('parent')
            ->with('replies.user')->with('user')->orderBy('created_at', 'DESC')->get();

        return response()->json($comments);
    }

    public function autocompleteSearch(Request $request)
    {
        $product = Product::where('name', 'like', '%' . $request['searchquery'] . '%')->take(5)->get();
        $category = Category::where('name', 'like', '%' . $request['searchquery'] . '%')->take(5)->get();
        $brand = Brand::where('name_e', 'like', '%' . $request['searchquery'] . '%')
            ->orWhere('name_f', 'like', '%' . $request['searchquery'] . '%')->take(5)->get();

        return response()->json(['product' => $product, 'category' => $category, 'brand' => $brand->unique('name')]);
    }

    public function search($cat, $name)
    {
        if ($cat == 'دسته ها') {
            $name = json_encode($name);

            return view('site.search_key', compact('name'));
        } else {
            $cat = json_encode($cat);
            $name = json_encode($name);
            return view('site.search', compact('cat', 'name'));
        }
    }

    public function fetchSearchProduct(Request $request)
    {
        if ($request['cat'] == 'محصول') {
            $products = Product::where('name', $request['name'])->with('cat')->get();
        } else {
            $products = Product::where('brand', $request['name'])->with('cat')->get();
        }

        return response()->json($products);
    }

    public function fetchSearchProductCat(Request $request)
    {
        $cat_id = Category::where('name', $request['name'])->pluck('id')->first();

        $products = Product::where('cat_id', $cat_id)->with('cat')->get();

        return response()->json($products);
    }

    public function fetchSearchCat()
    {
        $categories = Category::with('childrenRecursive')->whereNull('parent')->get();

        return response()->json($categories);
    }

    public function searchByCat($cat)
    {
        $x = Category::find($cat);

        $y = Category::where('parent', $x->id)->first();

        if (empty($y)) {
            $name = json_encode($cat);
            return view('site.balance.search_key', compact('name'));
        } else {
            $name = json_encode($cat);
            return view('site.balance.search', compact('name'));
        }
    }

    public function fetchSlider()
    {
        $sliderDesktop = Slider::where('type', 0)->orderBy('created_at', 'desc')->get();
        $sliderMobile = Slider::where('type', 1)->orderBy('created_at', 'desc')->get();

        return response()->json([
            'slider_desktop' => $sliderDesktop,
            'slider_mobile' => $sliderMobile,
        ]);
    }

    public function fetchCartNumber()
    {
        $number = Cart::where('cookie', $_COOKIE['cart'])->sum('number');

        return response()->json($number);
    }

    public function rules()
    {
        return view('site.page.rules');
    }

    public function delivery()
    {
        return view('site.page.delivery');
    }

    public function return()
    {
        return view('site.page.return');
    }

    public function complaint()
    {
        return view('site.page.complaint');
    }

    public function searchBrand($cat, $brand)
    {
        $cat_id = Category::where('name', $cat)->pluck('id')->first();
        $brand_ids = DB::table('brand_category')->where('cat_id', $cat_id)->get('brand_id');

        $brand_ids = json_decode($brand_ids, true);

        $result = array();
        foreach ($brand_ids as $brand_id) {
            array_push($result, $brand_id['brand_id']);

        }

        $result = Brand::whereIn('id', $result)->where(function ($q) use ($brand) {
            $q->where('name', 'like', '%' . $brand . '%')->orWhere('name_f', 'like', '%' . $brand . '%');
        })->get();

        return response()->json($result);
    }

    public function complaintStore(Request $request)
    {
        $complaint = Complaint::create([
            'name' => $request['name'],
            'mobile' => $request['mobile'],
            'ticket' => $request['ticket'],
        ]);

        $complaint->update([
            'shamsi_c' => Verta::instance($complaint->created_at)->format('Y/n/j')
        ]);
    }

    public function suggestFetch(Request $request)
    {
        $suggests = Suggest::with('product.colors')->take(20)->get();

        return response()->json($suggests);
    }

    public function trackFetch()
    {
        $tracks = Track::orderBy('created_at', 'desc')->take(10)->get();

        return response()->json($tracks);
    }

    public function SuggestTimerFetch()
    {
        $time = Key::where('key', 'suggest timer')->pluck('value')->first();

        return response()->json($time);
    }

    public function proSlider1Fetch()
    {
        $catId = Key::where('key', 'index_slider1')->pluck('value')->first();

        $category = Category::where('id', $catId)->with('childrenRecursive')->first();
        self::child($category);
        if (count($this->result) == 0) array_push($this->result, $catId);

        $ripoProductIds = [];
        foreach ($this->result as $catId) {
            $productIds = DB::table('category_product')->where('cat_id', $catId)
                ->pluck('product_id');

            foreach ($productIds as $productId) array_push($ripoProductIds, $productId);
        }
        $products = Product::whereIn('id', $ripoProductIds)->with('colors')
            ->orderBy('priority', 'desc')->take(15)->get();

        return response()->json([
            'cat_id' => $catId,
            'cat_name' => $category->name,
            'products' => $products,
        ]);
    }

    public function proSlider2Fetch()
    {
        $catId = Key::where('key', 'index_slider2')->pluck('value')->first();

        $category = Category::where('id', $catId)->with('childrenRecursive')->first();
        self::child($category);
        if (count($this->result) == 0) array_push($this->result, $catId);

        $ripoProductIds = [];
        foreach ($this->result as $catId) {
            $productIds = DB::table('category_product')->where('cat_id', $catId)
                ->pluck('product_id');

            foreach ($productIds as $productId) array_push($ripoProductIds, $productId);
        }
        $products = Product::whereIn('id', $ripoProductIds)->with('colors')
            ->orderBy('priority', 'desc')->take(15)->get();

        return response()->json([
            'cat_id' => $catId,
            'cat_name' => $category->name,
            'products' => $products,
        ]);
    }

//    public function proSlider3Fetch()
//    {
//        $catId = Key::where('key', 'index_slider3')->pluck('value')->first();
//        $cat = Category::where('id', $catId)->first(['categories.parent AS dad', 'categories.*']);
//
//        $products = $this->fetchProductBycat($catId);
//
//        return response()->json([
//            'cat_name' => $cat->name,
//            'products' => $products,
//        ]);
//    }

    public function proSlider3Fetch()
    {
        $catId = Key::where('key', 'index_slider3')->pluck('value')->first();

        $category = Category::where('id', $catId)->with('childrenRecursive')->first();
        self::child($category);
        if (count($this->result) == 0) array_push($this->result, $catId);

        $ripoProductIds = [];
        foreach ($this->result as $catId) {
            $productIds = DB::table('category_product')->where('cat_id', $catId)
                ->pluck('product_id');

            foreach ($productIds as $productId) array_push($ripoProductIds, $productId);
        }
        $products = Product::whereIn('id', $ripoProductIds)->with('colors')
            ->orderBy('priority', 'desc')->take(15)->get();

        return response()->json([
            'cat_id' => $catId,
            'cat_name' => $category->name,
            'products' => $products,
        ]);
    }



    public function settingsFetch()
    {
        $name = Key::where('key', 'setting_name')->pluck('value')->first();
        $tell = Key::where('key', 'setting_tell')->pluck('value')->first();
        $mobile = Key::where('key', 'setting_mobile')->pluck('value')->first();
        $instagram = Key::where('key', 'setting_instagram')->pluck('value')->first();
        $telegram = Key::where('key', 'setting_telegram')->pluck('value')->first();
        $address = Key::where('key', 'setting_address')->pluck('value')->first();
        $about = Key::where('key', 'setting_about')->pluck('value')->first();
        $logo = Key::where('key', 'setting_logo')->pluck('value')->first();

        return response()->json([
            'name' => $name,
            'tell' => $tell,
            'mobile' => $mobile,
            'instagram' => $instagram,
            'telegram' => $telegram,
            'address' => $address,
            'about' => $about,
            'image' => $logo,
        ]);
    }

    public function existColorFetch($size_id)
    {
        $colorIds = [];

        $res = Exist::where('size_id', $size_id)->get();

        foreach ($res as $item) {
            array_push($colorIds, $item->color_id);
        }

        return response()->json([
            'color_ids' => $colorIds,
        ]);
    }

    public function existSizeFetch($color_id)
    {
        $sizeIds = [];
        $res = Exist::where('color_id', $color_id)->get();

        foreach ($res as $item) {
            array_push($sizeIds, $item->size_id);
        }

        return response()->json([
            'size_ids' => $sizeIds,
        ]);
    }

    public function related($slug)
    {
        $product = Product::where('slug', $slug)->first();

        $pivet = DB::table('category_product')->where('product_id', $product->id)->first();

        $pivetProductId = DB::table('category_product')->where('cat_id', $pivet->cat_id)->take(15)->pluck('product_id');


        $related = Product::whereIn('id', $pivetProductId)->whereNotIn('id', [$product->id])
            ->with('colors')->get();

        return response()->json($related);
    }

    public function existSizeFetchWithoutColor($product_id)
    {
        $sizeIds = [];
        $res = Exist::where('product_id', $product_id)->get();

        foreach ($res as $item) {
            array_push($sizeIds, $item->size_id);
        }

        return response()->json([
            'size_ids' => $sizeIds,
        ]);
    }

    public function userFetch()
    {
        return response()->json(
            auth()->user()
        );
    }

    public function test()
    {
        $pivot = DB::table('category_product')->where('cat_id', 30)->pluck('product_id');
        dd($pivot);
    }

    public function fetchProductBycat($catId)
    {
        $productIds = DB::table('category_product')->where('cat_id', $catId)->pluck('product_id');


        $products = Product::whereIn('id', $productIds)->where('status', 1)
            ->with('colors')->orderBy('created_at', 'desc')->get();

        if ($products->first()) return $products;

        $categories = Category::where('id', $catId)->with('childrenRecursive')->first();
        self::child($categories);

        $productIds = DB::table('category_product')->where('cat_id', $this->result)->pluck('product_id');

        $products = Product::where('status', 1)->whereIn('id', $productIds)->with('colors')
            ->orderBy('created_at', 'desc')->get();

        return $products;
    }

    public function child($categories)
    {
        foreach ($categories->childrenRecursive as $item) {
            self::child($item);
            array_push($this->result, $item->id);
        }
    }

    public function fetchCatWithChild()
    {
        $categories = Category::with('childrenRecursive')->get();

        return response()->json($categories);
    }

    public function fetchSpec($slug)
    {
        $product_id = Product::where('slug', $slug)->pluck('id')->first();
        $specifications = Spec_value::where('product_id', $product_id)->get();

        return response()->json($specifications);
    }

}
