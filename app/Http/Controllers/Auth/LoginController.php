<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\MessagingService;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    public function login()
    {
        if (auth()->user()) {
            return redirect(url('/panel/account'));
        }

        if (isset($_COOKIE['user_id'])) {
            if ($_COOKIE['user_id'] != 0) {
                Auth::loginUsingId($_COOKIE['user_id']);
                return redirect(url('/panel/account'));
            }
        }

        return view('auth.login2');
    }

    public function sendCode(Request $request)
    {
        $code = rand(10000, 99999);
//        MessagingService::sendLoginSms($request['mobile'], $code);

        return response()->json($code);
    }

    public function checkCode(Request $request)
    {
        if ($request['code'] == session()->get('login_code')) {
            $userId = User::where('mobile', $request['mobile'])->pluck('id')->first();
            if (!empty($userId)) {
                Auth::loginUsingId($userId);
                setcookie('user_id', $userId, time() + 60 * 60 * 24 * 365, '/');
                return response()->json('111');
            } else {
                $user = User::create([
                    'mobile' => $request['mobile'],
                ]);

                if (isset($_COOKIE['bani'])) {
                    $user->update([
                        'bani' => 1,
                        'shamsi_c' => Verta::instance($user->created_at)->format('Y/n/j')
                    ]);
                } else {
                    $user->update([
                        'shamsi_c' => Verta::instance($user->created_at)->format('Y/n/j')
                    ]);
                }

                Auth::login($user);
                setcookie('user_id', $user->id, time() + 60 * 60 * 24 * 365, '/');

                return response()->json('111');
            }
        } else {
            return response()->json('000');
        }
    }

    public function loginValidation(Request $request)
    {
        $rules = [
            'mobile' => ['required', 'size:11'],
        ];

        $customMessages = [
            'mobile.required' => 'شماره همراه خود را وارد کنید.',
            'mobile.size' => 'شماره همراه را بطور صحیح وارد کنید.',
        ];

        $this->validate($request, $rules, $customMessages);

        if (Auth::attempt(['mobile' => $request['mobile'], 'password' => $request['password']])) {
            return response()->json(['key' => 'value'], 200);
        } else {
            return response()->json(['error' => 'not match'], 200);
        }
    }

    public function loginStore(Request $request)
    {
        if (!Auth::attempt(['mobile' => $request['mobile'], 'password' => $request['password']]))
            return response()->json(['key' => 'not match']);

    }

    public function newPasswordStore(Request $request)
    {
        User::updateOrCreate(['mobile' => $request['mobile']], [
            'mobile' => $request['mobile'],
            'password' => Hash::make($request['new_pass']),
        ]);

        Auth::attempt([
            'mobile' => $request['mobile'],
            'password' => $request['new_pass']
        ]);
    }

}
