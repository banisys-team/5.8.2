<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function login()
    {
        if (isset(auth('admin')->user()->id)) {
            return redirect('/admin/order/index');
        }
        return view('admin.auth.login');
    }

    public function loginAdmin(Request $request)
    {
        if (Auth::guard('admin')->attempt(['name' => $request['name'], 'password' => $request['password']])) {

            return redirect(url('/admin/order/index'));
        }

        return redirect()->back();
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.auth.login');
    }
}
