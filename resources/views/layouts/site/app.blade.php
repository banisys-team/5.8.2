<!doctype html>
<html lang="fa">

<head>
  <meta charset="UTF-8">
  <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title></title>

  {{-- <link rel="icon" type="image/x-icon" href="/images/favicon.png"> --}}
  <script src="{{ asset('/js/dress/app.js') }}"></script>
  <link rel="stylesheet" href="/layout_kaj/css/bootstrap.min.css">
  <link rel="stylesheet" href="/layout_kaj/css/style.css">
  <link rel="stylesheet" href="/layout_kaj/css/swiper.css">

  @yield('style')
  <style>
    .my-class {
      width: 100%;
      height: 100vh;
      background-color: white;
      opacity: .92;
      position: absolute;
      z-index: 999;
      pointer-events: none;
    }
  </style>
</head>

<body>
  {{-- <a onclick="myFunction()" style="position: fixed;z-index: 9999;">###</a>
  <div id="white-opacity" class="my-class"> --}}

  </div>
  <script>
    setTimeout(() => {
      const body = document.body;
      const html = document.documentElement;
      const height = Math.max(body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight);
      document.getElementById("white-opacity").style.height = `${height}px`;

    }, 500)

    function myFunction() {
      var element = document.getElementById("white-opacity")
      element.classList.toggle("d-none")
    }
  </script>
  <header class="col-12 px-0 pt-3 pt-md-4 pb-0 relative" id="header">

    <div v-if="flagTrusted" class="mobile-menu rtl text-right">
      <div class="row no-gutters" style="background: #1ac977;padding-top: 14px;padding-bottom: 12px;">
        <div class="search search-mobile col-10 px-3">
          <h4 style="color: white;margin: 0;font-size: 20px;padding: 3px 5px;">نماد اعتماد</h4>
        </div>
        <div class="col-2 mt-2">
          <img @click="closeTrusted" class="pl-2 float-left" src="/assets/images/back-mobile-menu2.svg"
            style="width: 28px;margin-left: 8px;">
        </div>
      </div>
      <div class="row no-gutters mt-4">
        <div class="col-12 px-4 mt-3 text-center">
          <img src="/assets/images/enamad.png" style="width: 120px;" alt="">
        </div>
      </div>
    </div>

    <div v-if="flagPages" class="mobile-menu rtl text-right">
      <div class="row no-gutters" style="background: #1ac977;padding-top: 14px;padding-bottom: 12px;">
        <div class="search search-mobile col-10 px-3">
          <h4 style="color: white;margin: 0;font-size: 20px;padding: 3px 5px;">قوانین سایت</h4>
        </div>
        <div class="col-2 mt-2">
          <img @click="closePages" class="pl-2 float-left" src="/assets/images/back-mobile-menu2.svg"
            style="width: 28px;margin-left: 8px;">
        </div>
      </div>
      <div class="row no-gutters mt-4">
        <div class="col-12 px-4 mt-2">
          <a href="/complaint">
            <span>ثبت شکایات</span>
            <img style="width: 12px;padding-top: 6px;opacity: .6" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a href="/return">
            <span>رویه‌های بازگرداندن کالا</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a href="/delivery">
            <span>شرایط تحویل کالا</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a href="/rules">
            <span>قوانین استفاده از سایت</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
      </div>
    </div>

    <div v-if="flagAbout" class="mobile-menu rtl text-right">
      <div class="row no-gutters" style="background: #1ac977;padding-top: 14px;padding-bottom: 12px;">
        <div class="search search-mobile col-10 px-3">
          <h4 style="color: white;margin: 0;font-size: 20px;padding: 3px 5px;">درباره ما</h4>
        </div>
        <div class="col-2 mt-2">
          <img @click="closeAbout" class="pl-2 float-left" src="/assets/images/back-mobile-menu2.svg"
            style="width: 28px;margin-left: 8px;">
        </div>
      </div>
      <div class="row no-gutters mt-4 px-4">
        <div class="search search-mobile col-10 px-3"></div>
        <div class="col-12 mb-5 pb-5">
          <p class="about-footer" style="line-height: 30px;color: #585858">@{{ form.about }}</p>
        </div>
      </div>


    </div>

    <div v-if="flagContact" class="mobile-menu rtl text-right">
      <div class="row no-gutters" style="background: #1ac977;padding-top: 14px;padding-bottom: 12px;">
        <div class="search search-mobile col-10 px-3">
          <h4 style="color: white;margin: 0;font-size: 20px;padding: 3px 5px;">ارتباط با ما </h4>
        </div>
        <div class="col-2 mt-2">
          <img @click="closeContact" class="pl-2 float-left" src="/assets/images/back-mobile-menu2.svg"
            style="width: 28px;margin-left: 8px;">
        </div>
      </div>
      <div class="row no-gutters mt-4 pr-4">
        <div class="search search-mobile col-10 px-3"></div>
        <div class="col-12">
          <ul class="col-lg-2 pr-0 pr-lg-2" style="font-size: 15px;">
            <li class="rtl">
              <span style="color: #f16422;font-size: 14px;">تلفن پشتیبانی :</span>
              <span>@{{ form.tell }}</span>
            </li>
            <li class="mt-4">
              <span style="color: #f16422;font-size: 14px;">موبایل :</span>
              <span>@{{ form.mobile }}</span>
            </li>
            <li class="mt-4">
              <span style="color: #f16422;font-size: 14px;">آدرس :</span>
              <span style="line-height: 30px;">@{{ form.address }}</span>
            </li>
            <li class="text-center mt-5">
              <a :href=`https://www.instagram.com/${form.instagram}` target="_blank">
                <img src="/assets/images/instagram.svg" alt="" style="width:28px;opacity: 0.6">
              </a>
              <a :href=`https://t.me/${form.telegram}` target="_blank" style="margin-left: 15px;margin-right: 25px;">
                <img src="/assets/images/telegram.svg" alt="" style="width:28px;opacity: 0.6">
              </a>
            </li>

          </ul>
        </div>
      </div>


    </div>

    <div v-if="flagCat" class="mobile-menu rtl text-right">
      <div class="row no-gutters" style="background: #1ac977;padding-top: 14px;padding-bottom: 12px;">
        <div class="search search-mobile col-10 px-3">
          <div style="background-color: #f5f5f5;border-radius: 100px;padding: 5px 20px;border: 1px solid #eee;">
            <img src="/assets/images/search.svg" alt="" style="width: 20px">
            <input @keyup="autoCompleteMobile" id="search-input"
              style="border: none;background-color: transparent;text-align: right;direction: rtl;padding: 0px 18px;outline: none;width: 85%"
              v-model="searchquery" placeholder="جستجو محصول،دسته..." type="text">
          </div>
          <i v-if="data_results.product.length || data_results.category.length || data_results.brand.length"
            @click="searchClose"
            style="position: absolute;left: 35px;top: 9px;font-size: 16px;cursor: pointer;color: rgb(175, 175, 188);font-style: normal;">✖</i>
          <div v-if="data_results.product.length || data_results.category.length || data_results.brand.length"
            style="position: absolute;width: 100%;z-index: 100;top: 35px;direction: rtl;">
            <ul class="list-group down-select pr-0">
              <li class="list-group-item" v-for="(result,index) in data_results.product"
                @click="selectResult(result.name,'product')" style="text-align: right;cursor: pointer">
                <span class="auto_search">محصول</span>
                <span class="auto_search2">@{{ result.name }}</span>
              </li>
              <li class="list-group-item" v-for="(result,index) in data_results.category"
                @click="selectResult(result.id,'cat')" style="text-align: right;cursor: pointer">

                <span class="auto_search">دسته ها</span>
                <span class="auto_search2">@{{ result.name }}</span>
              </li>
              <li class="list-group-item" v-for="(result,index) in data_results.brand"
                @click="selectResult(result.name_e,'brand')" style="text-align: right;cursor: pointer">
                <span class="auto_search">برند</span>
                <span class="auto_search2">@{{ result.name_e }}</span>
                <span style="margin-right:50px;font-size: 14px;color: #b9b9b9;">@{{ result.name_f }}</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-2 mt-2">
          <img @click="flagCat = false" class="pl-2 float-left" src="/assets/images/back-mobile-menu2.svg"
            style="width: 28px;margin-left: 8px;">
        </div>
      </div>

      <div>
        <ul v-if="flag1" class="mt-3 p-0">
          <li id="root-cat" v-for="root in roots" style="border-bottom: 2px solid #e8e8e8;" class="py-3 px-4"
            @click="fetchChild(root.id,root.name,'root')">
            @{{ root.name }}
            <img class="angle-left" src="/assets/images/left-arrow3.svg">
          </li>
        </ul>

        <ul v-if="flag2" class="mt-4">
          <li id="back" @click="back" style="color: #dc3545" class="mb-2">
            <i class="right">⮞</i>
            <span style="margin-right: 6px;padding-top: 3px;display: inline-block;">
              @{{ holder.parentName }}
            </span>
          </li>
          <li id="fix" @click="fixCat" class="mb-2 pb-2 pr-4">
            <a :href=`/auto/search/cat/${holder.selfId}`>
              <span style="vertical-align: middle;color: #949494">همه موارد این دسته</span>
              <i id="angle-left" class="right"
                style="font-size: 20px;display: inline-block;padding-right: 3px;color: #949494">›</i>
            </a>
          </li>
          <li v-for="child in children" style="line-height: 35px;" class="pb-2 pr-4 children-item">
            <a :href=`/auto/search/cat/${child.id}`>
              @{{ child.name }}
            </a>
            <img v-if="child.children_recursive.length" class="angle-left" src="/assets/images/left-arrow3.svg">
          </li>
        </ul>
      </div>
    </div>

    <div v-if="flagProfile" class="mobile-menu rtl text-right">

      <div class="d-flex justify-content-end px-4" style="background: #1ac977;height:120px">
        <img src="/assets/images/profile-back.png" alt="banimode" class="profile-back-img">
        <img @click="flagProfile = false" src="/assets/images/back-mobile-menu2.svg"
          style="width: 18px;margin-bottom: 40px;">
      </div>

      <div class="row no-gutters mt-2 pt-4"
        style="border-radius: 25px;
                    margin-top: -22px !important;
                    background: white;">
        <div class="col-12 px-4 mt-2">
          <a href="/panel/account">
            <img style="width: 18px;margin-top: -3px;" src="/assets/images/user4.svg" class="ml-3">
            <span>اطلاعات حساب کاربری</span>
            <img style="width: 12px;padding-top: 6px;opacity: .6" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a href="/panel/orders">
            <img style="width: 18px" src="/assets/images/order2.svg" class="ml-3">
            <span>سفارشات</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a href="/panel/reserve">
            <img style="width: 24px;margin-right: -3px;" src="/assets/images/reserve.svg" class="ml-3">
            <span>سفارشات رزرو</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a @click="openContact">
            <img style="width: 24px;margin-right: -3px;" src="/assets/images/support.svg" class="ml-3">
            <span>ارتباط با ما</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a @click="openAbout">
            <img style="width: 25px;margin-right: -3px;" src="/assets/images/group.svg" class="ml-3">
            <span>درباره ما</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a @click="openPages">
            <img style="width: 24px;margin-right: -3px;" src="/assets/images/balance.svg" class="ml-3">
            <span>قوانین سایت</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
        <div class="col-12 px-4 mt-3 pt-3" style="border-top: 1px solid #e2e2e2;">
          <a @click="openTrusted">
            <img style="width: 25px;margin-right: -3px;" src="/assets/images/trusted.svg" class="ml-3">
            <span>نماد اعتماد</span>
            <img style="width: 12px;opacity:0.6;padding-top: 6px;" src="/assets/images/left-arrow3.svg"
              class="float-left">
          </a>
        </div>
      </div>

    </div>

    <a class="logo-mobile pb-3" href="{{ url('/') }}">
      <img :src=`/images/${logo}` alt="" style="width: 160px">
    </a>

    <div class="nav-mobile justify-content-around flex-row-reverse align-items-center ">

      <a href="/" class="d-flex align-items-center flex-column">
        <img style="width: 20px" src="/assets/images/home.svg">
        <span class="pt-2">خانه</span>
      </a>

      <a class="d-flex align-items-center flex-column" @click="openCat">
        <img style="width: 20px" src="/assets/images/searching.svg">
        <span class="pt-2">جستجو و دسته بندی</span>
      </a>

      <a href="/cart" class="d-flex align-items-center flex-column">
        <img style="width: 20px" src="/assets/images/mobile-menu-cart.svg">
        <span class="pt-2">سبد خرید</span>
      </a>

      <a class="d-flex align-items-center flex-column" @click="openProfile">
        <img style="width: 20px" src="/assets/images/user.svg">
        <span class="pt-2">پروفایل</span>
      </a>

    </div>

    <div class="header d-none d-lg-flex flex-row-reverse justify-content-between align-items-center px-3">
      <div class="col-lg-6 px-0 text-right d-flex flex-row-reverse justify-content-between align-items-center">
        <div class="logo pr-3">
          <a href="{{ url('/') }}">
            <img :src=`/images/${logo}` alt="" style="width: 200px">
          </a>
        </div>
        <div class="search col-10 px-0 mr-5 d-none d-lg-block">
          <div style="background-color: #f5f5f5;border-radius: 100px;padding: 5px 20px;border: 1px solid #eee;">
            <input @keyup="autoComplete" id="search-input"
              style="border: none;background-color: transparent;text-align: right;direction: rtl;padding: 5px 12px;outline: none;width: 80%"
              v-model="searchquery" placeholder="جستجوی دسته یا اسم محصول ..." type="text">
            <img src="/assets/images/search.svg" alt="" style="width: 22px">
          </div>
          <i v-if="data_results.product.length || data_results.category.length || data_results.brand.length"
            @click="searchClose"
            style="position: absolute;left: 20px;top: 12px;font-size: 16px;cursor: pointer;color: rgb(175, 175, 188);font-style: normal;">✖</i>
          <div v-if="data_results.product.length || data_results.category.length || data_results.brand.length"
            style="position: absolute;width: 100%;z-index: 100;top: 43px;direction: rtl;left: 0;">
            <ul class="list-group down-select pr-0">
              <li class="list-group-item" v-for="(result,index) in data_results.product"
                @click="selectResult(result.name,'product')" style="text-align: right;cursor: pointer">
                <span class="auto_search">محصول</span>
                <span class="auto_search2">@{{ result.name }}</span>
              </li>
              <li class="list-group-item" v-for="(result,index) in data_results.category"
                @click="selectResult(result.id,'cat')" style="text-align: right;cursor: pointer">

                <span class="auto_search">دسته ها</span>
                <span class="auto_search2">@{{ result.name }}</span>
              </li>
              <li class="list-group-item" v-for="(result,index) in data_results.brand"
                @click="selectResult(result.name_e,'brand')" style="text-align: right;cursor: pointer">
                <span class="auto_search">برند</span>
                <span class="auto_search2">@{{ result.name_e }}</span>
                <span style="margin-right:50px;font-size: 14px;color: #b9b9b9;">@{{ result.name_f }}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="d-flex cart-btn col-lg-4 rtl flex-row-reverse align-items-center">
        <a href="/cart" class="primary-button rtl">
          <img src="/assets/images/cart.svg" alt="" style="width:20px;margin-left: 5px;">
          سبد خرید
        </a>
        <ul class="d-inline border_list list_none header_dropdown text-center text-md-right mb-0 ml-4"
          style="direction: rtl">
          @auth
            <div>
              <a href="/panel/account"
                style="font-size: 13px;background-color: #84848414;color: rgb(119, 119, 119);padding: 9px 20px;border-radius: 7px;">
                <img src="/assets/images/user5.svg" alt="" style="width: 20px;margin-left: 5px;">
                پروفایل
              </a>
            </div>
          @else
            <div>
              <a href="/login"
                style="font-size: 13px;background-color: #84848414;color: rgb(119, 119, 119);padding: 9px 10px;border-radius: 7px;">
                <img src="/assets/images/user5.svg" alt="" style="width: 20px;margin-left: 5px;">
                ورود به حساب کاربری
              </a>
            </div>
          @endauth
        </ul>
      </div>
    </div>

    <nav class="mt-4 d-none d-lg-block text-center px-3 pb-3" style="border-bottom: 1px solid #e9ecef;">
      <ul class="d-flex flex-row-reverse justify-content-start mb-0">
        <template v-for="root in roots">
          <li class="nav-item dropdown">
            <a :href=`/auto/search/cat/${root.id}` class="nav-link relative" style="cursor:pointer">

              <img v-if="root.children_recursive.length" style="width: 8px;margin: 5px 2px 0 0;"
                src="/assets/images/arrow-down.svg" alt="">

              @{{ root.name }}
            </a>
            <div class="cat-dropdown" v-if="root.children_recursive.length">
              <template v-for="cat in root.children_recursive">
                <a class="dropdown-item" :href=`/auto/search/cat/${cat.id}`>@{{ cat.name }}</a>
              </template>
            </div>
          </li>
        </template>
      </ul>
    </nav>
  </header>

  @yield('content')

  <footer class="mt-5 pb-5 py-lg-5 px-0 px-lg-3 col-12 text-right d-none d-lg-block" id="footer"
    style="background: white;">

    {{-- Desktop Footer  --}}

    <div class="text-right col-12 d-none d-lg-flex flex-wrap flex-row-reverse justify-content-between ">
      <ul class="col-lg-2 pr-0 pr-lg-2">
        <h4>ارتباط با ما </h4>
        <li class="rtl">
          <span>تلفن پشتیبانی :</span>
          <span>@{{ form.tell }}</span>
        </li>
        <li>
          <span>موبایل :</span>
          <span>@{{ form.mobile }}</span>
        </li>
        <li style="margin-top: 25px">
          <a :href=`https://www.instagram.com/${form.instagram}` target="_blank">
            <img src="/assets/images/instagram.svg" alt="" style="width:28px;opacity: 0.6">
          </a>
          <a :href=`https://t.me/${form.telegram}` target="_blank" style="margin-left: 15px;margin-right: 15px;">
            <img src="/assets/images/telegram.svg" alt="" style="width:28px;opacity: 0.6">
          </a>

        </li>
      </ul>
      <ul class="col-lg-2 mt-5 mt-md-0">
        <h4>دسترسی ها </h4>
        <li>
          <a href="/return">
            رویه‌های بازگرداندن کالا
          </a>
        </li>
        <li>
          <a href="/delivery">
            شرایط تحویل کالا
          </a>
        </li>
        <li>
          <a href="/rules">
            قوانین استفاده از سایت
          </a>
        </li>
        <li>
          <a href="/complaint">
            ثبت شکایات
          </a>
        </li>

      </ul>
      <ul class="col-lg-2 mt-5 mt-md-0">
        <h4>نماد اعتماد</h4>
        <li>
          <img src="/assets/images/enamad.png" style="width: 120px;" alt="">
        </li>
      </ul>
      <ul class="col-lg-5 mt-5 mt-md-0 rtl">
        <li style="color:#555;margin-top: 5px;">
          <span>آدرس :</span>
          <span>@{{ form.address }}</span>
        </li>

        <li style="border-top: 1px solid #999;padding: 16px 0 0 0;">
          <p style="font-size: 17px;color: black;margin-bottom: 8px;"> فروشگاه اینترنتی مد و لباس
            @{{ form.name }}</p>
          <p class="about-footer about" style="line-height: 27px;">@{{ form.about }}</p>
          <span v-if="flagAbout" @click="aboutShowMore"
            style="border-radius:8px;border: solid 1px #929292;padding: 7px 15px;color: #1c1c1c;font-size: 13px;cursor: pointer;">مشاهده
            بیشتر
            <img src="/assets/images/left-arrow.svg" alt="" style="margin: 4px 4px 0 0;width: 18px">
          </span>
          <span v-if="!flagAbout" @click="aboutShowLess"
            style="border: solid 1px #929292;padding: 5px 10px;color: #212121;font-size: 13px;cursor: pointer;">بستن</span>
        </li>
      </ul>
    </div>
  </footer>

  <script src="/layout_kaj/js/jquery.min.js"></script>
  <script src="/dress/assets/js/popper.js"></script>
  <script src="/dress/assets/js/bootstrap.min.js"></script>
  <script src="/layout_kaj/js/swiper.js"></script>

  <script>
    new Vue({
      el: '#header',
      data: {
        flag: false,
        products: {},
        searchquery: '',
        data_results: {
          product: {},
          category: {},
          brand: {},
        },
        focus: 0,
        carts: [],
        countCart: 0,
        totalCart: 0,
        roots: [],
        form: {},
        logo: '',
        flagCat: false,
        flagProfile: false,
        flagContact: false,
        flagAbout: false,
        flagPages: false,
        flagTrusted: false,
        flag1: true,
        flag2: false,
        children: [],
        holder: {
          selfName: 'بازگشت',
          selfId: '',
          parentName: '',
          parentId: '',
          grandName: '',
          grandId: '',
        },
      },
      methods: {
        openContact() {
          this.flagContact = true
          this.flagProfile = false
        },
        closeContact() {
          this.flagContact = false
          this.flagProfile = true
        },
        openAbout() {
          this.flagAbout = true
          this.flagProfile = false
        },
        closeAbout() {
          this.flagAbout = false
          this.flagProfile = true
        },
        openPages() {
          this.flagPages = true
          this.flagProfile = false
        },
        closePages() {
          this.flagPages = false
          this.flagProfile = true
        },
        openCat() {
          this.flagProfile = false
          this.flagCat = true
        },
        openProfile() {
          this.flagProfile = true
          this.flagCat = false
        },
        openTrusted() {
          this.flagProfile = false
          this.flagTrusted = true
        },
        closeTrusted() {
          this.flagProfile = true
          this.flagTrusted = false
        },
        fetchRootCat() {
          let _this = this
          axios.get(`/category/fetch/cat/root`).then(res => {
            _this.roots = res.data
          })
        },
        searchClose() {
          this.flag = false;
          this.data_results.product = {};
          this.data_results.category = {};
          this.data_results.brand = {};
          this.searchquery = '';
        },
        autoComplete(event) {
          event.preventDefault();
          if (this.searchquery.length === 0) {
            this.data_results.product = [];
            this.data_results.category = [];
            this.data_results.brand = [];
            this.flag = false;
            return false;
          }
          let li_count = $('.search .down-select li').length

          switch (event.keyCode) {
            case 13:
              let name = this.searchquery
              let cat = $('.select_search .auto_search')[0].innerText

              if (cat === 'دسته ها') {
                type = 'cat'
              }
              if (cat === 'محصول') {
                type = 'product'
              }
              if (cat === 'برند') {
                type = 'brand'

              }
              this.selectResult(name, type);
              break;

            case 38:
              if (this.focus === 1) {
                return false
              }
              this.focus--
              $(`.down-select li`).removeClass("select_search")
              $(`.down-select li:nth-child(${this.focus})`).addClass("select_search")
              this.searchquery = $(`.down-select li:nth-child(${this.focus}) .auto_search2`)[0].innerText

              return false

            case 40:

              if (this.focus === li_count) {
                return false;
              }

              this.focus++;
              $(`.down-select li`).removeClass("select_search");
              $(`.down-select li:nth-child(${this.focus})`).addClass("select_search")

              this.searchquery = $(`.down-select li:nth-child(${this.focus}) .auto_search2`)[0].innerText
              return false
            case 37:
              return false

            case 39:
              return false

          }
          let vm = this
          if (this.searchquery.length > 2) {
            this.flag = true
            axios.get('/autocomplete/search', {
              params: {
                'searchquery': this.searchquery,
              }
            }).then(res => {
              vm.data_results.product = res.data.product
              vm.data_results.category = res.data.category
              vm.data_results.brand = res.data.brand
              vm.focus = 0
            })
          }
        },
        autoCompleteMobile(event) {
          event.preventDefault()
          if (this.searchquery.length === 0) {
            this.data_results.product = [];
            this.data_results.category = [];
            this.data_results.brand = [];
            this.flag = false;
            return false;
          }
          let li_count = $('.search-mobile .down-select li').length

          switch (event.keyCode) {
            case 13:
              let name = this.searchquery
              let cat = $('.select_search .auto_search')[0].innerText

              if (cat === 'دسته ها') {
                type = 'cat'
              }
              if (cat === 'محصول') {
                type = 'product'
              }
              if (cat === 'برند') {
                type = 'brand'

              }
              this.selectResult(name, type);
              break;

            case 38:
              if (this.focus === 1) {
                return false
              }
              this.focus--
              $(`.down-select li`).removeClass("select_search")
              $(`.down-select li:nth-child(${this.focus})`).addClass("select_search")
              this.searchquery = $(`.down-select li:nth-child(${this.focus}) .auto_search2`)[0].innerText

              return false

            case 40:

              if (this.focus === li_count) {
                return false;
              }

              this.focus++;
              $(`.down-select li`).removeClass("select_search");
              $(`.down-select li:nth-child(${this.focus})`).addClass("select_search")

              this.searchquery = $(`.down-select li:nth-child(${this.focus}) .auto_search2`)[0].innerText
              return false
            case 37:
              return false

            case 39:
              return false

          }
          let vm = this
          if (this.searchquery.length > 2) {
            this.flag = true
            axios.get('/autocomplete/search', {
              params: {
                'searchquery': this.searchquery,
              }
            }).then(res => {
              vm.data_results.product = res.data.product
              vm.data_results.category = res.data.category
              vm.data_results.brand = res.data.brand
              vm.focus = 0
            })
          }
        },
        selectResult(name, type) {
          window.location.href = `/auto/search/${type}/${name}`
        },
        fetchCart() {
          let data = this;
          data.totalCart = 0;
          axios.get('/fetch/cart').then(res => {
            data.carts = res.data;
            data.countCart = res.data.length;
            for (const [key, value] of Object.entries(res.data)) {
              data.totalCart += value.total;
            }
          });
        },
        numberFormat(price) {
          return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        deleteCart(id) {
          let data = this;
          swal.fire({
            text: "آیا از پاک کردن اطمینان دارید ؟",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'بله',
            cancelButtonText: 'لغو',
          }).then((result) => {
            if (result.value) {
              axios.get(`/cart/delete/${id}`).then(res => {
                data.fetchCart();
              });
            }
          });
        },
        fetchSettings() {
          let vm = this
          axios.get(`/setting/fetch/all`).then(res => {
            vm.form = res.data
            vm.logo = res.data.image
          })
        },
        fetchChild(id, name, length) {
          this.flagPriority = false
          this.holder.grandName = this.holder.parentName
          this.holder.grandId = this.holder.parentId

          this.holder.parentName = this.holder.selfName
          this.holder.parentId = this.holder.selfId

          this.holder.selfName = name
          this.holder.selfId = id

          if (length === 0) {
            this.fixCat()
            this.holder.selfName = 'ریشه'
            // this.holder.selfId = ''
            this.holder.parentName = ''
            this.holder.parentId = ''
            this.holder.grandName = ''
            this.holder.grandId = ''
            this.flag1 = true
            this.flag2 = false
            this.fetchCategories()
            return
          }

          this.flag1 = false
          this.flag2 = true

          this.children = this.categories.filter(cat => cat.parent === id)
        },
        back() {
          let http = axios.create({
            baseURL: 'http://localhost:8000/api/admin'
          })
          http.defaults.withCredentials = true

          if (this.holder.parentName === 'بازگشت') {
            this.flag1 = true
            this.flag2 = false
            this.holder.selfName = 'بازگشت'
            this.holder.selfId = ''
            this.holder.parentName = ''
            this.holder.parentId = ''
            this.holder.grandName = ''
            this.holder.grandId = ''
          } else {
            this.children = this.categories.filter(cat => cat.parent === this.holder.parentId)

            this.holder.selfName = this.holder.parentName
            this.holder.selfId = this.holder.parentId

            this.holder.parentName = this.holder.grandName
            this.holder.parentId = this.holder.grandId

            let x = this.categories.filter(cat => cat.id === this.holder.grandId)
            if (x[0]) {
              let y = this.categories.filter(cat => cat.id === x[0].parent)
              if (y[0]) {
                this.holder.grandName = y[0].name
                this.holder.grandId = y[0].id
              } else {
                this.holder.grandName = 'بازگشت'
                this.holder.grandId = ''
              }
            }
          }
        },
        fixCat() {
          this.form.parent = this.holder.selfName
          this.flag = false
        },
        fixRoot() {
          this.form.catName = 'بازگشت';
          this.flag = false;
        },
        fetchCategories() {
          let _this = this
          axios.get(`/category/fetch/with/child`).then(res => {
            _this.categories = res.data
            _this.roots = res.data.filter(cat => cat.parent === null)
          })
        },
      },
      mounted() {
        this.fetchCategories()
        this.fetchSettings()
      }
    })
    new Vue({
      el: '#footer',
      data: {
        form: {},
        flagAbout: true,
      },
      methods: {
        aboutShowMore() {
          $(".about-footer").removeClass("about")
          this.flagAbout = false
        },
        aboutShowLess() {
          $(".about-footer").addClass("about")
          this.flagAbout = true
        },
        fetchSettings() {
          let vm = this
          axios.get(`/setting/fetch/all`).then(res => {
            vm.form = res.data
            window.logo_g = res.data.image
          })
        },
      },
      mounted() {
        this.fetchSettings()
      }
    })
  </script>
  @yield('script')
</body>

</html>
