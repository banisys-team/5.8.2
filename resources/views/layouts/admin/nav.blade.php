<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom py-1">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#">
                <img src="/assets/images/menu.svg" style="width: 18px;opacity: .6">
            </a>
        </li>
    </ul>

    <ul class="navbar-nav mr-auto">
        <form method="POST" action="{{ route('admin.auth.logout') }}">
            @csrf
            <button class="nav-link"
            style="background-color: unset;border: unset;color: #dc3545;cursor: pointer"
                    type="submit">
                <img src="/assets/images/power.svg" style="width:19px;opacity: .8">
            </button>
        </form>
    </ul>
</nav>
