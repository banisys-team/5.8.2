<aside class="main-sidebar" style="background-color: #343a40">
  <div class="sidebar" style="direction: ltr;height: 100vh;padding-left: 0">
    <div style="direction: rtl">
      <nav class="mt-4">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" id="ul-side-bar"
          data-accordion="false">
          <li id="side_category" class="nav-item has-treeview">
            <a href="#" class="nav-link ">
              <img src="/assets/images/category.svg" style="width: 20px;margin-left: 4px;margin-bottom: 6px;">
              <p>
                دسته بندی نوع کالا
              </p>
              <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item inner">
                <a id="side_category_add" href="{{ url('/admin/category') }}" class="nav-link">
                  <p style="font-size: 12px;">مدیریت دسته ها</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview" id="side_brand">
            <a href="#" class="nav-link">
              <img src="/assets/images/brand.svg" style="width: 17px;margin-left: 4px;margin-bottom: 6px;">
              <p>
                برند ها
                <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item inner">
                <a href="{{ url('/admin/brand') }}" class="nav-link" id="side_brand_add">
                  <p style="font-size: 12px;">مدیریت برندها</p>
                </a>
              </li>
            </ul>
          </li>

          <li id="side_product" class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <img src="/assets/images/product.svg" style="width: 21px;margin-left: 4px;margin-bottom: 5px;">
              <p>
                محصولات
                <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item inner">
                <a href="{{ url('/admin/product/create') }}" class="nav-link" id="side_product_add">
                  <p style="font-size: 12px;">افزودن</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/product/index') }}" class="nav-link" id="side_product_index">
                  <p style="font-size: 12px;">لیست</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/product/suggest/index') }}" class="nav-link" id="side_product_suggest">
                  <p style="font-size: 12px;">شگفت انگیز ها</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/exist/product/code') }}" class="nav-link" id="side_product_code">
                  <p style="font-size: 12px;">کد محصول</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview" id="side_order">
            <a href="#" class="nav-link">
              <img src="/assets/images/order.svg" style="width:20px;margin-left: 4px;margin-bottom: 6px;">
              <p>
                سفارشات
                <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item inner">
                <a href="{{ url('/admin/order/index') }}" class="nav-link" id="side_order_index">
                  <p style="font-size: 12px;">لیست سفارشات</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/order/reserve/users') }}" class="nav-link" id="side_order_user">
                  <p style="font-size: 12px;">درخواستی ها از سفارشات رزرو</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/order/reserve/history') }}" class="nav-link" id="side_order_history">
                  <p style="font-size: 12px;">پیگیری هزینه ارسال</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview" id="side_setting">
            <a href="#" class="nav-link">
              <img src="/assets/images/settings.svg" style="width:18px;margin-left: 4px;margin-bottom: 6px;">
              <p style="font-size: 12px;">
                تنظیمات
                <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item inner">
                <a href="{{ url('/admin/setting') }}" class="nav-link" id="side_setting_crud">
                  <p style="font-size: 12px;">پروفایل سایت</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/slider/crud') }}" class="nav-link" id="side_slider_create">
                  <p style="font-size: 12px;">اسلایدر صفحه اصلی</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/index/create') }}" class="nav-link" id="side_index_add">
                  <p style="font-size: 12px;">محصولات صفحه اصلی</p>
                </a>
              </li>
              <li class="nav-item inner">
                <a href="{{ url('/admin/setting/delivery') }}" class="nav-link" id="side_delivery_update">
                  <p style="font-size: 12px;">هزینه ارسال</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview" id="side_page">
            <a href="#" class="nav-link">
              <img src="/assets/images/page.svg" style="width:18px;margin-left: 4px;margin-bottom: 6px;">
              <p>
                صفحات
                <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item inner">
                <a href="{{ url('/admin/page/edit') }}" class="nav-link" id="side_page_edit">
                  <p style="font-size: 12px;">ویرایش</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview" id="side_complaint">
            <a href="#" class="nav-link">
              <img src="/assets/images/chat.svg" style="width:18px;margin-left: 4px;margin-bottom: 6px;">
              <p style="font-size: 12px;">
                شکایات
                <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item inner">
                <a href="{{ url('/admin/complaint/index') }}" class="nav-link" id="side_complaint_create">
                  <p style="font-size: 12px;">لیست</p>
                </a>
              </li>
            </ul>
          </li>

          {{-- <li class="nav-item has-treeview" id="side_user">
            <a href="#" class="nav-link">
              <img src="/assets/images/user2.svg" style="width:18px;margin-left: 4px;margin-bottom: 6px;">
              <p style="font-size: 12px;">
                کاربران
                <img src="/assets/images/left-arrow2.svg" style="width: 8px;margin-right: 5px;">
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item inner">
                <a href="{{ url('/admin/user/crud') }}" class="nav-link" id="side_user_crud">
                  <p style="font-size: 12px;">مدیریت کاربران</p>
                </a>
              </li>
            </ul>
          </li> --}}

          {{-- <li class="nav-item has-treeview" id="side_tutorial">
            <a href="/admin/banisys" class="nav-link">
              <img src="/assets/images/play-button.png" style="width:18px;margin-left: 4px;margin-bottom: 6px;">
              <p style="font-size: 12px;">
                فیلم های آموزشی
              </p>
            </a>
          </li> --}}

          {{-- <li class="nav-item has-treeview" id="side_bani">
            <a href="/admin/banisys" class="nav-link">
              <img src="/assets/images/logo.png" style="width:18px;margin-left: 4px;margin-bottom: 6px;">
              <p style="font-size: 12px;">
                بانی سیستم
              </p>
            </a>
          </li> --}}
        </ul>
      </nav>
    </div>
  </div>
</aside>
