@extends('layouts.site.app')
@section('content')
    <div class="container mt-5 pt-2 text-right rtl" id="content">
        <div class="row">
            @include('layouts.site.aside')
            <div class="col-lg-9 ">
                <div class="d-flex justify-content-between align-items-center col-12">
                    <div class=" text-center d-none d-lg-block" v-if="!flag2">
                        <p style="font-size: 13px;color:#777;margin-bottom:15px;">کل سفارشات زیر را برایم بفرست</p>
                        <a href="/panel/reserve/delivery/zarinpal" class="wanted">
                            پرداخت هزینه ارسال
                        </a>
                    </div>
                </div>

                <div class="row mt-5" v-if="orders.length">
                    <div class="col-12 d-lg-none">
                        <p style="color: #ffffff;font-size: 17px;text-align: center;background: #1ac977;padding: 8px 0;">لیست سفارشات رزرو</p>
                    </div>
                    <div class="col-12">
                        <table class="table">
                            <thead style="color: #939393">
                            <tr>
                                <th scope="col">شماره فاکتور</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="order in orders">
                                <td>@{{ order.id}}</td>
                                <td>@{{ fetchStatus(order.status) }}</td>
                                <td>@{{order.shamsi_c}}</td>
                                <td>
                                    <a :href=`/factor/${order.id}` target="_blank" class="btn-factor">
                                       فاکتور
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class=" text-center  d-lg-none mt-5" v-if="!flag2">
                            <p style="font-size: 13px;color:#777;margin-bottom:15px;">کل سفارشات زیر را برایم بفرست</p>
                            <a href="/panel/reserve/delivery/zarinpal" class="wanted">
                                پرداخت هزینه ارسال
                            </a>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <pagination :data="orders" @pagination-change-page="fetchReserves"></pagination>
                </div>

                <div class="row mt-5" v-if="!orders.length">
                    <div class="col-12">
                        <div class="alert alert-info pb-4" role="alert" style="text-align: center">
                            <p class="mb-4">
                                لیست سفارشات رزرو شما خالی است
                            </p>
                            <a href="/panel/orders" style="background: #1ac977;color: white;padding:15px 60px;border-radius: 10px;font-size: 13px;">
                                مشاهده سفارشات ارسالی
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                orders: [],
                user: [],
                form: {
                    image: '',
                },
                flag: true,
                flag2: false,
            },
            methods: {
                fetchReserves() {
                    let _this = this;
                    axios.get('/panel/reserve/fetch').then(res => {
                        _this.orders = res.data
                        _this.emptyOrder()
                    })
                },
                factor(id) {
                    window.open(`/factor/${id}`, '_blank');
                },
                fetchStatus(id) {
                    if (id === 0) {
                        return "در صف بررسی"
                    }
                    if (id === 1) {
                        return "ارسال شد"
                    }
                    if (id === 2) {
                        return "لغو شد"
                    }
                    if (id === 3) {
                        return "آماده ارسال"
                    }
                    if (id === 4) {
                        return "رزرو شد"
                    }
                },
                exit() {
                    this.$refs.formExit.submit();
                },
                emptyOrder() {
                    if (!this.orders.length) {
                        this.flag2 = true
                    }
                },
                fetchUser() {
                    let data = this;
                    axios.get(`/panel/fetch/user`).then(res => {
                        data.user = res.data;
                    })
                },
            },
            mounted () {
                this.fetchReserves()
                this.fetchUser()
            }
        })
        $("#reserve-btn").addClass('active-menu')
    </script>

@endsection

@section('style')
    <style>
    .btn-factor {
    background: #1ac977;
    padding: 4px 13px;
    width: 100%;
    border-radius: 8px;
    color: white;
    cursor: pointer;
    font-size: 13px;
}
        #side-reserve {
            color: #1ac977 !important;
            font-weight: bold;
        }
        .reserve-title{
            font-weight: bold;
            font-size: 22px;
            color: #444;
        }
        .wanted{
            font-size: 14px;
            padding: 10px 40px;
            color: white;
            background-color: #1ac977;
            border-radius: 8px;
        }
        .wanted:hover{
            color: white;
        }
        .alert-info{
            background-color: transparent !important;
            border-color: transparent !important;
        }
        @media  only screen and (max-width:768px) {
            .table thead th{
                text-align:center;
                font-weight:400;
                color:#666;

            }
            .table td, .table th{
                text-align: center;
            }

        }
    </style>
@endsection

