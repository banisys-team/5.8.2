@extends('layouts.site.app')
@section('content')
    <div class="container mt-lg-2 mt-lg-5 pt-lg-2 text-right rtl" id="content">
        <div class="row">
            @include('layouts.site.aside')
            <div class="col-lg-9">
                <div class="row" v-if="orders.data.length">
                    <div class="col-12 d-lg-none p-0">
                        <p style="color: #ffffff;font-size: 17px;text-align: center;background: #1ac977;padding: 8px 0;margin: 0">
                            لیست سفارشات</p>
                    </div>
                    <div class="col-12 mt-5 mt-lg-0">
                        <table class="table">
                            <thead style="color: #939393">
                            <tr>
                                <th scope="col">شماره فاکتور</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">تاریخ</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="order in orders.data">
                                <td>@{{ order.id}}</td>
                                <td>@{{ fetchStatus(order.status) }}</td>
                                <td>@{{order.shamsi_c}}</td>
                                <td>
                                    <a :href=`/factor/${order.id}` target="_blank"
                                       class="btn-factor">
                                        فاکتور
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <pagination :data="orders" @pagination-change-page="fetchOrders"></pagination>
                </div>
                <div class="row mt-4" v-if="!orders.data.length">
                    <div class="col-12">
                        <div class="alert alert-info py-4" role="alert" style="text-align: center">
                            <p class="mb-4">
                                لیست سفارشات ارسالی شما خالی است
                            </p>
                            <a href="/panel/reserve"
                               style="background: #1ac977;color: white;padding:15px 60px;border-radius: 10px;font-size: 13px;">
                                مشاهده سفارشات رزرو
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div v-show="modal">
            <div id="modal-back" @click="flagModal = false"></div>
            <div class="modal-content col-12 col-lg-6 pb-5 pt-3 rtl">
                <div class="row">
                    <div class="col-12 text-center relative">
                        <span class="btn-close" @click="flagModal = false">✖</span>
                        <img src="/assets/images/postal-code.svg" alt="" class="mt-4"
                             style="width: 100px;margin: auto">
                    </div>
                    <div class="col-12">
                        <p class="text-center mt-4" style="font-size: 16px;color: #777;">
                            جهت نهایی شدن سفارش در قسمت
                            <span style="color: #1ac977">اطلاعات حساب کاربری</span>
                            کد پستی خود را وارد کنید
                        </p>
                        <p class="text-center mt-3" style="font-size: 14px;color: #ababab;">
                            در صورت نداشتن کد پستی با پشتیبانی ارتباط بگیرید
                        </p>
                    </div>
                    <div class="col-12 text-center mt-5">
                        <a href="/panel/account"
                           class="px-4 py-3 py-lg-2 add-to-cart-btn mr-lg-3">
                            اطلاعات حساب کاربری
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                orders: [],
                user: [],
                form: {
                    image: '',
                },
                flag: true,
                flagModal: true,
            },
            computed: {
                boxClass() {
                    return this.showBox ? 'top-box' : ''
                },
                modalCart() {
                    return this.flagModalCart
                },
                modalTblSize() {
                    return this.flagModalTblSize
                },
                modal() {
                    return this.flagModal
                },
            },
            methods: {
                fetchOrders(page = 1) {
                    let vm = this
                    axios.get(`/order/fetch?page=${page}`).then(res => {
                        vm.orders = res.data
                    })
                },
                fetchStatus(id) {
                    if (id === 0) return "در صف بررسی"
                    if (id === 1) return "آماده ارسال"
                    if (id === 2) return "ارسال شد"
                    if (id === 3) return "رزرو شد"
                    if (id === 4) return "لغو شد"
                },
                fetchUser() {
                    let vm = this
                    axios.get(`/panel/fetch/user`).then(res => {
                        vm.user = res.data
                        if (res.data.postal_code) {
                            vm.flagModal = false
                        }
                    })
                },
                exit() {
                    this.$refs.formExit.submit();
                },
            },
            mounted() {
                this.fetchOrders()
                this.fetchUser()
            }
        });
        $("#order-btn").addClass('active-menu');
    </script>

@endsection

@section('style')
    <style>
        .add-to-cart-btn {
            color: white;
            background: rgb(26, 201, 119);
            font-size: 16px;
            border-radius: 8px;
            cursor: pointer;
        }

        .size-help {
            color: #1ac977;
            border: 1px solid #1ac977;
            font-size: 15px;
            border-radius: 8px;
            cursor: pointer;
        }

        #modal-back {
            z-index: 99998;
            left: 0;
            width: 100%;
            height: 100vh;
            background-color: #00000096;
            position: fixed;
            top: 0;
        }

        .modal-mask {
            position: fixed !important;
            top: 0 !important;
            left: 0 !important;
            width: 100% !important;
            height: 100% !important;
            background-color: #00000073 !important;
            display: table !important;
            transition: opacity .3s ease !important;
        }

        .modal-content p {
            font-size: 14px;
            text-align: right
        }

        .modal-content input {
            border: 1px solid #ddd;
            padding: 5px;
            border-radius: 7px;
        }

        .modal-content {
            position: fixed;
            top: 13%;
            left: 50%;
            transform: translateX(-50%);
            border-radius: 20px;
            z-index: 99999;
        }

        .modal-content-size {
            position: fixed;
            border-radius: 20px;
            z-index: 99999;
            top: 10%;
            left: 30%;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            pointer-events: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .2);
            outline: 0;
        }

        #side-order {
            color: #1ac977 !important;
            font-weight: bold;
        }

        .btn-factor {
            background: #1ac977;
            padding: 4px 13px;
            width: 100%;
            border-radius: 8px;
            color: white;
            cursor: pointer;
            font-size: 13px;
        }

        .alert-info {
            background-color: transparent !important;
            border-color: transparent !important;;
        }

        .btn-close {
            position: absolute;
            top: 8%;
            left: 4%;
            font-size: 18px;
            color: #a0a0a0;
            cursor: pointer
        }

        @media only screen and (max-width: 768px) {
            table {
                font-size: 12px !important;
            }

            .btn-factor {
                font-size: 12px;
            }

            .btn-close {
                top: 0% !important;
            }

            .table thead th {
                text-align: center;
                font-weight: 400;
                color: #666;

            }

            .table td, .table th {
                text-align: center;
            }

            .modal-content {
                top: 33% !important;
            }

            .modal-content p {
                line-height: 30px;
            }
        }

        a:hover {
            color: white;
        }
    </style>
@endsection

