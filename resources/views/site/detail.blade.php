@extends('layouts.site.app')
@section('content')
<div id="content" class="rtl container-fluid px-0 text-right mt-3 mb-5 pb-5 mt-lg-0">

    <div class="row no-gutters d-none d-lg-block">
        <div class="col-12 py-3 px-5 text-right" style="background: #f5f5f5">

            <template v-for="cat in categories">
                <div class="d-inline-block mb-2">
                    <div class="d-flex flex-row-reverse">
                        <span class="last-loop">@{{ cat?.name }}</span>

                        <img v-if="cat?.parent_recursive[0]?.name" class="mx-2"
                             src="/assets/images/left-arrow3.svg" alt="" style="width: 10px;margin-top: 2px">

                        <span>@{{ cat?.parent_recursive[0]?.name }}</span>

                        <img v-if="cat?.parent_recursive[0]?.parent_recursive[0]?.name" class="mx-2"
                             src="/assets/images/left-arrow3.svg" alt="" style="width: 10px;margin-top: 2px">

                        <span>@{{ cat?.parent_recursive[0]?.parent_recursive[0]?.name }}</span>

                        <img class="mx-2"
                             src="/assets/images/left-arrow3.svg" alt="" style="width: 10px;margin-top: 2px">

                        <a href="{{url('/')}}">خانه</a>

                    </div>
                </div>
                <br>
            </template>


        </div>
    </div>

    <div class="row mt-lg-5 mx-0 px-0 px-lg-5">
        <div class="col-md-6 px-0 px-lg-5 relative">
            <div class="position-relative">
                    <span v-if="checkHaveDiscount(product.discount)" class="gallery-discount img-discount">
                                %@{{ Math.round(product.discount) }}
                </span>

                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                        <template v-for="gallery in product.galleries">
                            <div class="swiper-slide">
                                <img class="w-100" :src="'/images/gallery/'+gallery.image"
                                     style="border-radius: 10px">
                            </div>
                        </template>
                    </div>
                    <div class="swiper-button-next "></div>
                    <div class="swiper-button-prev "></div>
                </div>

                <div class="swiper-container gallery-thumbs mt-3">
                    <div class="swiper-wrapper">
                        <template v-for="gallery in product.galleries">
                            <div class="swiper-slide mx-1">
                                <img style="border-radius: 5px" class="w-100"
                                     :src="'/images/gallery/'+gallery.image">
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 mt-lg-3">
            <div class="row pb-3">
                <div class="w-100 d-lg-none">
                    <div class="col-12 py-2 px-4 text-right" style="background: #f5f5f5">
                        <template v-for="cat in categories">
                            <div class="d-inline-block mb-2">
                                <div class="d-flex flex-row-reverse">
                                    <span class="last-loop">@{{ cat?.name }}</span>

                                    <img v-if="cat?.parent_recursive[0]?.name" class="mx-2"
                                         src="/assets/images/left-arrow3.svg" alt=""
                                         style="width: 10px;margin-top: 2px">

                                    <span>@{{ cat?.parent_recursive[0]?.name }}</span>

                                    <img v-if="cat?.parent_recursive[0]?.parent_recursive[0]?.name" class="mx-2"
                                         src="/assets/images/left-arrow3.svg" alt=""
                                         style="width: 10px;margin-top: 2px">

                                    <span>@{{ cat?.parent_recursive[0]?.parent_recursive[0]?.name }}</span>

                                    <img class="mx-2"
                                         src="/assets/images/left-arrow3.svg" alt=""
                                         style="width: 10px;margin-top: 2px">

                                    <a href="{{url('/')}}">خانه</a>

                                </div>
                            </div>
                            <br>
                        </template>
                    </div>
                </div>
                <div class="col-12 mt-4 mt-lg-3 mt-lg-0">
                    <div v-if="product.brand">
                        <span style="font-size: 15px;margin-left: 5px;color: #a0a0a0">برند : </span>
                        <span id="name-e">@{{ product.brand.name_e }}</span>
                        <span id="name-f">( @{{ product.brand.name_f }} )</span>
                    </div>

                    <h1 class="mt-4" style="font-size: 20px;">@{{ product.name }}</h1>
                    <div class="product-price price mt-4">

                            <span class="new-price ml-4"
                                  style="font-size: 22px;color: #1ac977;font-weight: bold;">@{{ calculateDiscount(product.price, product.discount) }} تومان</span>

                        <span v-if="checkHaveDiscount(product.discount)"
                              class="old-price detail-old-price">@{{ numberFormat(product.price) }} تومان</span>

                    </div>
                    <div class="mt-5 short-desc" style="line-height: 30px;">
                    <span class="my-3"
                          style="font-size: 15px;margin-left: 5px;color: rgb(160, 160, 160);">ویژگی ها :</span>
                        <div class="mt-2" v-html="product.short_desc"></div>
                    </div>
                </div>

                <div class="col-12" v-if="sizes.length">
                    <div class="mt-5">
                            <span class="my-3"
                                  style="font-size: 15px;margin-left: 5px;color: rgb(160, 160, 160);">انتخاب سایز :</span>

                        <div class="size mt-3 pr-2">
                            <template v-for="size in sizes">

                                <label :id=`size-label-${size.id}`
                                       style="font-size: 16px;padding: 7px 15px;margin-bottom: 10px"
                                       class="form-check-label size-item position-relative ml-2 ">
                                    @{{ size.name }}
                                    <input type="radio" name="size"
                                           @change="selectSize(size.id)"
                                           style="position: absolute;left: 0;top: 0;width: 100%;height: 100%;opacity:0;cursor: pointer">

                                    <span class="size-span"></span>
                                </label>

                            </template>
                        </div>

                    </div>
                </div>

                <div class="col-12" v-if="colors.length">
                    <div class="color mt-5 mb-4">
                        <span class="my-3"
                              style="font-size: 15px;margin-left: 5px;color: rgb(160, 160, 160);">انتخاب رنگ :</span>
                        <div class="example ex1"
                             style="margin-top: 0px;margin-right: 0px;">
                            <template v-for="color in colors">
                                <label class="radio mt-3 text-center color-item"
                                       style="background-color: unset;padding:2px 10px;"
                                       :id=`color-label-${color.id}`>

                                    <input type="radio" name="color" :id="'color'+color.id"
                                           @change="selectColor(color.id)"/>
                                    <span :style="{backgroundColor: color.code}"
                                          style=" width: 32px;height: 27px;display: inline-block;vertical-align: bottom;"></span>
                                    <a class="d-block "
                                       style="display: inline-block;color: #a0a0a0!important;font-size:14px">
                                        @{{color.name }}
                                    </a>
                                </label>

                            </template>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="mb-5" v-if="numberExist && flagAddCart">
                            <span class="my-3" v-if="colors.length"
                                  style="font-size: 15px;margin-left: 5px;color: rgb(160, 160, 160);">تعداد :</span>
                        <div class="example ex1"
                             style="margin-top: 10px;margin-right: 0">
                            <select
                                style="cursor:pointer;border-radius: 5px;padding:4px 10px;border-color: #dddddd;color: grey;direction: ltr;width: 150px"
                                v-model="number">
                                <option value="1">1</option>
                                <option value="2" v-if="checkNum2()">2</option>
                                <option value="3" v-if="checkNum3()">3</option>
                                <option value="4" v-if="checkNum4()">4</option>
                                <option value="5" v-if="checkNum5()">5</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="col-12 mt-3">
                        <span @click="flagModalTblSize = true" v-if="product.tbl_size"
                              class="px-4 py-3 py-lg-2 size-help">راهنمای انتخاب سایز</span>

                    <a class="px-4 py-3 py-lg-2 add-to-cart-btn mr-lg-2" v-if="flagAddCart"
                       @click="addToCart">
                        افزودن به سبد خرید
                    </a>

                    <a class="px-4 py-3 py-lg-2 add-to-cart-btn mr-lg-2" v-if="!flagAddCart"
                       style="background: #dc3545">
                        موجود نمی باشد
                    </a>
                </div>

            </div>
        </div>

        <div class="col-12" v-if="product.specifications.length">
            <div class="container-fluid specification">
                <div class="row">
                    <div class="col-12 mt-5 pt-3 mb-4">
                            <span style="font-size: 20px;border-bottom: 2px solid #1ac977;padding: 5px 15px;">
                                مشخصات محصول
                            </span>
                    </div>
                </div>

                <div class="row p-3" v-for="(specification , index) in product.specifications"
                     :class="{ 'back-gray': checkBackGray(index) }">
                    <span class="col-5 col-lg-2">@{{specification.key}}</span>
                    <span class="col-7 col-lg-3">@{{specification.value}}</span>
                </div>
            </div>
        </div>
    </div>

    <div v-if="related.length" class="container-fluid mt-5 mt-lg-5 mt-md-0">
        <div class="row text-right mt-lg-4 pt-3 px-1 px-lg-5">
            <div class="col-12">
                <span class="slide-pro-title" style="font-size: 21px;">محصولات  مشابه</span>
            </div>
        </div>
    </div>

    <section v-if="related.length" class="slider-sug col-12 d-flex flex-row-reverse mt-2 mt-lg-3 px-1 px-lg-5">
        <div class="col-12 d-flex flex-row-reverse px-1">
            <div class="swiper-container related">
                <div class="swiper-wrapper">
                    <div v-for="item in related" class="swiper-slide">
                        <div class="sug-slider-item">
                            <div class="sug-slider-img relative">
                                <a :href=`/detail/${item.slug}`>
                                    <img class="w-100"
                                         :src="'/images/product/'+item.image"
                                         :alt="item.name">
                                </a>
                                <span v-if="checkHaveDiscount(item['discount'])" class="img-discount">
                                            %@{{ Math.ceil(item.discount) }}
                                        </span>
                            </div>
                            <div class="sug-slider-info text-right bg-white py-2 px-2 px-lg-3">

                                <div class="title">
                                    <a :href=`/detail/${item.slug}`>
                                        <p class="list-pro-name">
                                            @{{ productName(item.name) }}
                                        </p>
                                    </a>
                                </div>
                                <div class="price d-flex align-items-center justify-content-between"
                                     style="direction: ltr;">
                                    <div class="old-price rtl" style="height: 20px">
                                        <div class="pl-lg-4" v-if="checkHaveDiscount(item['discount'])">
                                            @{{ numberFormat(item.price) }}
                                            <span class="toman">تومان</span>
                                        </div>
                                    </div>

                                    <div class="new-price rtl  mb-md-0">
                                        @{{ calculateDiscount(item.price, item.discount) }}
                                        تومان
                                    </div>


                                </div>
                                <div class="colors col-12 px-0 mb-2">
                                    <template v-for="color in item.colors">
                                        <a :href=`/detail/${item.slug}/${color.id}` class="color-chosen"
                                           :style="{backgroundColor: color.code}">
                                        </a>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-button-next swiper-sug-next d-none d-md-block slide-next-1"></div>
            <div class="swiper-button-prev swiper-sug-prev d-none d-md-block slide-prev-1"></div>
        </div>
    </section>

    <div v-show="modalTblSize">
        <div id="modal-back" @click="flagModalTblSize = false"></div>
        <div class="modal-content-size px-2 py-5 p-lg-5 col-lg-6">
            <div class="rtl relative pt-5 pt-lg-0">
                        <span @click="flagModalTblSize = false"
                              style="position: absolute;top: 4%;left: 4%;font-size: 18px;color: #525252;cursor: pointer">✖</span>
                <p style="margin-bottom: 10px;">در نظر داشته باشید محدوده ی خطای اندازه گیری بین 1 تا 2 سانتیمتر
                    نرمال است.</p>
                <p style="margin-bottom: 20px;">اعداد بر حسب سانتی‌متر می باشد.</p>
            </div>
            <div v-html="product.tbl_size"></div>
        </div>
    </div>

    <div v-show="modalCart">
        <div id="modal-back" @click="flagModalCart = false"></div>
        <div class="modal-content col-12 col-lg-4 pb-5 rtl">
            <div class="row">
                <div class="col-12 text-center">
                    <img src="/assets/images/thank-success.svg" alt="" class="mt-4"
                         style="width: 100px;margin: auto">
                </div>
                <div class="col-12">
                    <p class="text-center mt-3" style="font-size: 17px;color: #777;">سبد خرید شما به روز شد !</p>
                </div>
                <div class="col-12 text-center mt-4">
                        <span class="px-4 py-3 py-lg-2 size-help" @click="flagModalCart = false">
                            ادامه دادن خرید
                        </span>

                    <a href="/cart"
                       class="px-4 py-3 py-lg-2 add-to-cart-btn mr-lg-3">
                        مشاهده سبد خرید
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    new Vue({
        el: '#content',
        data: {
            product: [],
            galleries: [],
            specifications: [],
            categories: [],
            related: [],
            border: false,
            flagAddCart: true,
            loader: false,
            sizes: [],
            colors: [],
            sizeId: null,
            colorId: null,
            sizeIdHolder: [],
            colorIdHolder: [],
            flagModalCart: false,
            flagModalTblSize: false,
            numberExist: null,
            number: 1,
            flagLastLoop: false,
        },
        computed: {
            boxClass() {
                return this.showBox ? 'top-box' : ''
            },
            modalCart() {
                return this.flagModalCart
            },
            modalTblSize() {
                return this.flagModalTblSize
            },
        },
        methods: {
            productName(name) {
                if (name.length > 20) {
                    return name.substring(0, 20) + '...'
                } else {
                    return name
                }
            },
            checkBackGray(index) {
                return index % 2 == 0
            },
            existCheck() {
                this.flagAddCart = false
                let m = this.sizeIdHolder.includes(this.sizeId)
                let n = this.colorIdHolder.includes(this.colorId)

                for (const [key, value] of Object.entries(this.product.exists)) {
                    if (value.size_id == this.sizeId && value.color_id == this.colorId) {
                        this.numberExist = value.num
                    }
                }

                if (m && n) {
                    this.flagAddCart = true
                }
            },
            existCheckWithoutColor() {

                this.flagAddCart = false
                let m = this.sizeIdHolder.includes(this.sizeId)
                for (const [key, value] of Object.entries(this.product.exists)) {
                    if (value.size_id == this.sizeId) {
                        this.numberExist = value.num
                    }
                }

                if (m) {
                    this.flagAddCart = true
                }
            },
            existCheckWithoutSize() {
                this.flagAddCart = false
                let m = this.colorIdHolder.includes(this.colorId)
                for (const [key, value] of Object.entries(this.product.exists)) {
                    if (value.color_id == this.colorId) {
                        this.numberExist = value.num
                    }
                }

                if (m) {
                    this.flagAddCart = true
                }
            },
            async selectSize(id) {
                let vm = this
                this.sizeId = id

                if (!this.colors.length) {
                    this.existCheckWithoutColor()
                } else {
                    await axios.get(`/exist/color/fetch/${id}`).then(res => {
                        $(`.color-item`).addClass(`pale`)
                        res.data.color_ids.forEach((colorId) => {
                            $(`#color-label-${colorId}`).removeClass(`pale`)
                        })

                        vm.colorIdHolder = res.data.color_ids
                        vm.colorNumHolder = res.data.color_num
                    })
                    if (this.sizeId !== null && this.colorId !== null) {
                        this.existCheck()
                    }
                }

            },
            async selectColor(id) {
                let vm = this
                this.colorId = id

                if (!this.sizes.length) {
                    this.existCheckWithoutSize()
                } else {
                    await axios.get(`/exist/size/fetch/${id}`).then(res => {
                        $(`.size-item`).addClass(`pale`)
                        res.data.size_ids.forEach((sizeId) => {
                            $(`#size-label-${sizeId}`).removeClass(`pale`)
                        })
                        vm.sizeIdHolder = res.data.size_ids
                        vm.sizeNumHolder = res.data.size_num
                    })
                    if (this.sizeId !== null && this.colorId !== null) {
                        this.existCheck()
                    }
                }
            },
            checkHaveDiscount(discount) {
                return (discount === 0) ? 0 : 1

            },
            async fetchProduct() {
                let vm = this
                await axios.get(`/fetch/product/${window.slug}`).then(res => {
                    vm.product = res.data
                    vm.sizes = res.data.sizes
                    vm.colors = res.data.colors
                })
                this.checkFirstLook()
                this.swiperGallery()
                this.tblSizeClass()
            },
            tblSizeClass() {
                let element = document.getElementsByTagName('table')
                element[0].classList.add('table')
                element[0].classList.add('table-striped')
                element[0].classList.add('table-bordered')
            },
            swiperGallery() {
                var galleryThumbs = new Swiper('.gallery-thumbs', {
                    spaceBetween: 10,
                    slidesPerView: 3,
                    freeMode: true,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                    breakpoints: {
                        400: {
                            slidesPerView: 4
                        },
                        768: {
                            slidesPerView: 4
                        },
                        999: {
                            slidesPerView: 5
                        }
                    },
                })
                new Swiper('.gallery-top', {
                    spaceBetween: 10,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    thumbs: {
                        swiper: galleryThumbs
                    }
                })
            },
            checkFirstLook() {
                if (!this.sizes.length && !this.colors.length) {
                    if (!this.product.exists.length) {
                        this.flagAddCart = false
                    }
                } else if (!this.colors.length) {
                    $(`.size-item`).addClass(`pale`)
                    this.product.exists.forEach((exist) => {
                        $(`#size-label-${exist.size_id}`).removeClass(`pale`)
                        this.sizeIdHolder.push(exist.size_id)
                    })
                } else if (!this.sizes.length) {
                    $(`.color-item`).addClass(`pale`)
                    this.product.exists.forEach((exist) => {
                        $(`#color-label-${exist.color_id}`).removeClass(`pale`)
                        this.colorIdHolder.push(exist.color_id)
                    })
                }
            },
            lastLoop(index) {
                if (index === this.categories.length - 1) {
                    return 'green'
                }
            },
            async fetchRelated() {
                let vm = this
                await axios.get(`/related/product/${window.slug}`).then(res => {
                    vm.related = res.data
                })
                setTimeout(() => {
                    new Swiper('.related', {
                        slidesPerView: 1,
                        spaceBetween: 10,
                        autoplay: false,
                        loop: true,
                        breakpoints: {
                            325: {
                                slidesPerView: 2
                            },
                            768: {
                                slidesPerView: 3
                            },
                            999: {
                                slidesPerView: 5
                            }
                        },
                        navigation: {
                            nextEl: '.swiper-sug-next',
                            prevEl: '.swiper-sug-prev',
                        },
                    })
                }, 2000)
            },
            calculateDiscount(price, discount) {
                let onePercent = price / 100
                let difference = 100 - discount
                let total = difference * onePercent
                let cal_discount = Math.round(total)
                return cal_discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            },
            numberFormat(price) {
                return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            fetchSpecifications() {
                let vm = this
                axios.get(`/fetch/product/spec/${window.slug}`).then(res => {
                    vm.specifications = res.data
                })
            },
            fetchCats() {
                let _this = this;
                axios.get(`/fetch/product/category/${window.slug}`).then(res => {

                    _this.categories = res.data
                })
            },
            redirectFilter(cat) {
                window.location.href = `/search/${cat}`;
            },
            addToCart() {
                let vm = this
                if (this.sizes.length != 0) {
                    if (this.sizeId === null) {
                        swal.fire(
                            {
                                text: `سایز را انتخاب کنید`,
                                icon: "info",
                                confirmButtonText: 'باشه',
                            }
                        )
                        return
                    }
                }
                if (this.colors.length != 0) {
                    if (this.colorId === null) {
                        swal.fire(
                            {
                                text: `رنگ را انتخاب کنید`,
                                icon: "info",
                                confirmButtonText: 'باشه',
                            }
                        )
                        return
                    }
                }

                axios.post('/cart/store', {
                    size_id: this.sizeId,
                    color_id: this.colorId,
                    product_id: this.product.id,
                    number: this.number,
                }).then((res) => {
                    vm.flagModalCart = true
                    // window.location.href = `/cart`;
                })


            },
            checkNum2() {
                if (this.numberExist >= 2) {
                    return true
                } else {
                    return false
                }
            },
            checkNum3() {
                if (this.numberExist >= 3) {
                    return true
                } else {
                    return false
                }
            },
            checkNum4() {
                if (this.numberExist >= 4) {
                    return true
                } else {
                    return false
                }
            },
            checkNum5() {
                if (this.numberExist >= 5) {
                    return true
                } else {
                    return false
                }
            },
        },
        mounted() {
            let parts = window.location.href.split('/')
            window.slug = parts.pop() || parts.pop()
            this.fetchProduct()
            this.fetchCats()
            this.fetchRelated()
        },
    })
</script>
@endsection

@section('style')
<style>
    th {
        background: rgb(82, 82, 82);
        color: white;
    }

    #modal-back {
        z-index: 99998;
        left: 0;
        width: 100%;
        height: 100vh;
        background-color: #00000096;
        position: fixed;
        top: 0;
    }

    .modal-mask {
        position: fixed !important;
        top: 0 !important;
        left: 0 !important;
        width: 100% !important;
        height: 100% !important;
        background-color: #00000073 !important;
        display: table !important;
        transition: opacity .3s ease !important;
    }

    .modal-content p {
        font-size: 14px;
        text-align: right
    }

    .modal-content input {
        border: 1px solid #ddd;
        padding: 5px;
        border-radius: 7px;
    }

    .modal-content {
        position: fixed;
        top: 13%;
        left: 50%;
        transform: translateX(-50%);
        border-radius: 20px;
        z-index: 99999;
    }


    .modal-content-size {
        position: fixed;
        border-radius: 20px;
        z-index: 99999;
        top: 10%;
        left: 24%;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        pointer-events: auto;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, .2);
        outline: 0;
    }

</style>
<style>
    .size-help {
        color: #1ac977;
        border: 1px solid #1ac977;
        font-size: 15px;
        border-radius: 8px;
        cursor: pointer;
    }

    .slider-sug {
        background-color: unset;
    }

    .colors {
        height: 20px;
        overflow: hidden;
    }

    .back-gray {
        background: #f9fafb
    }

    .specification {
        font-size: 15px;
    }

    .add-to-cart-btn {
        color: white;
        background: rgb(26, 201, 119);
        font-size: 16px;
        border-radius: 8px;
        cursor: pointer;
    }

    .not-exist-btn {
        color: white;
        background: #dc3545;
        font-size: 16px;
        border-radius: 8px;
    }

    .add-to-cart-btn:hover {
        color: white;
    }

    .not-exist-btn:hover {
        color: white;
    }

    body {
        background: white;
    }

    .size-span {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        cursor: pointer;
        border: 1px solid #0000004d;
        border-radius: 5px;

    }

    .size-item input:checked + span {
        border: 2px solid #1ac977;
        border-radius: 5px;
        background: #1ac97721;
    }

    .size-item {
        color: #505050;
    }

    .pale {
        opacity: .4;
    }

    #name-e {
        font-size: 16px;
        margin-left: 10px;
    }

    #name-f {
        color: #a0a0a0;
    }

    .short-desc ul li {
        list-style-type: disc;
    }

    .short-desc ul {
        padding-right: 22px;
    }


</style>
<style>
    input {
        background: white
    }

    .bread-crumb {
        color: #777 !important;
    }

    .green {
        color: #1ac977 !important;
        font-weight: bold;
    }

    .short_desc ul li {
        list-style: unset !important;
    }
</style>
<style>

    .example input {
        display: none;
    }

    .example label {
        /*margin-right: 20px;*/
        display: inline-block;
        cursor: pointer;
    }

    .example label a {
        color: #343434 !important;
        margin-top: 5px
    }

    .ex1 span {
        display: block;
        padding: 10px 15px 10px 25px;
        border: 1px solid #c5c5c5;
        border-radius: 7px;
        position: relative;
        transition: all 0.25s linear;
    }

    .ex1 span:before {
        content: '\2713 ';
        position: absolute;
        font-size: 20px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.3);
        color: white;
        opacity: 0;
        border-radius: 5px;
        transition: all 0.25s linear;
        font-weight: bold;
    }

    .ex1 .color-item input:checked + span:before {
        opacity: 1;
    }

    .ex1 .color-item input:checked ~ a {
        font-weight: bold;
        color: #333 !important;
    }

    .answer {
        position: relative;
    }

    .answer:after {
        position: absolute;
        top: 50%;
        right: -40px;
        height: 2px;
        width: 40px;
        content: "";
        background-color: #e0e0e0;
    }

    #Description p {
        line-height: 35px
    }

    textarea {
        border-color: #e0e0e0;
    }

    .product:hover {
        box-shadow: unset !important;
    }
</style>
<style>
    .product-gallery__featured {
        box-shadow: inset 0 0 0 2px #f2f2f2;
        padding: 2px;
        border-radius: 2px
    }

    .product-gallery__featured a {
        display: block;
        padding: 20px
    }

    .product-gallery__carousel {
        margin-top: 16px
    }

    .product-gallery__carousel-item {
        cursor: pointer;
        display: block;
        box-shadow: inset 0 0 0 2px #f2f2f2;
        padding: 12px;
        border-radius: 2px
    }

    .product-gallery__carousel-item--active {
        box-shadow: inset 0 0 0 2px #ffd333
    }

    .product-tabs {
        margin-top: 50px
    }

    .product-tabs__list {
        display: -ms-flexbox;
        display: flex;
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
        margin-bottom: -2px
    }

    .product-tabs__list:after,
    .product-tabs__list:before {
        content: "";
        display: block;
        width: 8px;
        -ms-flex-negative: 0;
        flex-shrink: 0
    }

    .product-tabs__item {
        font-size: 20px;
        padding: 18px 48px;
        border-bottom: 2px solid transparent;
        color: inherit;
        font-weight: 500;
        border-radius: 3px 3px 0 0;
        transition: all .15s
    }

    .product-tabs__item:hover {
        color: inherit;
        background: #f7f7f7;
        border-bottom-color: #d9d9d9
    }

    .product-tabs__item:first-child {
        margin-right: auto
    }

    .product-tabs__item:last-child {
        margin-left: auto
    }

    .product-tabs__item--active {
        transition-duration: 0s
    }

    .product-tabs__item--active,
    .product-tabs__item--active:hover {
        cursor: default;
        border-bottom-color: #ffd333;
        background: transparent
    }

    .product-tabs__content {
        border: 2px solid #f0f0f0;
        border-radius: 2px;
        padding: 80px 90px
    }

    .product-tabs__pane {
        overflow: hidden;
        height: 0;
        opacity: 0;
        transition: opacity .5s
    }

    .product-tabs__pane--active {
        overflow: visible;
        height: auto;
        opacity: 1
    }

    .product-tabs--layout--sidebar .product-tabs__item {
        padding: 14px 30px
    }

    .product-tabs--layout--sidebar .product-tabs__content {
        padding: 48px 50px
    }

    .radio-border {
        border: 1px solid #da3d64 !important;
        padding: 2px 10px;
        border-radius: 5px;
        color: #3f3f3f;
        font-weight: bold;
    }

</style>
<style>
    #gallery {
        display: flex;
        flex-direction: column;
        margin-top: 20px;
    }

    #gallery img {
        width: 100%;
    }

    /*product info table*/
    .tab-content {
        background-color: #f9fafb;
    }

    .detail-info-table-title {
        background-color: #f0f4f8;
        color: #061923;
        padding: 10px 5px;
        font-size: 14px;
        font-weight: bold;
    }

    .detail-info-table-txt {
        background-color: #f0f4f8;
        color: #4f4f4f;
        font-size: 14px;
        padding: 10px 5px;
    }

    .nav-tabs .nav-link {
        color: #6f7478;
        border-radius: 15px 15px 0 0 !important;

    }

    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        background-color: #f9fafb !important;
        color: #061923 !important;
        border-bottom-color: #f4f7f9 !important;
        border-radius: 15px 15px 0 0 !important;
    }

    .detail-info-title {
        color: #061923;
        font-size: 18px;
        font-weight: bold;
    }

    .detail-info-txt {
        color: #6f7478;
        font-size: 14px;
        line-height: 25px;
    }

    .swiper-slide-thumb-active {
        /*border: 2px dashed #1ac977;*/
        border-radius: 6px;
    }

    .swiper-slide {
        cursor: pointer;
    }

    .img-discount {
        position: absolute;
        top: 20px;
        left: 0;
        background: #f16422;
        color: white;
        padding: 9px 25px 7px 18px;
        font-size: 16px;
        border-radius: 0px 20px 20px 0;
        z-index: 9;
    }

    .slider-sug .img-discount {
        padding: 9px 15px 9px 15px;
        font-size: 13px;
    }

    .related .img-discount {
        left: 0px;
    }


    .swiper-slide-thumb-active {
        border: 2px dashed #f16422;
    }

    table th {
        font-weight: lighter !important;
    }

    @media only screen and (max-width: 768px) {
        .modal-content {
            top: unset;
            bottom: 0;
            left: 0%;
            transform: translate(0%, 0%);
            width: 100% !important;
            border-radius: 25px 25px 0 0 !important;
        }

        .img-discount {
            padding: 6px 12px 4px 6px !important;
            font-size: 13px !important;
        }

        .modal-content-size {
            top: 0% !important;
            left: 0% !important;
            height: 100vh !important;
            border-radius: 0 !important;
        }

        .modal-content-size table {
            font-size: 12px !important;
        }

        .gallery-discount {
            padding: 9px 17px 6px 12px !important;
            font-size: 16px !important;
        }
    }

    .last-loop {
        color: #1ac977;
        font-weight: bold;
    }


</style>
@endsection

