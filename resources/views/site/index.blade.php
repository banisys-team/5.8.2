@extends('layouts.site.app')
@section('content')
  <div class="container rtl mb-5 pb-5 mb-lg-0 pb-lg-5" id="content">
    <div class="row">
      <section class="col-12 px-0">
        <div class="container-fluid p-0">
          <div class="swiper-container carouselTop slider-desktop mt-4">
            <div class="swiper-wrapper">
              <div v-for="item in sliderDesktop" class="swiper-slide">
                <a :href="item.url">
                  <img class="w-100" :src="'/images/slider/' + item.image"
                  style="border-radius: 16px">
                </a>
              </div>
            </div>
            <div class="swiper-button-next swiper-arrow"></div>
            <div class="swiper-button-prev swiper-arrow"></div>
          </div>

          <div class="swiper-container carouselTop slider-mobile">
            <div class="swiper-wrapper">
              <div v-for="item in sliderMobile" class="swiper-slide">
                <a :href="item.url">
                  <img class="w-100" :src="'/images/slider/' + item.image">
                </a>
              </div>
            </div>
            <div class="swiper-button-next swiper-arrow"></div>
            <div class="swiper-button-prev swiper-arrow"></div>
          </div>

        </div>
      </section>

      <div class="container-fluid mt-3 mt-md-0" v-if="suggests.length">
        <div class="d-flex justify-content-between align-items-center text-md-right mt-lg-4 pt-3 px-lg-5">
          <div class="col-6 col-md-6 px-0  text-right">
            <span class="index-suggest-title">شگفت انگیز های روز</span>
          </div>
          <div class="">
            <div class="col-md-12 text-left text-md-center px-0  mt-md-0" style="padding-top: 4px">
              <p class="index-counter" id="demo">
                <span id="demo1" style="width: 40px;display: inline-block;text-align: center"></span> :
                <span id="demo2" style="width: 40px;display: inline-block;text-align: center"></span> :
                <span id="demo3" style="width: 40px;display: inline-block;text-align: center"></span>
              </p>
            </div>
            <div class="d-none d-lg-block col-md-12 text-center text-md-left" style="padding-top: 0px">
              <span class="index-remaining-time">زمان باقی مانده تا پایان سفارش</span>
            </div>
          </div>
        </div>
      </div>

      <section class="slider-sug col-12 d-flex flex-row-reverse mt-3 px-1 px-lg-5 suggest-index" v-if="suggests.length">
        <div class="col-12 d-flex flex-row-reverse px-1">
          <div class="swiper-container suggest">
            <div class="swiper-wrapper">
              <div v-for="item in suggests" class="swiper-slide">
                <div class="sug-slider-item">
                  <div class="sug-slider-img relative">
                    <a :href=`/detail/${item.product.slug}`>
                      <img class="w-100" :src="'/images/product/' + item.product.image" :alt="item.product.name">
                    </a>
                    <span v-if="checkHaveDiscount(item.product['discount'])" class="img-discount">
                      %@{{ Math.ceil(item.product.discount) }}
                    </span>
                  </div>
                  <div class="sug-slider-info text-right bg-white py-2 px-2 px-lg-3">

                    <div class="title">
                      <a :href=`/detail/${item.product.slug}`>
                        <p class="list-pro-name">
                          @{{ productName(item.product.name) }}
                        </p>
                      </a>
                    </div>
                    <div class="price d-flex align-items-center justify-content-between" style="direction: ltr;">
                      <div class="old-price rtl" style="height: 20px">
                        <div class="pl-lg-4" v-if="checkHaveDiscount(item.product['discount'])">
                          @{{ numberFormat(item.product.price) }}
                          <span class="toman">تومان</span>
                        </div>
                      </div>

                      <div class="new-price rtl  mb-md-0">
                        @{{ calculateDiscount(item.product.price, item.product.discount) }}
                        تومان
                      </div>


                    </div>
                    <div class="colors col-12 px-0 mb-2">
                      <template v-for="color in item.product.colors">
                        <a :href=`/detail/${item.slug}/${color.id}` class="color-chosen"
                          :style="{ backgroundColor: color.code }">
                        </a>
                      </template>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-button-next swiper-sug-next d-none d-md-block"></div>
          <div class="swiper-button-prev swiper-sug-prev d-none d-md-block"></div>
        </div>
      </section>
      <div class="col-12 mt-3 mt-lg-4 text-center" v-if="suggests.length">
        <a href="/suggest" class="see-all-btn">
          مشاهده همه
        </a>
      </div>


      <div class="col-12 text-center mt-5 py-4" style="background: white">
        <h1 style="font-size: 23px;color: #454545;">با خیال راحت</h1>
        <p class="mt-2" style="font-size: 17px;color: #939393;">از فروشگاه {{ $site_name }} خرید کنید </p>
        <div class="col-12 d-flex justify-content-center clearfix mt-5">
          <div class="col-12 col-lg-8 d-flex flex-row flex-wrap justify-content-center">
            <div class="col-12 col-md-4 float-none">
              <img src="/assets/images/terme-serv.svg" class="col-5 col-lg-5 mx-auto d-block float-none">
              <span class="feature-title d-block mt-3" style="color: #1ac977;font-weight: bold;">
                پاسخگویی پشتیبانی
              </span>
              <span class="feature-desc d-block mt-2" style="color: #929292">9:00 الی 22:00</span>
            </div>
            <div class="col-12 col-md-4 float-none mt-5 mt-lg-0">
              <img src="/assets/images/terme-check.svg" class="col-5 col-lg-5 mx-auto d-block float-none">
              <span class="feature-title d-block mt-3" style="color: #1ac977;font-weight: bold;">تضمین کیفیت </span>
              <span class="feature-desc d-block mt-2" style="color: #929292">تضمین بالاترین کیفیت و اصل بودن کالا</span>
            </div>
            <div class="col-12 col-md-4  float-none mt-5 mt-lg-0">
              <img src="/assets/images/terme-sale.svg" class="col-5 col-lg-5 mx-auto d-block float-none">
              <span class="feature-title d-block mt-3" style="color: #1ac977;font-weight: bold;">قیمت استثنایی</span>
              <span class="feature-desc d-block mt-2" style="color: #929292">تضمین مناسب ترین قیمت موجود در بازار</span>
            </div>
          </div>
        </div>
      </div>


      <div v-if="proSlider1.length" class="container-fluid mt-5 mt-lg-3 mt-md-0">
        <div class="row text-right mt-lg-4 pt-3 px-1 px-lg-5">
          <div class="col-12">
            <span class="slide-pro-title" style="font-size: 19px;">@{{ proSlider1Cat }}</span>
            <a :href=`/auto/search/cat/${proSlider1CatId}` class="float-left slide-pro-more">مشاهده همه
              <img src="/assets/images/left-arrow3.svg" alt="" style="width: 8px">
            </a>
          </div>
        </div>
      </div>
      <section v-if="proSlider1.length" class="slider-sug col-12 d-flex flex-row-reverse mt-2 mt-lg-3 px-1 px-lg-5">
        <div class="col-12 d-flex flex-row-reverse px-1">
          <div class="swiper-container pro-slider1">
            <div class="swiper-wrapper">
              <div v-for="item in proSlider1" class="swiper-slide">
                <div class="sug-slider-item">
                  <div class="sug-slider-img relative">
                    <a :href=`/detail/${item.slug}`>
                      <img class="w-100" :src="'/images/product/' + item.image" :alt="item.name">
                    </a>
                    <span v-if="checkHaveDiscount(item['discount'])" class="img-discount">
                      %@{{ Math.ceil(item.discount) }}
                    </span>
                  </div>
                  <div class="sug-slider-info text-right bg-white py-2 px-2 px-lg-3">

                    <div class="title">
                      <a :href=`/detail/${item.slug}`>
                        <p class="list-pro-name">
                          @{{ productName(item.name) }}
                        </p>
                      </a>
                    </div>
                    <div class="price d-flex align-items-center justify-content-between" style="direction: ltr;">
                      <div class="old-price rtl" style="height: 20px">
                        <div class="pl-lg-4" v-if="checkHaveDiscount(item['discount'])">
                          @{{ numberFormat(item.price) }}
                          <span class="toman">تومان</span>
                        </div>
                      </div>

                      <div class="new-price rtl  mb-md-0">
                        @{{ calculateDiscount(item.price, item.discount) }}
                        تومان
                      </div>


                    </div>
                    <div class="colors col-12 px-0 mb-2">
                      <template v-for="color in item.colors">
                        <a :href=`/detail/${item.slug}/${color.id}` class="color-chosen"
                          :style="{ backgroundColor: color.code }">
                        </a>
                      </template>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-button-next swiper-sug-next d-none d-md-block slide-next-1"></div>
          <div class="swiper-button-prev swiper-sug-prev d-none d-md-block slide-prev-1"></div>
        </div>
      </section>

      <div v-if="proSlider2.length" class="container-fluid mt-lg-4">
        <div class="row text-right mt-lg-4 pt-3 px-1 px-lg-5">
          <div class="col-12">
            <span class="slide-pro-title" style="font-size: 19px;">@{{ proSlider2Cat }}</span>
            <a :href=`/auto/search/cat/${proSlider2CatId}` class="float-left slide-pro-more">مشاهده همه
              <img src="/assets/images/left-arrow3.svg" alt="" style="width: 8px;">
            </a>
          </div>
        </div>
      </div>
      <section v-if="proSlider2.length" class="slider-sug col-12 d-flex flex-row-reverse mt-2 mt-lg-3 px-1 px-lg-5">
        <div class="col-12 d-flex flex-row-reverse px-1">
          <div class="swiper-container pro-slider2">
            <div class="swiper-wrapper">
              <div v-for="item in proSlider2" class="swiper-slide">
                <div class="sug-slider-item">
                  <div class="sug-slider-img relative">
                    <a :href=`/detail/${item.slug}`>
                      <img class="w-100" :src="'/images/product/' + item.image" :alt="item.name">
                    </a>
                    <span v-if="checkHaveDiscount(item['discount'])" class="img-discount">
                      %@{{ Math.ceil(item.discount) }}
                    </span>
                  </div>
                  <div class="sug-slider-info text-right bg-white py-2 px-2 px-lg-3">

                    <div class="title">
                      <a :href=`/detail/${item.slug}`>
                        <p class="list-pro-name">
                          @{{ productName(item.name) }}
                        </p>
                      </a>
                    </div>
                    <div class="price d-flex align-items-center justify-content-between" style="direction: ltr;">
                      <div class="old-price rtl" style="height: 20px">
                        <div class="pl-lg-4" v-if="checkHaveDiscount(item['discount'])">
                          @{{ numberFormat(item.price) }}
                          <span class="toman">تومان</span>
                        </div>
                      </div>

                      <div class="new-price rtl  mb-md-0">
                        @{{ calculateDiscount(item.price, item.discount) }}
                        تومان
                      </div>


                    </div>
                    <div class="colors col-12 px-0 mb-2">
                      <template v-for="color in item.colors">
                        <a :href=`/detail/${item.slug}/${color.id}` class="color-chosen"
                          :style="{ backgroundColor: color.code }">
                        </a>
                      </template>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-button-next swiper-sug-next d-none d-md-block slide-next-2"></div>
          <div class="swiper-button-prev swiper-sug-prev d-none d-md-block slide-prev-2"></div>
        </div>
      </section>

      <div v-if="proSlider3.length" class="container-fluid mt-3 mt-lg-4 pt-lg-4">
        <div class="row text-right pt-3 px-1 px-lg-5">
          <div class="col-12">
            <span class="slide-pro-title" style="font-size: 19px;">@{{ proSlider3Cat }}</span>
            <a :href=`/auto/search/cat/${proSlider3CatId}` class="float-left ml-lg-2 slide-pro-more">مشاهده
              همه
              <img src="/assets/images/left-arrow3.svg" alt="" style="width: 8px;">
            </a>
          </div>
        </div>
      </div>
      <section v-if="proSlider3.length" class="slider-sug col-12 d-flex flex-row-reverse mt-2 mt-lg-3 px-1 px-lg-5">
        <div class="col-12 d-flex flex-row-reverse px-1">
          <div class="swiper-container pro-slider3">
            <div class="swiper-wrapper">
              <div v-for="item in proSlider3" class="swiper-slide">
                <div class="sug-slider-item">
                  <div class="sug-slider-img relative">
                    <a :href=`/detail/${item.slug}`>
                      <img class="w-100" :src="'/images/product/' + item.image" :alt="item.name">
                    </a>
                    <span v-if="checkHaveDiscount(item['discount'])" class="img-discount">
                      %@{{ Math.ceil(item.discount) }}
                    </span>
                  </div>
                  <div class="sug-slider-info text-right bg-white py-2 px-2 px-lg-3">

                    <div class="title">
                      <a :href=`/detail/${item.slug}`>
                        <p class="list-pro-name">
                          @{{ productName(item.name) }}
                        </p>
                      </a>
                    </div>
                    <div class="price d-flex align-items-center justify-content-between" style="direction: ltr;">
                      <div class="old-price rtl" style="height: 20px">
                        <div class="pl-lg-4" v-if="checkHaveDiscount(item['discount'])">
                          @{{ numberFormat(item.price) }}
                          <span class="toman">تومان</span>
                        </div>
                      </div>

                      <div class="new-price rtl  mb-md-0">
                        @{{ calculateDiscount(item.price, item.discount) }}
                        تومان
                      </div>


                    </div>
                    <div class="colors col-12 px-0 mb-2">
                      <template v-for="color in item.colors">
                        <a :href=`/detail/${item.slug}/${color.id}` class="color-chosen"
                          :style="{ backgroundColor: color.code }">
                        </a>
                      </template>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="swiper-button-next swiper-sug-next d-none d-md-block slide-next-3"></div>
          <div class="swiper-button-prev swiper-sug-prev d-none d-md-block slide-prev-3"></div>
        </div>
      </section>
    </div>
  </div>
@endsection

@section('script')
  <script>
    new Vue({
      el: '#content',
      data: {
        suggests: [],
        cats: [],
        brands: [],
        brandsChosen: [],
        filters: [],
        offers: [],
        loader: false,
        search: '',
        flagMore: true,
        sliderDesktop: [],
        sliderMobile: [],
        newProducts: [],
        i1: '',
        i2: '',
        i3: '',
        i4: '',
        i5: '',
        i6: '',
        page: 1,
        time: '',
        proSlider1: [],
        proSlider2: [],
        proSlider3: [],
        proSlider1Cat: '',
        proSlider2Cat: '',
        proSlider3Cat: '',
        proSlider1CatId: '',
        proSlider2CatId: '',
        proSlider3CatId: '',
      },
      computed: {
        filteredList() {
          return this.brands.filter(brand => {
            return brand.name.includes(this.search) || brand.name_f.includes(this.search);
          });
        }
      },
      methods: {
        productName(name) {
          if (name.length > 20) {
            return name.substring(0, 20) + '...'
          } else {
            return name
          }
        },
        async proSlider1Fetch() {
          let vm = this
          await axios.get(`/slider/product/fetch/1`).then(res => {
            vm.proSlider1 = res.data.products
            vm.proSlider1CatId = res.data.cat_id
            vm.proSlider1Cat = res.data.cat_name
          })
          new Swiper('.pro-slider1', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: false,
            loop: true,
            breakpoints: {
              320: {
                slidesPerView: 2,
              },
              768: {
                slidesPerView: 3
              },
              999: {
                slidesPerView: 5
              }
            },
            navigation: {
              nextEl: '.slide-next-1',
              prevEl: '.slide-prev-1',
            },
          })
        },
        async proSlider2Fetch() {
          let vm = this
          await axios.get(`/slider/product/fetch/2`).then(res => {
            vm.proSlider2 = res.data.products
            vm.proSlider2CatId = res.data.cat_id
            vm.proSlider2Cat = res.data.cat_name
          })
          new Swiper('.pro-slider2', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: false,
            loop: true,
            breakpoints: {
              320: {
                slidesPerView: 2,
              },
              768: {
                slidesPerView: 3
              },
              999: {
                slidesPerView: 5
              }
            },
            navigation: {
              nextEl: '.slide-next-2',
              prevEl: '.slide-prev-2',
            },
          })
        },
        async proSlider3Fetch() {
          let vm = this
          await axios.get(`/slider/product/fetch/3`).then(res => {
            vm.proSlider3 = res.data.products
            vm.proSlider3CatId = res.data.cat_id
            vm.proSlider3Cat = res.data.cat_name
          })
          new Swiper('.pro-slider3', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: false,
            loop: true,
            breakpoints: {
              320: {
                slidesPerView: 2,
              },
              768: {
                slidesPerView: 3
              },
              999: {
                slidesPerView: 5
              }
            },
            navigation: {
              nextEl: '.slide-next-3',
              prevEl: '.slide-prev-3',
            },
          })
        },
        async fetchSuggest() {
          let vm = this
          await axios.get(`/fetch/suggests`).then(res => {
            vm.suggests = res.data
          })

          new Swiper('.suggest', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: false,
            loop: true,
            breakpoints: {
              320: {
                slidesPerView: 2,
              },
              768: {
                slidesPerView: 3
              },
              999: {
                slidesPerView: 5
              }
            },
            navigation: {
              nextEl: '.swiper-sug-next',
              prevEl: '.swiper-sug-prev',
            },
          })
        },
        calculateDiscount(price, discount) {
          onePercent = price / 100;
          difference = 100 - discount;
          total = difference * onePercent;
          result = Math.round(total);
          return result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        numberFormat(price) {
          return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        fetchCats() {
          let _this = this;
          axios.get('/fetch/search/cats').then(function(res) {
            _this.cats = res.data;
          });
        },
        redirectFilter(cat) {
          window.location.href = `/search/${cat}`;
        },
        async fetchSlider() {
          let vm = this
          await axios.get(`/fetch/slider`).then(res => {
            vm.sliderDesktop = res.data.slider_desktop
            vm.sliderMobile = res.data.slider_mobile
          })
          new Swiper('.carouselTop', {
            loop: true,
            slidesPerView: 1,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
            autoplay: {
              delay: 8000,
            },
          })
        },
        checkHaveDiscount(discount) {
          if (discount === 0) {
            return false
          } else {
            return true
          }
        },
        timer() {
          const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
          ]

          const d = new Date()
          let year = new Date().getFullYear()

          var countDownDate =
            new Date(monthNames[d.getMonth()] + " " + d.getDate() + ", " + year + " " + this.time + ":00:00 GMT+4:30")
            .getTime()

          var x = setInterval(() => {

            var now = new Date().getTime()

            var distance = countDownDate - now

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // document.getElementById("demo").innerHTML = hours + "&nbsp;&nbsp; : &nbsp;&nbsp;"
            //     + minutes + "&nbsp;&nbsp; : &nbsp;&nbsp;" + seconds

            document.getElementById("demo1").innerHTML = hours
            document.getElementById("demo2").innerHTML = minutes
            document.getElementById("demo3").innerHTML = seconds

            if (distance < 0) {
              clearInterval(x)
              document.getElementById("demo").innerHTML = ''
            }
          }, 1000);
        },
        async fetchTime() {
          let vm = this
          await axios.get(`/product/suggest/timer/fetch`).then(res => {
            vm.time = res.data
          })
          this.timer()
        },
      },
      mounted() {
        this.fetchSuggest()
        this.fetchSlider()
        this.fetchTime()
        this.proSlider1Fetch()
        this.proSlider2Fetch()
        this.proSlider3Fetch()
      }
    })
  </script>
@endsection

@section('style')
  <style>
    .colors {
      height: 20px;
      overflow: hidden;
    }
  </style>
@endsection
