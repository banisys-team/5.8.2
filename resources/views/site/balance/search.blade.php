@extends('layouts.site.app')
@section('content')
    <div id="content" class="my-4 my-md-4" style="direction: rtl">
        <div class="container search-content">
            <div class="d-flex">
                <div class="col-lg-3 d-none d-lg-block text-right">
                    <ul id="categories">
                        <template v-for="(cat,key,index) in cats">
                            <li style="font-size:14px;">
                                <a :href=`/auto/search/cat/${cat.id}`
                                   style="color: #1ac977;display:inline-block">@{{ cat.name }}
                                </a>

                                <item :cat="cat"></item>
                            </li>
                            <hr v-if="cats.length !== key+1"
                                style="margin-top: .7rem;margin-bottom: .7rem">
                        </template>
                    </ul>
                    <span style="color:#3490dc;text-decoration: underline;cursor: pointer" class="mr-3"
                          @click="more" v-if="flagMore">+ موارد بیشتر</span>
                    <span style="color:#3490dc;text-decoration: underline;cursor: pointer" class="mr-3"
                          @click="less" v-if="!flagMore">- موارد کمتر</span>
                </div>

                <div class="col-lg-9 px-0">
                    <div class="row mb-1 px-4">
                        <div class="col-md-9">
                            <div class="breadcrumb_content text-right">
                                <a href="{{ url('/') }}">خانه</a>
                                <template v-for="category in breadcrumb">
                                    <i class="mx-1" v-if="category.parent_recursive.length">
                                        <img src="/assets/images/left-arrow3.svg" style="width: 8px">
                                    </i>
                                    <a v-if="category.parent_recursive.length"
                                        :href=`/auto/search/cat/${category.parent_recursive[0].id}` style="cursor: pointer"
                                    >@{{ category.parent_recursive[0].name }}</a>

                                    <i class="mx-1">
                                        <img src="/assets/images/left-arrow3.svg" style="width: 8px">
                                    </i>
                                    <a :href=`/auto/search/cat/${category.id}` style="cursor: pointer"
                                    >@{{ category.name }}</a>
                                </template>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <span class="float-right float-lg-left pt-lg-0" style="margin-top: 13px;color: #a5a5a5">
                                @{{ products.total }} کالا
                            </span>
                        </div>
                    </div>
                    <div class="row mt-2 px-2 px-lg-0 pl-lg-4 py-3">
                        <div class="col-6 col-lg-3 px-2 mb-4" v-for="item in products.data">
                            <div class="sug-slider-item">
                                <div class="sug-slider-img relative">
                                    <a :href=`/detail/${item.slug}`>
                                        <img class="w-100"
                                             :src="'/images/product/'+item.image"
                                             :alt="item.name">
                                    </a>
                                    <span v-if="checkHaveDiscount(item['discount'])" class="img-discount">
                                            %@{{ Math.ceil(item.discount) }}
                                        </span>
                                </div>
                                <div class="sug-slider-info text-right bg-white py-2 px-2 px-lg-3">

                                    <div class="title">
                                        <a :href=`/detail/${item.slug}`>
                                            <p class="list-pro-name">
                                                @{{ productName(item.name) }}
                                            </p>
                                        </a>
                                    </div>
                                    <div class="price d-flex align-items-center justify-content-between"
                                         style="direction: ltr;">
                                        <div class="old-price rtl" style="height: 20px">
                                            <div class="pl-lg-4" v-if="checkHaveDiscount(item['discount'])">
                                                @{{ numberFormat(item.price) }}
                                                <span class="toman">تومان</span>
                                            </div>
                                        </div>

                                        <div class="new-price rtl  mb-md-0">
                                            @{{ calculateDiscount(item.price,item.discount) }}
                                            تومان
                                        </div>
                                    </div>
                                    <div class="colors col-12 px-0 mb-2">
                                        <template v-for="color in item.colors">
                                            <a :href=`/detail/${item.slug}/${color.id}` class="color-chosen"
                                               :style="{backgroundColor: color.code}">
                                            </a>
                                        </template>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12" style="text-align: center">
                            <div @click="goUp()" style="display: inline-block">
                                <pagination :limit="2" :data="products"
                                            @pagination-change-page="fetchProducts"></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        Vue.component('item', {
            name: 'item',
            props: ['cat'],
            template: '<ul style="margin-bottom:8px;margin-top:8px" class="pr-0">' +
                '<li v-for="item in cat.children_recursive" @click.stop="redirectFilter(item.name)" style="margin-right: 8px;text-align: right;font-size:14px;margin-bottom: 10px;">' +
                '<i style="margin-left: 3px;font-size: 19px;color: #afafaf;display: inline-block;font-style: normal;">›</i>' +
                '<a :href="`/auto/search/cat/${item.id}`">@{{ item.name }} </a>' +
                '<item :cat="item"></item></li></ul>'
        })

        new Vue({
            el: '#content',
            data: {
                products: [],
                cal_discount: '',
                cats: [],
                brands: [],
                brandsChosen: [],
                filters: [],
                name:{!! $name !!},
                loader: false,
                search: '',
                flagMore: true,
                breadcrumb: {},
            },
            computed: {
                filteredList() {
                    return this.brands.filter(brand => {
                        return brand.name.includes(this.search) || brand.name_f.includes(this.search);
                    });
                }
            },
            methods: {
                productName(name) {
                    if (name.length > 20) {
                        return name.substring(0, 20) + '...'
                    } else {
                        return name
                    }
                },
                detail(slug) {
                    window.location.href = `/detail/${slug}`;
                },
                fav(id) {
                    let data = this;
                    axios.get(`/add/fav/${id}`).then(res => {
                        swal.fire(
                            {
                                text: "این کالا به لیست علاقه مندی ها افزوده شد !",
                                type: "success",
                                confirmButtonText: 'باشه',
                            }
                        );
                    });
                },
                fetchProducts(page = 1) {
                    let data = this;
                    axios.get(`/fetch/products/parent/cat/${this.name}?page=` + page).then(res => {
                        data.products = res.data
                    })
                },
                calculateDiscount(price, discount) {
                    onePercent = price / 100;
                    difference = 100 - discount;
                    total = difference * onePercent;
                    result = Math.round(total);
                    return result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                numberFormat(price) {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                redirectFilter(cat) {
                    window.location.href = `/search/${cat}`;
                },
                fetchBrand() {
                    let data = this;
                    axios.get(`/fetch/all/brands/related/${this.name}`).then(res => {
                        data.brands = res.data;
                    });
                },
                changeBrand(name) {
                    str = name.replace(/\s+/g, '_');
                    if ($(`#${str}`).prop('checked')) {
                        this.brandsChosen.push(str);
                    } else {
                        index = this.filters.indexOf(name);
                        this.brandsChosen.splice(index, 1);
                    }
                    this.searchProducts();
                },
                searchProducts() {
                    let data = this;
                    axios.post('/search/brand/cat', {
                        key: this.filters,
                        brand: this.brandsChosen,
                        cat: this.name
                    }).then(res => {
                        data.products = res.data;
                    });
                },
                replaceUnder(id) {
                    str = id.replace(/\s+/g, '_');
                    return str;
                },
                goUp() {
                    $("html, body").animate({scrollTop: 0}, "slow");
                },
                ggg() {
                    this.loader = false;
                },
                more() {
                    this.flagMore = false;
                    $("#categories").addClass("more");
                },
                less() {
                    this.flagMore = true;
                    $("#categories").removeClass("more");
                },
                checkHaveDiscount(discount) {
                    if (discount == 0) {
                        return false;
                    } else {
                        return true;
                    }
                },
                fetchBreadcrumb() {
                    let vm = this
                    axios.get(`/fetch/product/category2/${window.slug}`).then(res => {
                        vm.breadcrumb = res.data
                    })
                },
                fetchCats() {
                    let vm = this
                    axios.get('/fetch/search/cats').then((res) => {
                        vm.cats = res.data
                    })
                },
            },
            mounted() {
                let parts = window.location.href.split('/')
                window.slug = parts.pop() || parts.pop()
                this.fetchProducts()
                this.fetchCats()
                this.fetchBreadcrumb()
            }
        })
    </script>
@endsection

@section('style')
    <style>
        .search-content .img-discount {
            left: 4px;
            padding: 7px 15px 5px 10px;
        }

        body {
            background-color: white
        }

        .colors {
            height: 20px;
            overflow: hidden;
        }

        #categories {
            padding: 20px 20px 0px 0px;
            max-height: 1200px;
            overflow: hidden;
            cursor: pointer;
            user-select: none;
        }

        .more {
            height: auto !important;
            overflow: visible !important;
        }

        .sug-slider-item {
            border-radius: 8px;
            overflow: hidden;
        }
    </style>
@endsection

