@extends('layouts.site.app')
@section('content')
    <div class="container-fluid rtl mb-5 pb-5 pb-lg-0 px-0 mt-4 mt-lg-0" id="content">


        <div class="d-flex justify-content-center pt-3 pb-3 pb-lg-0 pt-lg-4" style="background: #dc3545">
{{--            <img src="/assets/images/suggest.png" alt="" class="suggest-img">--}}
            <p class="suggest-title m-0">شگفت انگیزها</p>
        </div>

        <div class="d-flex flex-column flex-lg-row justify-content-center pb-3 pb-lg-0 pt-3 pt-lg-4 timer-container">
            <p id="demo" class="mb-2 mb-lg-4 suggest-timer text-center">
                <span id="demo1" style="width: 20px;display: inline-block;text-align: center"></span> :
                <span id="demo2" style="width: 20px;display: inline-block;text-align: center"></span> :
                <span id="demo3" style="width: 20px;display: inline-block;text-align: center"></span>
            </p>
            <span class="suggest-timer-span text-center"> تا پایان مهلت ثبت سفارش از کالاهای شگفت انگیز</span>
        </div>

        <div class="row p-2 p-lg-5 mt-4 mt-lg-0 no-gutters mx-lg-5">
            <div class="col-6 col-lg-3 px-1 mb-3 mb-lg-5 pb-4 px-lg-3" v-for="item in products.data">
                <div class="sug-slider-item" style="box-shadow: 1px 1px 6px 0 rgb(0 0 0 / 20%);border-radius: 10px;">
                    <div class="sug-slider-img relative">
                        <span v-if="checkHaveDiscount(item.product['discount'])" class="img-discount">
                            %@{{ Math.ceil(item.product.discount) }}
                        </span>
                        <a :href=`/detail/${item.product.slug}`>
                            <img class="w-100" style="border-radius: 8px 8px 0 0;"
                                 :src="'/images/product/'+item.product.image"
                                 :alt="item.product.name">
                        </a>
                    </div>

                    <div class="sug-slider-info text-right bg-white pt-2 pb-3 pb-lg-4  px-2 px-lg-3">
                        <div class="title">
                            <a :href=`/detail/${item.slug}`>
                                <p class="list-pro-name">
                                    @{{ productName(item.product.name) }}
                                </p>
                            </a>
                        </div>
                        <div class="price d-flex align-items-center justify-content-between"
                             style="direction: ltr;">
                            <div class="old-price rtl" style="height: 20px;margin-top: 8px;">
                                <div class="pl-lg-4" v-if="checkHaveDiscount(item.product['discount'])">
                                    @{{ numberFormat(item.product.price) }}
                                    <span class="toman">تومان</span>
                                </div>
                            </div>

                            <div class="new-price rtl  mb-md-0">
                                @{{ calculateDiscount(item.product.price,item.product.discount) }}
                                تومان
                            </div>
                        </div>
                        <div class="colors col-12 px-0 mb-2">
                            <template v-for="color in item.product.colors">
                                <a :href=`/detail/${item.product.slug}/${color.id}` class="color-chosen"
                                   :style="{backgroundColor: color.code}">
                                </a>
                            </template>
                        </div>
                        <a :href=`/detail/${item.product.slug}` class="add-cart-suggest">
                            خرید محصول
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                products: []
            },
            methods: {
                productName(name) {
                    if (name.length > 20) {
                        return name.substring(0, 20) + '...'
                    } else {
                        return name
                    }
                },
                timer() {
                    const monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                    ]

                    const d = new Date()
                    let year = new Date().getFullYear()

                    var countDownDate =
                        new Date(monthNames[d.getMonth()] + " " + d.getDate() + ", " + year + " " + this.time + ":00:00 GMT+4:30").getTime()

                    var x = setInterval(() => {

                        var now = new Date().getTime()

                        var distance = countDownDate - now

                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        // document.getElementById("demo").innerHTML = hours + "&nbsp;&nbsp; : &nbsp;&nbsp;"
                        //     + minutes + "&nbsp;&nbsp; : &nbsp;&nbsp;" + seconds

                        document.getElementById("demo1").innerHTML = hours
                        document.getElementById("demo2").innerHTML = minutes
                        document.getElementById("demo3").innerHTML = seconds

                        if (distance < 0) {
                            clearInterval(x)
                            document.getElementById("demo").innerHTML = "!منقضی شد"
                        }
                    }, 1000);
                },
                async fetchTime() {
                    let vm = this
                    await axios.get(`/product/suggest/timer/fetch`).then(res => {
                        vm.time = res.data
                    })
                    this.timer()
                },
                fetchSuggest(page = 1) {
                    let vm = this
                    axios.get(`/suggest/fetch/all?page=${page}`).then(res => {
                        vm.products = res.data
                    })
                },
                checkHaveDiscount(discount) {
                    if (discount == 0) {
                        return false;
                    } else {
                        return true;
                    }
                },
                numberFormat(price) {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                },
                calculateDiscount(price, discount) {
                    let onePercent = price / 100
                    let difference = 100 - discount
                    let total = difference * onePercent
                    let result = Math.round(total)
                    return result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                },
            },
            mounted() {
                this.fetchTime()
                this.fetchSuggest()
            }
        })
    </script>
@endsection

@section('style')
    <style>
        body {
            background: white;
        }

        .colors {
            height: 20px;
            overflow: hidden;
        }

        .add-cart-suggest {
            background: #1ac977;
            color: white;
            padding: 5px 45px;
            font-size: 16px;
            border-radius: 30px;
            position: absolute;
            bottom: -15px;
            left: 22%;
        }

        .sug-slider-item {
            border-radius: 8px;
            overflow: hidden;
        }

        .img-discount {
            left: 16px;
            padding: 7px 15px 7px 15px;
        }

        .add-cart-suggest {
            bottom: 6px;
        }

        @media only screen and (max-width: 768px) {
            .img-discount {
                left: 4px !important;
            }
        }

        @media only screen and (max-width: 768px) {
            .colors {
                margin-top: 0px;
            }
        }


    </style>
@endsection

