<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>فروشگاه پوشاک</title>
    <script src="{{ asset('/js/dress/app.js')}}"></script>
    <link rel="stylesheet" href="/layout_kaj/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout_kaj/css/style.css">
    <link rel="stylesheet" href="/layout_kaj/css/swiper.css">
    <style>
        .btn-primary {
            font-size: 14px;
            border-bottom: 3px solid #005abb;
        }
    </style>
</head>
<body>

<style>
    body {
        height: 100vh !important;
    }

    #xxx > li {
        margin-left: 30px;
    }

    #xxx > li > a {
        font-size: 14px;
        color: #828282;
    }
</style>

<br><br>
<?php


$MerchantID = '7e0c3e5e-77d3-421d-ae62-f8e64310c080';
$Amount2 = session()->get('sum_final');



$Authority = $_GET['Authority'];
if ($_GET['Status'] == 'OK') {
$client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
$result = $client->PaymentVerification([
    'MerchantID' => $MerchantID,
    'Authority' => $Authority,
    'Amount' => $Amount2,
]);

// if ($result->Status == 100) {

if (100 == 100) {

// session()->put('refid', $result->RefID);

session()->put('refid', '67655636');

(new App\Http\Controllers\Site\ReserveController())->wanted();

?>

<div class="container rtl text-right">
    <div class="row" style="height:150px">
        <div class="col-2"></div>
        <div class="col-sm-8" id="content" style="margin-top: 30px;">
            <div class="alert alert-success" role="alert">
                پرداخت شما با موفقیت انجام شد.
                <br>
                شناسه پرداخت شما:
                <br>
                {{$result->RefID}}
            </div>
            <div class="buttons mt-3" style="float: left;">

                <a href="{{url('/panel/orders')}}" style="font-size: 13px"
                   class="btn btn-danger ml-4">مشاهده سفارشات</a>
            </div>
        </div>
    </div>
</div>

<?php
} else {
?>
<div class="container rtl text-right">
    <div class="row" style="height:150px">
        <div class="col-sm-12" id="content" style="margin-top: 30px;">
            <div class="alert alert-danger" role="alert">
                پرداخت انجام نشد.
            </div>
            <div class="buttons" style="float: left">
                <div class="pull-right">
                    <a href="{{url('/')}}" class="btn btn-primary">بازگشت به صفحه اصلی</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}
} else {
?>
<div class="container rtl text-right">
    <div class="row" style="height:150px">
        <div class="col-sm-12" id="content" style="margin-top: 30px;">

            <div class="alert alert-danger" role="alert">
                پرداخت توسط شما لغو گردید.
            </div>
            <div class="buttons" style="float: left">
                <div class="pull-right">
                    <a href="{{url('/')}}" class="btn btn-primary">بازگشت به صفحه اصلی</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}
?>

</body>

</html>
