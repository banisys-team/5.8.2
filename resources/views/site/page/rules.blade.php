@extends('layouts.site.app')
@section('content')
    <div class="container pb-5 pb-lg-0 my-4 my-lg-5 rtl text-right" id="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="mb-4" style="color: #1ac977;font-size: 22px">قوانین استفاده از سایت</h1>
                <p style="text-align: justify;font-size:15px;line-height: 30px;" v-html="rules"></p>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                rules: '',
            },
            methods: {
                fetch() {
                    let _this = this;
                    axios.get('/admin/page/fetch').then(res => {
                        _this.rules = res.data.rules
                    })
                },
            },
            mounted() {
                this.fetch()
            }
        });
    </script>

@endsection

@section('style')
    <style>
        body {
            background: white;
        }
    </style>
@endsection

