@extends('layouts.site.app')
@section('content')
    <div class="container my-5 pb-5 pb-lg-0" style="direction: rtl;text-align: right" id="content">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h1 class="mb-4 text-center" style="color: #1ac977;font-size: 22px">.:: فرم ثبت شکایات ::.</h1>
                <div class="form-group">
                    <label>نام</label>
                    <input type="text" class="form-control" v-model="name">
                </div>

                <div class="form-group my-4">
                    <label>شماره همراه</label>
                    <input type="text" class="form-control" v-model="mobile">
                </div>

                <div class="form-group">
                    <label>متن پیام</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="8"
                              v-model="ticket"></textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-success" @click="formSubmit"
                            style="float: left;width: 140px;">
                        ارسال
                    </button>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                name: '',
                mobile: '',
                ticket: '',
            },
            methods: {
                formSubmit() {
                    if (this.name === '' || this.mobile === '' || this.ticket === '') {
                        swal.fire({
                            text: "مقادیر خالی را وارد کنید.",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                        return
                    }

                    let vm = this
                    axios.post('/complaint/store', {
                        name: this.name,
                        mobile: this.mobile,
                        ticket: this.ticket,
                    }).then(() => {
                        swal.fire({
                            text: "پیام شما با موفقیت ثبت شد !",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                        vm.name = ''
                        vm.mobile = ''
                        vm.ticket = ''
                    })
                },
            },
            mounted() {

            },
        })
    </script>

@endsection

@section('style')
    <style>
        body {
            background: white
        }

        .swal2-container {
            direction: rtl
        }
    </style>
@endsection

