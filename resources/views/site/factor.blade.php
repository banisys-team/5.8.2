<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>پوشاک پارلا</title>
    <script src="{{ asset('/js/dress/app.js')}}"></script>
    <link rel="stylesheet" href="/layout_kaj/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout_kaj/css/style.css">
    <style>
        .mobile-item-title {
            font-size: 11px;
            color: #777;
        }

        .logo-img {
            width: 35%
        }

        .factor-title {
            text-align: center;
            font-size: 1.6rem
        }

        @media only screen and (max-width: 768px) {
            .logo-img {
                width: 100%
            }

            .factor-title {
                font-size: 1rem;
                text-align: right;
            }
        }
    </style>
</head>
<body>

<div class="text-right rtl" id="content">
    <div class="container my-lg-5 p-3" style="border: 1px solid #a8a8a8">
        <div class="row">
            <div class="col-12 px-0 col-lg-4 d-flex d-lg-block">
                <div class=" col-6">
                    <div class="col-12 px-0 mb-3">
                        <span>شماره فاکتور : @{{ order.id }}</span>
                    </div>
                </div>
                <div class=" col-6 px-0 pr-lg-3">
                    <div class="col-12 px-0">
                        <span>تاریخ : @{{ getHours(order.created_at) }} - @{{order.shamsi_c}}</span>
                    </div>
                </div>
            </div>
            <div class="col-8 col-lg-4 factor-title m-auto">
                فروشگاه پوشاک
                @{{ settings.name.value }}
            </div>
            <div class="col-4 col-lg-4" style="text-align: left">
                <img :src=`/images/${settings.logo.value}` class="logo-img">
            </div>
        </div>

        <div class="row mb-3 mt-3">
            <div class="col-12 py-2 title-bg"
                 style="text-align: center;border-bottom: 1px #999 solid;border-top: 1px #999 solid;">
                <span style="font-size: 1.2rem;color: #1a1a1a">مشخصات خریدار</span>
            </div>
        </div>

        <div class="row mb-lg-3">
            <div class="col-lg-3 mb-2">
                <span>نام خانوادگی : @{{ order.name }}</span>
            </div>
            <div class="col-lg-3 mb-2">
                <span>همراه : @{{ order.user.mobile }}</span>
            </div>
            <div class="col-lg-3 mb-2">
                <span>شماره پیگیری : @{{ order.refid }}</span>
            </div>
            <div class="col-lg-3 mb-2">
                <span>استان/شهر : @{{ order.city }} - @{{ order.state }}</span>
            </div>
        </div>

        <div class="row mb-lg-3" v-if="order.postal_code">
            <div class="col-12 mb-2">
                <span>کد پستی : @{{ order.postal_code }}</span>
            </div>
        </div>

        <div class="row mb-lg-3">
            <div class="col-12 mb-2">
                <span>نشانی : @{{ order.address }}</span>
            </div>
        </div>
        <div class="row mb-lg-3">
            <div class="col-12">
                <span>توضیحات : @{{ order.description }}</span>
            </div>
        </div>


        <div class="row mt-4 mt-lg-5">
            <div class="d-none d-lg-block col-12">
                <table class="table table-bordered">
                    <thead style="background: #eaeaea">
                    <tr>
                        <td scope="col">ردیف</td>
                        <td scope="col">کد محصول</td>
                        <td scope="col">نام</td>
                        <td scope="col">قیمت واحد</td>
                        <td scope="col">تخفیف</td>
                        <td scope="col">تعداد</td>
                        <td scope="col">کل</td>
                    </tr>
                    </thead>
                    <tbody>

                    <tr v-for="(order_value,index) in order.order_values">
                        <td>@{{ index+1 }}</td>
                        <td>@{{ order_value.product_code }}</td>
                        <td>
                            <p class="m-0">
                                @{{ order_value.product_name }}
                            </p>

                            <p class="mt-3 mb-0 mr-2" v-if="order_value.size"
                               style="font-weight: bold;font-size: 13px">
                                سایز : @{{ order_value.size }}
                            </p>
                            <p class="mt-2 mb-0 mr-2" v-if="order_value.color"
                               style="font-weight: bold;font-size: 13px">
                                رنگ : @{{ order_value.color }}
                            </p>
                        </td>
                        <td>@{{ numberFormat(order_value.price) }}</td>
                        <td>%@{{ order_value.discount}}</td>
                        <td>@{{ order_value.number }}</td>
                        <td>@{{ calTotal(order_value.price,order_value.discount,order_value.number) }}</td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <div class="d-lg-none col-12 align-items-center">
                <div class="d-flex">
                    <div class="col-3 px-0 py-2 text-center" style="background-color: #eee;border: 1px solid #ddd">
                        ردیف
                    </div>
                    <div class="col-9 px-0 py-2 text-center" style="background-color: #eee;border: 1px solid #ddd">
                        مشخصات
                    </div>
                </div>
                <div v-for="(order_value,index) in order.order_values">
                    <div class="d-flex align-items-stretch">
                        <div class="col-3 px-0  d-flex align-items-center justify-content-center"
                             style="border: 1px solid #ddd">
                            @{{ index+1 }}
                        </div>
                        <div class="col-9 pr-3 py-2 d-flex flex-wrap" style="border:1px solid #ddd">
                            <div class="col-12 px-0 mb-2">
                                <span class="mobile-item-title">کد محصول:</span>
                                <span>@{{ order_value.product_code }}</span>
                            </div>
                            <div class="col-12 px-0 mb-2">
                                <span class="mobile-item-title">نام محصول:</span>
                                <span>@{{ order_value.product_name }}</span>
                            </div>
                            <div class="d-flex col-12 px-0">
                                <p class="col-6 mt-2 mb-0  px-0" v-if="order_value.size"
                                   style="font-size: 13px">
                                    سایز : @{{ order_value.size }}
                                </p>
                                <p class="col-6 mt-2 mb-0 px-0 " v-if="order_value.color"
                                   style="font-size: 13px">
                                    رنگ : @{{ order_value.color }}
                                </p>
                            </div>
                            <div class="col-6 px-0 mb-2">
                                <span class="mobile-item-title">قیمت:</span>
                                <span>@{{ numberFormat(order_value.price) }}</span>
                            </div>
                            <div class="col-6 px-0">
                                <span class="mobile-item-title">تعداد:</span>
                                <span>@{{ order_value.number }}</span>
                            </div>
                            {{--                            <div>--}}
                            {{--                                <span>تخفیف</span>--}}
                            {{--                                <span>( %@{{ order_value.discount}} )</span>--}}
                            {{--                            </div>--}}
                            <div class="col-12 px-0">
                                <span class="mobile-item-title">قیمت کل:</span>
                                <span>@{{ calTotal(order_value.price,order_value.discount,order_value.number) }}</span>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-6">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td style="background: #eaeaea">تعداد کالاهای خریداری شده:</td>
                                <td> @{{ totalNumber }} عدد</td>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="row mt-lg-4 pr-2">
                    <div class="col-lg-6">
                        <span>کد رهگیری پستی :</span>
                        <span v-if="order.track">@{{ order.track.number }}</span>
                        <span v-if="!order.track" style="color: #767676;font-size: 12.5px;line-height: 26px;">
                            ......... <br>  ( از تاریخ ثبت سفارش ، حداکثر تا 24 ساعت سفارش شما تحویل پست می شود و کد رهگیری برای رهگیری بسته از اداره پست در این قسمت قرار می گیرد. )
                        </span>
                    </div>
                </div>

            </div>

            <div class="col-lg-4 mt-3 mt-lg-5 mt-lg-0">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td style="background: #eaeaea">قیمت کالاها :</td>
                        <td>@{{ numberFormat(order.final_total) }} تومان</td>
                    </tr>
                    <tr>
                        <td style="background: #eaeaea">هزینه ارسال :</td>
                        <td>@{{ numberFormat(order.delivery) }} تومان</td>
                    </tr>
                    <tr>
                        <td style="background: #eaeaea">ارزش افزوده :</td>
                        <td>@{{ numberFormat(order.vat) }} تومان</td>
                    </tr>
                    <tr>
                        <td style="background: #eaeaea">مبلغ قابل پرداخت :</td>
                        <td>@{{ numberFormat(order.final_total + order.delivery + order.vat) }} تومان</td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#content',
        data: {
            order: [],
            sum: 0,
            sum_price: 0,
            percent: [],
            num_per: 0,
            totalNumber: 0,
            totalWithoutDiscount: 0,
            settings: [],
        },
        methods: {
            getHours(created_at) {
                let res = created_at.split(" ")
                let x = res[1].split(":")

                return `${x[0]}:${x[1]}`
            },
            async fetchOrder(id) {
                let vm = this
                await axios.get(`/fetch/order/${id}`).then(res => {
                    vm.order = res.data
                })
                this.calTotalWithoutDiscount()
                this.calTotalNumber()
            },
            fetchSettings() {
                let vm = this
                axios.get(`/setting/fetch`).then(res => {
                    vm.settings = res.data
                })
            },
            calTotalNumber() {
                this.totalNumber = 0
                this.order.order_values.forEach(order_value => {
                    this.totalNumber += order_value.number
                })
            },
            numberFormat(price) {
                return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            },
            calTotal(price, discount, number) {
                let x = 100 - discount
                let y = price / 100
                let z = (x * y) * number

                return z.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            },
            calTotalWithoutDiscount() {
                this.order.order_values.forEach(order_value => {
                    this.totalWithoutDiscount += order_value.price * order_value.number
                })
            },
        },
        mounted() {
            let parts = window.location.href.split('/')
            let id = parts.pop() || parts.pop()
            this.fetchOrder(id)
            this.fetchSettings()
        }
    })
</script>

</body>
</html>
