@extends('layouts.admin.app')
@section('content')
    <div class="container-fluid mt-4" id="content">
        <div class="row">
            <div class="col-12 col-lg-5">
                <div class="form-group d-flex flex-row ">
                    <label for="name" class="col-4 mt-2 control-label"
                           style="display: inline-block;text-align: left">کد محصول :</label>
                    <input id="name" type="text" class="col-6 form-control" @keyup.enter="formSubmit"
                           style="display: inline-block;direction: ltr;border-bottom-left-radius: 0;border-top-left-radius: 0;"
                           maxlength="12"
                           v-model="code" autofocus>
                    <button type="button" class="btn btn-dark" @click="formSubmit"
                            style="border: unset;border-bottom-right-radius: 0;border-top-right-radius: 0;background-color: #343a40">
                        <img src="/assets/images/search-white.svg" style="width: 19px">
                    </button>
                </div>
            </div>
        </div>

        <div class="d-flex align-items-start flex-column m-5" v-if="product">
            <div>
                <span style="color: #656565;">نام محصول :</span>
                <span style="font-weight: bold">@{{ product }}</span>
            </div>
            <div class="mt-3">
                <span style="color: #656565;">سایز :</span>
                <span style="font-weight: bold">@{{ size }}</span>
            </div>
            <div class="mt-3">
                <span style="color: #656565;">رنگ :</span>
                <span style="font-weight: bold">@{{ color }}</span>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                code: '',
                product: '',
                size: '',
                color: '',
            },
            methods: {
                formSubmit() {
                    if (this.code.length !== 12) {
                        swal.fire({
                            text: 'کد محصول را بصورت صحیح وارد کنید.',
                            icon: 'warning',
                            confirmButtonText: 'باشه',
                        })
                        return
                    }
                    this.product = ''
                    this.size = ''
                    this.color = ''
                    let vm = this
                    axios.post('/admin/exist/search/product/code', {
                        'code': this.code
                    }).then((res) => {
                        vm.product = res.data.product
                        vm.size = res.data.size
                        vm.color = res.data.color
                    })
                },
            }
        });
    </script>
    <script>
        $("#side_product").addClass("menu-open")
        $("#side_product_code").addClass("active")
    </script>
@endsection

@section('style')

@endsection
