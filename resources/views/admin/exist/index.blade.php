@extends('layouts.admin.app')
@section('content')
    <div class="container-fluid mt-4" id="content">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">نام</th>
                        <th scope="col">دسته</th>
                        <th scope="col">برند</th>
                        <th scope="col">تاریخ</th>
                        <th scope="col">
                            <div style="position: absolute;top: 17px">
                                <span style="font-size: 13px;color: #808080;">مجموع:</span>
                                <span style="font-weight: bold;color: #808080;">@{{ products.total }}</span>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="position-relative">
                            <div class="position-relative">
                                <input type="text" class="form-control col-12 d-inline-block" v-model="search.name"
                                       @keyup="checkSearchName"
                                       style="padding: 4px 10px;"
                                       @keyup.enter="searchName" placeholder="جستجو...">
                                <a class="btn btn-success btn-sm btn-search" @click="searchName"
                                   style="border:unset;height: 100%">
                                    <img src="/assets/images/search-white.svg">
                                </a>
                            </div>
                        </td>
                        <td>
                            <button @click="clickCatBtn" id="btn-cat">
                                @{{ form.cat }}
                                <i id="angle-down">ˇ</i>
                            </button>
                            <div v-if="flag" id="sss">
                                <ul v-if="flag1">
                                    <li style="line-height: 35px;font-weight: bold"
                                        @click="fetchProducts()">
                                        نمایش همه دسته ها
                                    </li>
                                    <li style="line-height: 35px;"
                                        @click="fetchChild(root.id,root.name)"
                                        v-for="root in roots">
                                        @{{ root.name }}
                                        <i id="angle-left" class="right">›</i>
                                    </li>
                                </ul>
                                <ul v-if="flag2" style="padding: 0 6px 0 0;">
                                    <li style="color: #dc3545;font-weight: bold"
                                        @click="back(holder.parentName)">
                                        <i class="left">‹</i>
                                        @{{ holder.parentName }}
                                    </li>
                                    <li v-for="child in children"
                                        @click="fixCat(child.name)"
                                        style="margin-right: 22px">
                                        @{{ child.name }}
                                        <i id="angle-left"
                                           v-if="child.children_recursive.length">˂</i>
                                    </li>
                                </ul>
                            </div>
                        </td>
                        <td class="position-relative">
                            <div class="position-relative">
                                <input type="text" class="form-control col-12 d-inline-block"
                                       style="padding: 4px 10px;"
                                       v-model="search.brand"
                                       @keyup="checkSearchBrand"
                                       @keyup.enter="searchBrand" placeholder="جستجو...">
                                <a class="btn btn-success btn-sm btn-search" @click="searchBrand"
                                   style="border:unset;height: 100%">
                                    <img src="/assets/images/search-white.svg">
                                </a>
                            </div>
                        </td>
                        <td class="position-relative">
                            <div class="position-relative">
                                <input type="text" class="form-control col-12 d-inline-block"
                                       style="padding: 4px 10px;"
                                       v-model="search.shamsi_c"
                                       @keyup="checkSearchShamsi"
                                       @keyup.enter="searchShamsi_c" placeholder="جستجو...">
                                <a class="btn btn-success btn-sm btn-search" @click="searchShamsi_c"
                                   style="border:unset;height: 100%">
                                    <img src="/assets/images/search-white.svg">
                                </a>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr v-for="product in products.data" :id=`tr${product.id}`
                        @click="selectRow(product.id)">

                        <td>@{{product.name}}</td>
                        <td>@{{product.cat.name}}</td>
                        <td>@{{product.brand.name_f}}</td>
                        <td>@{{product.shamsi_c}}</td>
                        <td>
                            <a :href=`/admin/exist/set/${product.id}` target="_blank"
                               style="background-color: #3490dc;padding: 3px 6px;color: white;border-radius: 4px;font-size: 14px" class="ml-2">
                               موجودی
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3">
            <pagination :data="products" @pagination-change-page="fetchProducts" style="margin:auto"></pagination>
        </div>
        <div v-if="showModal">
            <transition name="modal">
                <div class="modal-mask">
                    <div class="modal-dialog-scrollable" role="document" style="max-width:100%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" @click="showModal = false">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" style="text-align: center">
                                <img v-if="pic" :src="'/images/brand/'+pic" style="width: 100%">
                                <p v-if="description">@{{ description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                pluss: true,
                pluss2: false,
                fflag: false,
                fflag2: false,
                fflag3: false,
                showModal: false,
                cats: false,
                search: {
                    id: '',
                    name: '',
                    brand: '',
                    shamsi_c: '',
                    exist: '',
                    existless: '',
                    existmore: '',
                    shamsiless: '',
                    shamsimore: '',
                    more: '',
                    less: '',
                    price: '',
                    cat: '',
                },
                description: '',
                products: [],
                //tree
                holder: {
                    selfName: 'ریشه',
                    selfId: '',
                    parentName: '',
                    parentId: '',
                    grandName: 'ریشه',
                    grandId: '',
                },
                flag: false,
                flag1: true,
                flag2: false,
                roots: [],
                childs: [],
                form: {
                    cat: 'انتخاب کنید...',
                },
                children: [],
            },
            methods: {
                checkSearchShamsi() {
                    if (this.search.shamsi_c.length === 0) {
                        this.fetchProducts()
                    }
                },
                checkSearchPrice() {
                    if (this.search.price.length === 0) {
                        this.fetchProducts()
                    }
                },
                checkSearchName() {
                    if (this.search.name.length === 0) {
                        this.fetchProducts()
                    }
                },
                checkSearchBrand() {
                    if (this.search.brand.length === 0) {
                        this.fetchProducts()
                    }
                },
                setExist(id) {
                    window.location.href = `/admin/exist/set/${id}`;
                },
                fetchProducts(page = 1) {
                    let vm = this
                    axios.get('/admin/product/all/fetch?page=' + page).then(res => {
                        vm.products = res.data
                        vm.flag = false
                        vm.form.cat = 'انتخاب کنید...'
                    })
                },
                toggleFlag() {
                    if (this.pluss === false) {
                        this.pluss = true;
                    } else {
                        if (this.pluss === true) {
                            this.pluss = false;
                        }
                    }
                    if (this.pluss2 === false) {
                        this.pluss2 = true;
                    } else {
                        if (this.pluss2 === true) {
                            this.pluss2 = false;
                        }
                    }
                    if (this.fflag === false) {
                        this.fflag = true;
                    } else {
                        if (this.fflag === true) {
                            this.fflag = false;
                        }
                    }
                },
                async searchCat(page = 1) {
                    await this.fetchCatId();
                    this.search.brand = ''
                    this.search.price = ''
                    this.search.shamsi_c = ''
                    data = this;
                    if (this.search.cat > 0) {
                        await axios.get('/admin/product/all/search?page=' + page, {
                            params: {
                                cat: this.search.cat,
                                brand: this.search.brand,
                                price: this.search.price,
                                less: this.search.less,
                                more: this.search.more,
                                exist: this.search.exist,
                                shamsi_c: this.search.shamsi_c,
                            }
                        }).then(response => {
                            data.products = response.data;
                        });
                    }
                },
                searchName(page = 1) {
                    this.search.cat = ''
                    this.search.brand = ''
                    this.search.price = ''
                    this.search.shamsi_c = ''
                    let vm = this
                    if (this.search.name.length > 0) {
                        axios.get('/admin/product/all/search?page=' + page, {params: {name: this.search.name}}).then(res => {
                            vm.products = res.data
                        })
                    }

                },
                searchBrand(page = 1) {
                    this.search.name = ''
                    this.search.cat = ''
                    this.search.price = ''
                    this.search.shamsi_c = ''
                    let vm = this
                    if (this.search.brand.length > 0) {
                        axios.get('/admin/product/all/search?page=' + page, {
                            params: {
                                price: this.search.price,
                                brand: this.search.brand,
                                cat: this.search.cat,
                                less: this.search.less,
                                more: this.search.more,
                                exist: this.search.exist,
                                shamsi_c: this.shamsi_c,
                            }
                        }).then(res => {
                            vm.products = res.data;
                        });
                    }
                    if (this.search.brand.length === 0) {
                        this.fetchProducts()
                    }
                },
                searchShamsi_c(page = 1) {
                    this.search.name = ''
                    this.search.cat = ''
                    this.search.brand = ''
                    this.search.price = ''
                    data = this;
                    if (this.search.shamsi_c.length > 0) {
                        axios.get('/admin/product/all/search?page=' + page, {params: {shamsi_c: this.search.shamsi_c}}).then(response => {
                            data.products = response.data;
                        });
                    }
                    if (this.search.shamsi_c.length === 0) {
                        this.fetchProducts();
                    }
                },
                searchPrice(page = 1) {
                    this.search.name = ''
                    this.search.id = ''
                    this.search.less = ''
                    this.search.more = ''
                    let vm = this
                    if (this.search.price.length > 0) {
                        axios.get('/admin/product/all/search?page=' + page, {
                            params: {
                                price: this.search.price, brand: this.search.brand,
                            }
                        }).then(res => {
                            vm.products = res.data
                        })
                    }
                    if (this.search.price.length === 0) {
                        this.fetchProducts()
                    }
                },
                numberFormat(number) {
                    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },

                clickCatBtn() {
                    if (this.flag === false) {
                        this.flag = true;
                    } else if (this.flag === true) {
                        this.flag = false
                    }
                },
                fetchRootCat() {
                    let data = this;
                    axios.get(`/admin/brand/fetch/cat/root`).then(res => {
                        data.roots = res.data;
                    });
                    this.holder.parentName = 'ریشه';
                    this.holder.parentId = '';
                    this.holder.grandName = 'ریشه';
                    this.holder.grandId = '';
                },
                fetchChild(id, name) {
                    let data = this;

                    this.holder.grandName = this.holder.parentName;
                    this.holder.grandId = this.holder.parentId;

                    this.holder.parentName = this.holder.selfName;
                    this.holder.parentId = this.holder.selfId;

                    this.holder.selfName = name;
                    this.holder.selfId = id;
                    axios.get(`/admin/brand/fetch/cat/child/${id}`).then(res => {
                        data.children = res.data;
                        data.flag1 = false;
                        data.flag2 = true;
                    });
                },
                back(parent) {
                    let data = this;
                    if (parent === 'ریشه') {
                        this.flag1 = true;
                        this.flag2 = false;
                        this.holder.selfName = 'ریشه';
                        this.holder.selfId = '';
                        this.holder.parentName = '';
                        this.holder.parentId = '';
                        this.holder.grandName = '';
                        this.holder.grandId = '';
                        axios.get('/admin/mega/fetch/cat/root').then(res => {
                            data.roots = res.data;
                        });
                    } else {
                        axios.get(`/admin/mega/fetch/cat/child/${this.holder.parentId}`).then(res => {
                            data.childs = res.data;
                            data.holder.selfName = data.holder.parentName;
                            data.holder.selfId = data.holder.parentId;
                            data.holder.parentName = data.holder.grandName;
                            data.holder.parentId = data.holder.grandId;
                        });
                    }
                },
                fixCat(catName) {
                    this.form.cat = catName
                    this.flag = false
                    this.searchCat()
                },
                async fetchCatId() {
                    let _this = this;
                    await axios.get(`/admin/product/fetch/cat/id/${_this.form.cat}`).then(res => {
                        _this.search.cat = res.data;
                    });
                },
                selectRow(id) {
                    $(this.holderClass).removeClass('select-row');
                    this.holderClass = `#tr${id}`;
                    $(`#tr${id}`).addClass('select-row');
                },
            },
            mounted() {
                this.fetchProducts();
                this.fetchRootCat();
            },
            updated() {
                if (this.holder.parentName === this.holder.selfName) {
                    this.holder.parentName = 'ریشه';
                    this.holder.parentId = '';
                    this.holder.grandName = 'ریشه';
                    this.holder.grandId = '';
                }
            }
        })
    </script>
    <script>
        $("#side_exist").addClass("menu-open");
        $("#side_exist_index").addClass("active");
    </script>
@endsection

@section('style')
    <style>
        .btn-exist {
            background: unset;
            color: #007bff;
            padding: 1px 7px;
            font-size: 12px;
            border-radius: 4px;
            font-weight: bold;
            margin-bottom: 3px;
            display: block;
            text-decoration: underline;
        }

        .value-center {
            width: 90% !important;
        }

        .value-center::placeholder {
            text-align: right;
            direction: rtl
        }

        .value-center:not(placeholder_shown) {
            text-align: center;
            direction: ltr
        }


        #btn-cat {
            position: relative;
            background-color: white;
            border: 1px solid #ced4da;
            width: 180px;
            padding: 5px 8px 3px 8px;
            border-radius: 5px;
            text-align: right;
            color: #484848;
        }

        #btn-cat:focus {
            outline: unset;
            color: #495057;
            background-color: #fff;
            border-color: #80bdff;
            box-shadow: inset 0 0 0 transparent, 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }

        #angle-down {
            float: left;
            color: #636363;
            font-size: 40px;
            margin: 0px 0px 0px -4px;
            height: 0;
        }

        .btn-search {
            position: absolute;
            top: 0px;
            left: 0px;
            border-radius: 5px 0 0 5px;
            padding: 7px 6px;
        }

        .btn-search img {
            width: 15px;
        }

        .display-none {
            display: none
        }


        .left {
            float: right;
            font-weight: bold;
            font-size: 22px;
            margin: 3px -2px 0px 5px;
        }

        #angle-left {
            float: left;
            color: #636363;
            font-weight: bold;
            font-size: 22px;
        }

        .modal-mask {
            position: fixed !important;
            z-index: 9998 !important;
            top: 0 !important;
            left: 0 !important;
            width: 82.5% !important;
            height: 100vh !important;
            background-color: rgba(0, 0, 0, .5) !important;
            display: table !important;
            transition: opacity .3s ease !important;
        }

        .modal-content {
            max-height: calc(100vh - -3.5rem) !important;
            height: 100vh
        }

    </style>

    <style>
        li {
            list-style: none
        }

        #sss {
            position: absolute;
            list-style: none;
            z-index: 99;
            background-color: white;
            padding: 5px 15px;
            width: 210px;
            line-height: 35px;
            box-shadow: 0px 10px 21px 0px rgba(0, 0, 0, 0.75);
        }


        #sss li {
            cursor: pointer
        }

        .fa {
            font-size: 1.1rem
        }

        .btn-success {
            background-color: #b5b5b5;
            border-color: #b5b5b5;
        }

        .select-row {
            border: 2px #6cafff dotted;
        }

    </style>
@endsection
