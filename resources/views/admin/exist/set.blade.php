@extends('layouts.admin.app')
@section('content')
    <div class="container mt-5 pb-5" id="area" style="position: relative">
        <span style="position: absolute;margin-right:10px;top: -14px;background-color: #f4f6f9;color: #9f9f9f;">افزودن تعداد برای
        <i style="color: #007bff;font-weight: bold;">{{$product->name}}</i>
        </span>

        <div class="row" style="border: 1px #dedede solid;padding:35px 5px 35px 5px;border-radius: 10px">
            <div class="col-md-12">
                <div class="container">
                    <div class="row mt-4 mt-lg-3 mb-5" v-if="colors.length">
                        <div class="col-12">
                            <div class="example ex1" style="display: inline-block">
                                <template v-for="color in colors">
                                    <label class="radio red ml-2" style="background-color: unset;">
                                        <a style="display: inline-block">@{{ color.name }}</a>
                                        <input type="radio" name="group1"
                                               :value="color.price" :id="'color'+color.id"
                                               @change="onChangeColor($event,color.id)"/>
                                        <span :style="{ backgroundColor: color.code}"
                                              style=" width: 45px;height: 27px;display: inline-block;vertical-align: bottom;">
                                        </span>
                                    </label>
                                </template>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-2 mb-4" v-if="sizes.length">
                                        <div class="form-group">
                                            <label for="name_f" class="col-12 control-label">سایز</label>
                                            <select class="form-control" v-model="form.sizeId">
                                                <option value="null" disabled hidden>انتخاب کنید...</option>
                                                <option v-for="size in sizes" :value="size.id">
                                                    @{{ size.name }}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mx-lg-3">
                                        <div class="form-group">
                                            <label for="number" class="col-12 control-label">تعداد</label>
                                            <input id="number" type="number" class="form-control"
                                                   v-model="form.num">
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-submit">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary w-100" @click="formSubmit">
                                                ثبت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12">
                <div class="scrollme">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th v-if="colors.length" scope="col">رنگ</th>
                            <th v-if="sizes.length" scope="col">سایز</th>
                            <th scope="col">تعداد</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="exist in exists">
                            <td v-if="colors.length">@{{exist.color.name}}</td>

                            <td v-if="sizes.length">@{{ exist.size.name }}</td>

                            <td>@{{ exist.num }}</td>
                            <td>
                                <a @click="deleteExist(exist.id)" style="font-size: 20px;">
                                    <i style="color: #dc3545;font-style: normal;font-size: 18px;">✖</i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection
@section('script')
    <script>
        new Vue({
            el: '#area',
            data: {
                colors: [],
                sizes: [],
                exists: [],
                form: {
                    color_id: null,
                    sizeId: null,
                    num: 0,
                },
            },
            methods: {
                colorsFetch() {
                    let vm = this
                    axios.get(`/admin/exist/fetch/color/{{$product->id}}`).then(res => {
                        vm.colors = res.data
                    })
                },
                sizesFetch() {
                    let vm = this
                    axios.get(`/admin/exist/fetch/sizes/{{$product->id}}`).then(res => {
                        vm.sizes = res.data
                    })
                },
                onChangeColor(event, color) {
                    this.form.color_id = color;
                },
                fetchExists() {
                    let _this = this;
                    let parts = window.location.href.split('/')
                    let id = parts.pop() || parts.pop()
                    axios.get(`/admin/exist/fetch/${id}`).then(res => {
                        _this.exists = res.data
                    })
                },
                deleteExist(id) {
                    let _this = this
                    swal.fire({
                        text: "آیا از پاک کردن اطمینان دارید ؟",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'بله',
                        cancelButtonText: 'لغو',
                    }).then(result => {
                        if (result.value) {
                            axios.get(`/admin/exist/delete/${id}`)
                                .then(() => {
                                    swal.fire(
                                        {
                                            text: " با موفقیت حذف شد !",
                                            icon: "success",
                                            confirmButtonText: 'باشه',
                                        }
                                    );
                                    _this.fetchExists();
                                }).catch(() => {
                                swal.fire(
                                    {
                                        text: "درخواست شما انجام نشد !",
                                        icon: "error",
                                        confirmButtonText: 'باشه',
                                    }
                                )
                            });
                        }
                    });

                },
                formSubmit() {
                    let vm = this
                    let formData = new FormData()

                    if (this.sizes.length != 0) {
                        formData.append('size_id', this.form.sizeId)
                    } else {
                        formData.append('size_id', 'size not set')
                    }

                    if (this.colors.length != 0) {
                        formData.append('color_id', this.form.color_id)
                    } else {
                        formData.append('color_id', 'color not set')
                    }

                    formData.append('num', this.form.num)
                    formData.append('product_id', {{$product->id}})

                    axios.post('/admin/exist/store/num', formData)
                        .then(() => {
                            swal.fire({
                                text: " با موفقیت ثبت شد !",
                                icon: "success",
                                confirmButtonText: 'باشه',
                            })
                            vm.fetchExists()
                        })
                },
            },
            mounted() {
                this.colorsFetch()
                this.sizesFetch()
                this.fetchExists()
            },
        });
    </script>
@endsection
@section('style')
    <style>
        #Reviews img {
            width: 70px;
            border-radius: 60px;
        }

        body {
            background-color: white
        }

        #dis {
            float: left;
            font-size: 15px;
            background-color: #07d765;
            color: white;
            padding: 8px;
            border-radius: 30px;
        }
    </style>
    <style>
        .example input {
            display: none;
        }

        .example label {
            margin-right: 20px;
            display: inline-block;
            cursor: pointer;
        }

        .ex1 span {
            display: block;
            padding: 5px 10px 5px 25px;
            border: 2px solid #ddd;
            border-radius: 5px;
            position: relative;
            transition: all 0.25s linear;
        }

        .ex1 span:before {
            content: '';
            position: absolute;
            left: 5px;
            top: 50%;
            -webkit-transform: translatey(-50%);
            transform: translatey(-50%);
            width: 10px;
            height: 10px;
            border-radius: 50%;
            background-color: #ffffff;
            transition: all 0.25s linear;
            border: 2px solid #ffffff;
        }

        .ex1 .red input:checked + span:before {
            background-color: #0aa0d7;

        }

        .answer {
            position: relative;
        }

        .answer:after {
            position: absolute;
            top: 50%;
            right: -40px;
            height: 2px;
            width: 40px;
            content: "";
            background-color: #e0e0e0;
        }

        #Description p {
            line-height: 35px
        }

        textarea {
            border-color: #e0e0e0;
        }

        body {
            background-color: #eceff3;
        }

        .product:hover {
            box-shadow: unset !important;
        }
    </style>
    <style>
        .product-gallery__featured {
            box-shadow: inset 0 0 0 2px #f2f2f2;
            padding: 2px;
            border-radius: 2px
        }

        .product-gallery__featured a {
            display: block;
            padding: 20px
        }

        .product-gallery__carousel {
            margin-top: 16px
        }

        .product-gallery__carousel-item {
            cursor: pointer;
            display: block;
            box-shadow: inset 0 0 0 2px #f2f2f2;
            padding: 12px;
            border-radius: 2px
        }

        .product-gallery__carousel-item--active {
            box-shadow: inset 0 0 0 2px #ffd333
        }

        .product-tabs {
            margin-top: 50px
        }

        .product-tabs__list {
            display: -ms-flexbox;
            display: flex;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
            margin-bottom: -2px
        }

        .product-tabs__list:after,
        .product-tabs__list:before {
            content: "";
            display: block;
            width: 8px;
            -ms-flex-negative: 0;
            flex-shrink: 0
        }

        .product-tabs__item {
            font-size: 20px;
            padding: 18px 48px;
            border-bottom: 2px solid transparent;
            color: inherit;
            font-weight: 500;
            border-radius: 3px 3px 0 0;
            transition: all .15s
        }

        .product-tabs__item:hover {
            color: inherit;
            background: #f7f7f7;
            border-bottom-color: #d9d9d9
        }

        .product-tabs__item:first-child {
            margin-right: auto
        }

        .product-tabs__item:last-child {
            margin-left: auto
        }

        .product-tabs__item--active {
            transition-duration: 0s
        }

        .product-tabs__item--active,
        .product-tabs__item--active:hover {
            cursor: default;
            border-bottom-color: #ffd333;
            background: transparent
        }

        .product-tabs__content {
            border: 2px solid #f0f0f0;
            border-radius: 2px;
            padding: 80px 90px
        }

        .product-tabs__pane {
            overflow: hidden;
            height: 0;
            opacity: 0;
            transition: opacity .5s
        }

        .product-tabs__pane--active {
            overflow: visible;
            height: auto;
            opacity: 1
        }

        .product-tabs--layout--sidebar .product-tabs__item {
            padding: 14px 30px
        }

        .product-tabs--layout--sidebar .product-tabs__content {
            padding: 48px 50px
        }

        .radio-border {
            border: 2px solid red
        }

        .col-submit {
            margin-top: 32px
        }

        .form-group label {
            font-size: 15px;
        }

        .scrollme {
            overflow-x: auto;
        }
    </style>
@endsection
