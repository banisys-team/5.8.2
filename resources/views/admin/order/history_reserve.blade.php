@extends('layouts.admin.app')
@section('content')
    <div class="container-fluid mt-4" id="content">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group d-flex flex-row ">
                    <label for="name" class="col-md-3 mt-2 control-label"
                           style="display: inline-block;text-align: left">شماره همراه کاربر :</label>
                    <input id="auto-focus" type="text" class="col-md-5 form-control" @keyup.enter="formSubmit"
                           style="display: inline-block;direction: ltr;border-bottom-left-radius: 0;border-top-left-radius: 0;"
                           maxlength="11"
                           v-model="code">
                    <button type="button" class="btn btn-dark" @click="formSubmit"
                            style="border: unset;border-bottom-right-radius: 0;border-top-right-radius: 0;background-color: #343a40">
                        <img src="/assets/images/search-white.svg" style="width: 18px">
                    </button>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <tbody>
                    <tr v-for="(factors , key ,index) in factorGroups">
                        <td>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>
                                        <span>فاکتورها: </span>
                                        <a :href=`/factor/${factor.id}` target="_blank"
                                           class="factor" v-for="factor in factors">
                                            @{{ factor.id }}
                                        </a>
                                    </td>
                                    <td>
                                        <span>تاریخ درخواست کاربر: </span>
                                        <span style="color: #3490dc;font-weight: bold">
                                            @{{ factors[0].reserve_shamsi }}
                                        </span>
                                    </td>
                                    <td>
                                        <span>ش.پیگیری هزینه ارسال: </span>
                                        <span style="color: #3490dc;font-weight: bold">
                                            @{{ factors[0].refid_delivery }}
                                        </span>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                factorGroups: [],
                code: '',
                cookie: '',
            },
            methods: {
                formSubmit() {
                    let _this = this
                    let formData = new FormData()
                    formData.append('mobile', this.code)

                    axios.post('/admin/order/reserve/search/mobile', formData).then((res) => {
                            _this.factorGroups = res.data
                        })
                },
                changeExist(e) {
                    e.preventDefault();
                    let _this = this;
                    let formData = new FormData();
                    formData.append('num', this.product.num);
                    formData.append('code', this.code);

                    axios.post('/admin/exist/product/code/change/num', formData)
                        .then(function (response) {
                            swal.fire(
                                {
                                    text: " با موفقیت ثبت شد !",
                                    type: "success",
                                    confirmButtonText: 'باشه',
                                }
                            );
                        })
                        .catch(function (error) {

                        });
                },
                numberFormat(price) {
                    price = Math.trunc(price);
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                removeDecimal(num) {
                    return parseInt(num);
                }
            },
            mounted(){
                document.getElementById("auto-focus").focus()
            }
        })
    </script>
    <script>
        $("#side_order").addClass("menu-open");
        $("#side_order_history").addClass("active");
    </script>
@endsection

@section('style')
    <style>
        .factor {
            text-decoration: underline !important;
            margin-left: 25px;
            color: #0b94c8 !important;
            font-weight: bold;
        }
    </style>
@endsection
