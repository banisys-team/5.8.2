@extends('layouts.admin.app')
@section('content')
    <div class="container-fluid" id="area">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th scope="col">موبایل</th>
                        <th scope="col">فاکتورها</th>
                        <th scope="col">وضعیت</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr v-for="user in users.data">
                        <td>@{{  user.mobile }}</td>

                        <td>
                            <a :href=`/factor/${factor.id}` target="_blank"
                               class="factor" v-for="factor in user.factors_wanted">
                                @{{ factor.id }}
                            </a>
                        </td>
                        <td>
                            <select class="form-control" @change="status(user.id,$event)">
                                <option value="0" :selected="0 == user.status_order">در صف بررسی</option>
                                <option value="1" :selected="1 == user.status_order">آماده ارسال</option>
                                <option value="2" :selected="2 == user.status_order">ارسال شد</option>
                                <option value="3" :selected="3 == user.status_order">رزرو شد</option>
                                <option value="4" :selected="4 == user.status_order">لغو شد</option>
                            </select>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-2 mr-3">
            <div class="col-12" style="text-align:center">
                <pagination :limit="1" :data="users" @pagination-change-page="fetchUsers"></pagination>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#area',
            data: {
                users: [],
            },
            methods: {
                numberFormat(price) {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                getHours(created_at) {
                    let result = created_at.split(" ");
                    return result[1];
                },
                fetchUsers(page = 1) {
                    let _this = this
                    axios.get('/admin/order/reserve/users/fetch?page=' + page).then(res => {
                        _this.users = res.data
                    })
                },
                toggleFlag() {
                    if (this.pluss === false) {
                        this.pluss = true;
                    } else {
                        if (this.pluss === true) {
                            this.pluss = false;
                        }
                    }
                    if (this.pluss2 === false) {
                        this.pluss2 = true;
                    } else {
                        if (this.pluss2 === true) {
                            this.pluss2 = false;
                        }
                    }
                    if (this.flag === false) {
                        this.flag = true;
                    } else {
                        if (this.flag === true) {
                            this.flag = false;
                        }
                    }
                },
                deleteOrder(id) {
                    swal.fire({
                        text: "آیا از پاک کردن اطمینان دارید ؟",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'بله',
                        cancelButtonText: 'لغو',
                    }).then((result) => {
                        if (result.value) {
                            axios.get(`/admin/order/delete/${id}`).then(res => {
                                this.fetchOrders();
                            });
                        }
                    });
                },
                factor(id) {
                    window.open(`/factor/${id}`, '_blank');
                },
                confirm(id) {
                    let _this = this;
                    axios.get(`/admin/factor/confirm/${id}`).then(function () {
                        swal.fire(
                            {
                                text: "محصولات موجود در فاکتور از انبار کسر شد !",
                                type: "success",
                                confirmButtonText: 'باشه',
                            }
                        );
                        _this.fetchNotConfirm();
                        _this.confirmFlag = true;
                    });
                },
                selectRow(id) {
                    $(this.holderClass).removeClass('select-row');
                    this.holderClass = `#tr${id}`;
                    $(`#tr${id}`).addClass('select-row');
                },
                fetchNotConfirm(page = 1) {
                    this.confirmFlag = false;
                    let _this = this;
                    axios.get('/admin/order/not-confirm/fetch?page=' + page).then(res => {
                        _this.orders = res.data;
                    });
                },
                fetchConfirm(page = 1) {
                    this.confirmFlag = true;
                    let _this = this;
                    axios.get('/admin/order/confirm/fetch?page=' + page).then(res => {
                        _this.orders = res.data;
                    });
                },
                status(id, event) {
                    let _this = this
                    axios.post(`/admin/order/reserve/change/status/${id}`, {
                        status: event.target.value,
                    }).then(() => {
                        swal.fire(
                            {
                                text: "تغییرات با موفقیت اعمال شد !",
                                type: "success",
                                confirmButtonText: 'باشه',
                            }
                        )
                        _this.fetchUsers()
                    })
                },
            },
            mounted() {
                this.fetchUsers()
            }
        });
    </script>

    <script>
        $("#side_order").addClass("menu-open")
        $("#side_order_user").addClass("active")
    </script>
@endsection

@section('style')
    <style>
        .factor {
            text-decoration: underline !important;
            margin-left: 25px;
            color: #0b94c8 !important;
            font-weight: bold;
        }

        .pagination {
            display: inline-flex;
        }

        .select-row {
            border: 2px #6bb2ff dotted;
        }
    </style>
@endsection
