@extends('layouts.admin.app')
@section('content')
    <div class="container-fluid" id="content">
        <div class="row">
            <div class="col-12">
                <div class="scrollme">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 15%">شماره فاکتور</th>
                            <th style="width: 20%">همراه</th>
                            <th style="width: 20%">تاریخ</th>
                            <th style="width: 20%">وضعیت</th>
                            <th style="width: 25%">
                                <span style="font-size: 13px;color: #808080;">مجموع:</span>
                                <span style="font-weight: bold;color: #808080;">@{{ orders.total }}</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <div class="position-relative">
                                    <input type="text" class="form-control col-12 d-inline-block"
                                           v-model="search.id"
                                           @keyup="checkSearchId"
                                           style="padding: 4px 10px;"
                                           @keyup.enter="searchId" placeholder="جستجو...">
                                    <a class="btn btn-success btn-sm btn-search d-none d-lg-block" @click="searchId"
                                       style="border:unset;height: 100%">
                                        <img src="/assets/images/search-white.svg">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="position-relative">
                                    <input type="text" class="form-control col-12 d-inline-block"
                                           v-model="search.mobile"
                                           @keyup="checkSearchMobile"
                                           style="padding: 4px 10px;"
                                           @keyup.enter="searchMobile" placeholder="جستجو...">
                                    <a class="btn btn-success btn-sm btn-search d-none d-lg-block" @click="searchMobile"
                                       style="border:unset;height: 100%">
                                        <img src="/assets/images/search-white.svg">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="position-relative">
                                    <input type="text" class="form-control col-12 d-inline-block"
                                           v-model="search.shamsi_c"
                                           @keyup="checkSearchShamsi_c"
                                           style="padding: 4px 10px;"
                                           @keyup.enter="searchShamsi_c" placeholder="جستجو...">
                                    <a class="btn btn-success btn-sm btn-search d-none d-lg-block"
                                       @click="searchShamsi_c"
                                       style="border:unset;height: 100%">
                                        <img src="/assets/images/search-white.svg">
                                    </a>
                                </div>

                                <div class="position-relative my-2" v-if="flag">
                                    <input type="text" class="form-control col-12 d-inline-block"
                                           v-model="search.shamsiless"
                                           @keyup="checkSearchShamsiLess"
                                           style="padding: 4px 10px;"
                                           @keyup.enter="searchShamsiLess" placeholder="قبل از...">
                                    <a class="btn btn-success btn-sm btn-search d-none d-lg-block"
                                       @click="searchShamsi_c"
                                       style="border:unset;height: 100%">
                                        <img src="/assets/images/search-white.svg">
                                    </a>
                                </div>

                                <div class="position-relative" v-if="flag">
                                    <input type="text" class="form-control col-12 d-inline-block"
                                           v-model="search.shamsimore"
                                           @keyup="checkSearchShamsiMore"
                                           style="padding: 4px 10px;"
                                           @keyup.enter="searchShamsiMore" placeholder="بعد از...">
                                    <a class="btn btn-success btn-sm btn-search d-none d-lg-block"
                                       @click="searchShamsi_c"
                                       style="border:unset;height: 100%">
                                        <img src="/assets/images/search-white.svg">
                                    </a>
                                </div>

                            </td>
                            <td>
                                <select class="form-control" @change="searchStatus()" v-model="search.status">
                                    <option value="" disabled hidden>انتخاب کنید...</option>
                                    <option value="all">کل سفارشات</option>
                                    <option value="0">در صف بررسی</option>
                                    <option value="1">آماده ارسال</option>
                                    <option value="2">ارسال شد</option>
                                    <option value="3">رزرو شد</option>
                                    <option value="4">لغو شد</option>
                                </select>
                            </td>
                            <td>
                                <i v-if="pluss"
                                   style="color: #888888;font-size:20px;float: left;cursor: pointer;font-weight: bold"
                                   @click="toggleFlag()">+</i>
                                <i v-if="pluss2" class="fa fa-minus"
                                   style="color: #888888;font-size:23px;float: left;cursor: pointer;font-weight: bold"
                                   @click="toggleFlag()">_</i>
                            </td>
                        </tr>
                        <tr v-for="order in orders.data" :class="statusClass(order.reserve)"
                            :id="'tr'+order.id" @click="selectRow(order.id)">
                            <td>@{{order.id}}</td>
                            <td>@{{order.user.mobile}}</td>
                            <td>@{{ getHours(order.created_at) }} - @{{order.shamsi_c}}</td>
                            <td>
                                <select class="form-control" @change="status(order.id,$event)">
                                    <option value="0" :selected="0 == order.status">در صف بررسی</option>
                                    <option value="1" :selected="1 == order.status">آماده ارسال</option>
                                    <option value="2" :selected="2 == order.status">ارسال شد</option>
                                    <option value="3" :selected="3 == order.status">رزرو شد</option>
                                    <option value="4" :selected="4 == order.status">لغو شد</option>
                                </select>
                            </td>
                            <td>
                                <a class="btn-factor mb-1" :href=`/factor/${order.id}` target="_blank">
                                    فاکتور
                                </a>
                                <a class="btn-note mb-1" @click="showNote(order.id)" style="color: white">
                                    یادداشت
                                </a>
                                <a class="btn-post mb-1" @click="showPostTack(order.id)" style="color: white">
                                    کد رهگیری
                                </a>
                                <img v-if="order.bani"
                                     src="/assets/images/bani-logo-short.png" style="width: 19px;margin-right: 5px;">

                                <img v-if="order.manually"
                                     src="/assets/images/admin.svg" style="width:28px;margin-right: 5px;">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row mt-2 mr-3">
            <div class="col-12" style="text-align:center">
                <pagination :limit="1" :data="orders" @pagination-change-page="fetchOrders"></pagination>
            </div>
        </div>

        <div class="container note" v-if="flagNote">
            <div class="row mt-3">
                <div class="col-6">
                    <a @click="closeNote()" class="mr-3" style="color: #dc3545;font-weight: bold;">
                        <i>✖</i>
                    </a>
                    <a class="mr-3" @click="storeNote()" style="color: #008e08;font-weight: bold;">
                        <i>✓</i>
                    </a>
                </div>
                <div class="col-6">
                    <a class="" @click="storeNote()" style="color: #4b4b4b;font-size: 14px">
                        فاکتور: @{{ holderOrderId }}
                    </a>

                    <a class="mr-5" @click="storeNote()" style="color: #4b4b4b;font-size: 14px">
                        @{{ noteDate }}
                    </a>
                </div>

                <div class="col-12 mt-3">
                    <textarea rows="12" class="note-textarea" v-model="note"
                              placeholder="یادداشت خود را وارد کنید..."></textarea>
                </div>
            </div>
        </div>

        <div v-show="modalCart">
            <div id="modal-back" @click="flagModalCart = false"></div>
            <div class="modal-content col-12 col-lg-4">
                <div class="row">
                    <div class="col-12 text-center relative">
                        <input type="text" style="border: unset;padding: 10px;font-size: 18px;"
                               @keyup.enter="storePostTrack" v-model="postTrackNum"
                               class="form-control col-12 d-inline-block" id="posts-tracking"
                               placeholder="کد رهگیری پستی این سفارش را وارد کنید...">
                        <a class="btn-track" @click="storePostTrack">ثبت</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                pluss: true,
                pluss2: false,
                flag: false,
                flag2: false,
                flag3: false,
                cats: false,
                search: {
                    id: '',
                    mobile: '',
                    status: '',
                    shamsi_c: '',
                    shamsiless: '',
                    shamsimore: '',
                },
                description: '',
                orders: [],
                holderClass: '',
                flagNote: false,
                note: '',
                postTrackNum: '',
                noteDate: '',
                flagModalCart: false,
                holderOrderId: null,
                holderOrderId2: null,
            },
            computed: {
                modalCart() {
                    return this.flagModalCart
                }
            },
            methods: {
                checkSearchId() {
                    if (this.search.id.length === 0) {
                        this.fetchOrders()
                    }
                },
                checkSearchMobile() {
                    if (this.search.mobile.length === 0) {
                        this.fetchOrders()
                    }
                },
                checkSearchRefid() {
                    if (this.search.refid.length === 0) {
                        this.fetchOrders()
                    }
                },
                checkSearchShamsi_c() {
                    if (this.search.shamsi_c.length === 0) {
                        this.fetchOrders()
                    }
                },
                checkSearchShamsiLess() {
                    if (this.search.shamsiless.length === 0) {
                        this.fetchOrders()
                    }
                },
                checkSearchShamsiMore() {
                    if (this.search.shamsimore.length === 0) {
                        this.fetchOrders()
                    }
                },
                statusClass(reserve) {
                    if (reserve === 1) return 'orange'
                    if (reserve === 2) return 'green'
                    if (reserve === 3) return 'blue'
                },
                numberFormat(price) {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                },
                getHours(created_at) {
                    let res = created_at.split(" ")
                    let x = res[1].split(":")

                    return `${x[0]}:${x[1]}`
                },
                fetchOrders(page = 1) {
                    let vm = this
                    axios.get(`/admin/order/fetch?page=${page}`).then(res => {
                        vm.orders = res.data

                    })
                },
                status(id, event) {
                    axios.post(`/admin/change/status/${id}`, {
                        status: event.target.value,
                    }).then(() => {
                        swal.fire({
                            text: "تغییرات با موفقیت اعمال شد !",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                    })
                },
                toggleFlag() {
                    if (this.pluss === false) {
                        this.pluss = true;
                    } else {
                        if (this.pluss === true) {
                            this.pluss = false;
                        }
                    }
                    if (this.pluss2 === false) {
                        this.pluss2 = true;
                    } else {
                        if (this.pluss2 === true) {
                            this.pluss2 = false;
                        }
                    }
                    if (this.flag === false) {
                        this.flag = true;
                    } else {
                        if (this.flag === true) {
                            this.flag = false;
                        }
                    }
                },
                searchId(page = 1) {
                    this.search.mobile = ''
                    this.search.status = ''
                    this.search.shamsi_c = ''
                    this.search.shamsiless = ''
                    this.search.shamsimore = ''
                    let vm = this
                    if (this.search.id.length > 0) {
                        axios.get('/admin/order/search?page=' + page,
                            {params: {id: this.search.id}}).then(res => {
                            vm.orders = res.data
                        })
                    }
                },
                searchMobile(page = 1) {
                    this.search.id = ''
                    this.search.status = ''
                    this.search.shamsi_c = ''
                    this.search.shamsiless = ''
                    this.search.shamsimore = ''
                    let vm = this
                    if (this.search.mobile.length > 0) {
                        axios.get('/admin/order/search?page=' + page,
                            {params: {mobile: this.search.mobile}}).then(res => {
                            vm.orders = res.data
                        })
                    }
                },
                searchShamsi_c(page = 1) {
                    this.search.id = ''
                    this.search.mobile = ''
                    this.search.status = ''
                    this.search.shamsiless = ''
                    this.search.shamsimore = ''
                    let vm = this
                    if (this.search.shamsi_c.length > 0) {
                        axios.get('/admin/order/search?page=' + page,
                            {params: {shamsi_c: this.search.shamsi_c}}).then(res => {
                            vm.orders = res.data
                        })
                    }
                },
                searchStatus(page = 1) {
                    this.search.id = ''
                    this.search.mobile = ''
                    this.search.shamsi_c = ''
                    this.search.shamsiless = ''
                    this.search.shamsimore = ''
                    let vm = this
                    if (this.search.status !== 'all') {
                        axios.get(`/admin/order/search?page=${page}`,
                            {params: {status: this.search.status,}}).then(res => {
                            vm.orders = res.data
                        })
                    } else {
                        this.fetchOrders()
                    }
                },
                searchShamsiLess(page = 1) {
                    this.search.id = ''
                    this.search.mobile = ''
                    this.search.status = ''
                    this.search.shamsi_c = ''
                    this.search.shamsimore = ''
                    let vm = this
                    if (this.search.shamsiless.length > 0) {
                        axios.get('/admin/order/search?page=' + page, {params: {shamsiless: this.search.shamsiless}}).then(res => {
                            vm.orders = res.data
                        })
                    }
                    if (this.search.shamsiless.length === 0) {
                        this.fetchOrders();
                    }
                },
                searchShamsiMore(page = 1) {
                    this.search.id = ''
                    this.search.mobile = ''
                    this.search.status = ''
                    this.search.shamsi_c = ''
                    this.search.shamsiless = ''
                    let vm = this
                    if (this.search.shamsimore.length > 0) {
                        axios.get('/admin/order/search?page=' + page, {params: {shamsimore: this.search.shamsimore}}).then(res => {
                            vm.orders = res.data
                        })
                    }
                },
                selectRow(id) {
                    $(this.holderClass).removeClass('select-row');
                    this.holderClass = `#tr${id}`;
                    $(`#tr${id}`).addClass('select-row');
                },
                closeNote() {
                    this.flagNote = false
                    this.note = ''
                },
                showNote(id) {
                    this.flagNote = true
                    this.holderOrderId = id
                    let vm = this
                    axios.get(`/admin/order/note/fetch/${id}`).then((res) => {
                        vm.note = res.data.note
                        vm.noteDate = res.data.shamsi_u
                    })
                },
                storeNote() {
                    let vm = this
                    this.flagNote = true
                    let formData = new FormData
                    formData.append('order_id', this.holderOrderId)
                    formData.append('note', this.note)

                    axios.post('/admin/order/note/store', formData).then(() => {
                        swal.fire({
                            text: "با موفقیت ذخیره شد.",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                        vm.showNote(vm.holderOrderId)
                    })
                },
                showPostTack(id) {
                    this.flagModalCart = true
                    this.holderOrderId2 = id
                    setTimeout(() => {
                        document.getElementById("posts-tracking").focus()
                    }, 200)

                    let vm = this
                    axios.get(`/admin/order/post/track/fetch/${id}`).then((res) => {
                        vm.postTrackNum = res.data.number
                    })

                },
                storePostTrack() {
                    axios.post('/admin/order/post/track/store', {
                        order_id: this.holderOrderId2,
                        number: this.postTrackNum,
                    }).then(() => {
                        swal.fire({
                            text: "با موفقیت ذخیره شد.",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                    })
                    this.postTrackNum = ''
                    this.flagModalCart = false
                },
            },
            mounted() {
                this.fetchOrders()
            }
        });
    </script>

    <script>
        $("#side_order").addClass("menu-open");
        $("#side_order_index").addClass("active");
    </script>
@endsection

@section('style')
    <style>
        .btn-track {
            position: absolute;
            top: 5px;
            left: 6px;
            background: #28a745;
            color: white !important;
            padding: 7px 15px;
            border-radius: 13px 0px 0 13px;
        }

        .btn-search {
            position: absolute;
            top: 0px;
            left: 0px;
            border-radius: 5px 0 0 5px;
            padding: 7px 6px;
        }

        .btn-search img {
            width: 15px;
        }

        .pagination {
            display: inline-flex;
        }

        .select-row {
            border: 2px #6cafff dotted;
        }

        .orange {
            border-right: 8px solid #ffa707;
        }

        .green {
            border-right: 8px solid #38c172;
        }

        .blue {
            border-right: 8px solid #35bcff;
        }

        .btn-success {
            background-color: #b5b5b5;
            border-color: #b5b5b5;
        }

        .btn-factor {
            display: inline-block;
            color: #ffffff;
            padding: 2px 7px;
            font-size: 13px;
            background: #007bff;
            text-align: center;
            border-radius: 5px;
        }

        .btn-factor:hover {
            color: white;
        }


        .btn-note {
            display: inline-block;
            color: #ffffff;
            padding: 2px 7px;
            font-size: 13px;
            background: #28a745;
            text-align: center;
            border-radius: 5px;
        }

        .btn-post {
            display: inline-block;
            color: #ffffff;
            padding: 2px 7px;
            font-size: 13px;
            background: #241b81;
            text-align: center;
            border-radius: 5px;
        }

        .note-textarea {
            width: 100%;
            background: unset;
            border: unset;
            padding: 20px 15px;
        }

        .note-textarea:focus-visible {
            outline: unset;
        }

        .note {
            position: fixed;
            top: 15%;
            left: 30%;
            height: 400px;
            background: #f3e16e;
            width: 450px;
        }

        i {
            font-style: unset;
        }

        #modal-back {
            z-index: 99998;
            left: 0;
            width: 100%;
            height: 100vh;
            background-color: #00000096;
            position: fixed;
            top: 0;
        }

        .modal-mask {
            position: fixed !important;
            top: 0 !important;
            left: 0 !important;
            width: 100% !important;
            height: 100% !important;
            background-color: #00000073 !important;
            display: table !important;
            transition: opacity .3s ease !important;
        }

        .modal-content p {
            font-size: 14px;
            text-align: right
        }

        .modal-content input {
            border: 1px solid #ddd;
            padding: 5px;
            border-radius: 37px;
        }

        .modal-content {
            position: fixed;
            top: 13%;
            left: 50%;
            transform: translateX(-50%);
            border-radius: 20px;
            z-index: 99999;
        }

        .modal-content .form-control:focus {
            color: unset;
            background-color: unset;
            border-color: unset;
            outline: unset;
            box-shadow: unset;
        }

        .scrollme {
            overflow-x: auto;
        }
    </style>
@endsection
