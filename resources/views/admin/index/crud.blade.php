@extends('layouts.admin.app')
@section('content')
    <div class="container mt-5" id="content" style="height: 80vh">
        <div class="row mb-4" v-if="progress">
            <progress :value="percent" max="100" style="width: 100%"></progress>
        </div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>دسته بندی</label>
                                <br>
                                <button @click="clickCatBtn" id="btn-cat">
                                    @{{ catName }}
                                    <i id="angle-down">ˇ</i>
                                </button>

                                <div v-if="flag" id="sss">
                                    <ul v-if="flag1">
                                        <li style="line-height: 35px;"
                                            @click="fetchChild(root.id,root.name)"
                                            v-for="root in roots">
                                            @{{ root.name }}
                                            <i id="angle-left" class="right">›</i>
                                        </li>
                                    </ul>
                                    <ul v-if="flag2" style="padding: 0 6px 0 0;">
                                        <li style="color: #dc3545;font-weight: bold"
                                            @click="back(holder.parentName)">
                                            <i class="left">‹</i>
                                            @{{ holder.parentName }}
                                        </li>
                                        <li style="background: #717171;color: white;border-radius: 4px"
                                            @click="fixCat(holder.selfName)">
                                            @{{ holder.selfName }}
                                        </li>
                                        <li v-for="child in children"
                                            @click="fixCat(child.name)"
                                            style="margin-right: 22px">
                                            @{{ child.name }}
                                            <i id="angle-left"
                                               v-if="child.children_recursive.length">˂</i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mt-4 mt-lg-0">
                            <div class="form-group" style="margin-bottom: 0">
                                <label for="exampleFormControlSelect3">جایگاه</label>
                                <select class="form-control" id="exampleFormControlSelect3"
                                        v-model="priority">
                                    <option value="" disabled hidden>انتخاب کنید...</option>
                                    <option value="index_slider1">اسلایدر اول</option>
                                    <option value="index_slider2">اسلایدر دوم</option>
                                    <option value="index_slider3">اسلایدر سوم</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2 px-lg-3" style="margin-top: 32px;">
                            <button class="btn btn-primary w-100 mt-3 mt-lg-0" @click="storeSlideProduct"
                                    style="border-bottom: 3px solid #0062cc;">
                                ثبت
                            </button>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <div class="scrollme">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr v-if="sliderPro1">
                                        <td>اسلایدر اول</td>
                                        <td>
                                            @{{ sliderPro1 }}
                                        </td>
                                        <td>
                                            <a @click="deleteSlideProduct('index_slider1')"
                                               style="font-size: 16px;display: inline-block;color: #dc3545;vertical-align: sub;">
                                                ✖
                                            </a>
                                        </td>
                                    </tr>
                                    <tr v-if="sliderPro2">
                                        <td>اسلایدر دوم</td>
                                        <td>
                                            @{{ sliderPro2 }}
                                        </td>
                                        <td>
                                            <a @click="deleteSlideProduct('index_slider2')"
                                               style="font-size: 16px;display: inline-block;color: #dc3545;vertical-align: sub;">
                                                ✖
                                            </a>
                                        </td>
                                    </tr>
                                    <tr v-if="sliderPro3">
                                        <td>اسلایدر سوم</td>
                                        <td>
                                            @{{ sliderPro3 }}
                                        </td>
                                        <td>
                                            <a @click="deleteSlideProduct('index_slider3')"
                                               style="font-size: 16px;display: inline-block;color: #dc3545;vertical-align: sub;">
                                                ✖
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                sliderPro1: '',
                sliderPro2: '',
                sliderPro3: '',
                priority: '',
                slides: [],
                slidesDemo: [],
                files: [],
                images: [],
                progress: false,
                percent: 0,
                // tree
                categories: [],
                output: '',
                catName: 'انتخاب کنید...',
                holder: {
                    selfName: 'ریشه',
                    selfId: '',
                    parentName: '',
                    parentId: '',
                    grandName: 'ریشه',
                    grandId: '',
                },
                flag: false,
                flag1: true,
                flag2: false,
                roots: [],
                children: [],
            },
            methods: {
                fetchSliderProduct() {
                    let vm = this
                    axios.get(`/admin/slider/product/fetch`).then(res => {
                        vm.sliderPro1 = res.data.slider_pro1
                        vm.sliderPro2 = res.data.slider_pro2
                        vm.sliderPro3 = res.data.slider_pro3
                    })
                },
                deleteSlideProduct(key) {
                    let vm = this
                    axios.get(`/admin/slider/product/delete/${key}`).then(() => {
                        swal.fire(
                            {
                                text: " با موفقیت حذف شد !",
                                icon: "success",
                                confirmButtonText: 'باشه',
                            }
                        )
                        vm.fetchSliderProduct()
                    })
                },
                storeSlideProduct() {
                    if (this.catName === 'انتخاب کنید...' || this.priority === '') {
                        swal.fire(
                            {
                                text: "دسته بندی و جایگاه را انتخاب کنید!",
                                icon: "warning",
                                confirmButtonText: 'باشه',
                            }
                        )
                        return
                    }

                    let vm = this
                    axios.post('/admin/slider/product/store', {
                        'cat_name': this.catName,
                        'priority': this.priority,
                    }).then((res) => {
                        swal.fire(
                            {
                                text: " با موفقیت ثبت شد !",
                                icon: "success",
                                confirmButtonText: 'باشه',
                            }
                        )
                        vm.fetchSliderProduct()
                    })
                },
                addImage(file) {
                    this.files.push(file);
                    const reader = new FileReader();
                    reader.onload = (e) => this.images.push(e.target.result);
                    reader.readAsDataURL(file);
                },
                deleteImage(index) {
                    this.images.splice(index, 1);
                    this.files.splice(index, 1);
                },
                addRow() {
                    this.slides.push({});
                },
                deleteRow(index) {
                    this.slides.splice(index, 1)
                },
                onInputChange(e) {
                    const files = e.target.files;
                    Array.from(files).forEach(file => this.addImage(file));
                },
                // tree
                clickCatBtn() {
                    if (this.flag === false) {
                        this.flag = true;
                    } else if (this.flag === true) {
                        this.flag = false
                    }
                },
                fetchRootCat() {
                    let vm = this
                    axios.get(`/admin/product/fetch/cat/root`).then(res => {
                        vm.roots = res.data
                    })
                    this.holder.parentName = 'ریشه'
                    this.holder.parentId = ''
                    this.holder.grandName = 'ریشه'
                    this.holder.grandId = ''
                },
                fetchChild(id, name) {
                    let data = this;

                    this.holder.grandName = this.holder.parentName;
                    this.holder.grandId = this.holder.parentId;

                    this.holder.parentName = this.holder.selfName;
                    this.holder.parentId = this.holder.selfId;

                    this.holder.selfName = name;
                    this.holder.selfId = id;
                    axios.get(`/admin/product/fetch/cat/child/${id}`).then(res => {
                        data.children = res.data;
                        data.flag1 = false;
                        data.flag2 = true;
                    });
                },
                back(parent) {
                    let data = this;
                    if (parent === 'ریشه') {
                        this.flag1 = true;
                        this.flag2 = false;
                        this.holder.selfName = 'ریشه';
                        this.holder.selfId = '';
                        this.holder.parentName = '';
                        this.holder.parentId = '';
                        this.holder.grandName = '';
                        this.holder.grandId = '';
                        axios.get('/admin/mega/fetch/cat/root').then(res => {
                            data.roots = res.data;
                        });
                    } else {
                        axios.get(`/admin/mega/fetch/cat/child/${this.holder.parentId}`).then(res => {
                            data.children = res.data;
                            data.holder.selfName = data.holder.parentName;
                            data.holder.selfId = data.holder.parentId;
                            data.holder.parentName = data.holder.grandName;
                            data.holder.parentId = data.holder.grandId;
                        });
                    }
                },
                fixCat(catName) {
                    this.catName = catName
                    this.flag = false
                },
                fixRoot() {
                    this.form.catName = 'ریشه';
                    this.flag = false;
                },
            },
            mounted() {
                this.fetchRootCat()
                this.fetchSliderProduct()
            },
            updated() {
                if (this.holder.parentName === this.holder.selfName) {
                    this.holder.parentName = 'ریشه'
                    this.holder.parentId = ''
                    this.holder.grandName = 'ریشه'
                    this.holder.grandId = ''
                }
            }
        })
    </script>
    <script>
        $("#side_setting").addClass("menu-open");
        $("#side_index_add").addClass("active");
    </script>
@endsection

@section('style')
    <style>
        body {
            background: #f4f6f9
        }

        .submit-btn {
            background: rgb(9, 144, 32);
            border: unset;
            color: white;
            width: 70px;
            height: 31px;
            position: absolute;
            top: 13px;
            left: 20px;
            border-radius: 50px;
            cursor: pointer;
            font-size: 15px;
        }

        li {
            list-style: none
        }

        #btn-cat {
            position: relative;
            background-color: white;
            border: 1px solid #ced4da;
            width: 100%;
            padding: 8px 8px 4px 8px;
            border-radius: 5px;
            text-align: right;
            color: #484848;
        }

        #btn-cat:focus {
            outline: unset;
            color: #495057;
            background-color: #fff;
            border-color: #80bdff;
            box-shadow: inset 0 0 0 transparent, 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }

        #angle-down {
            float: left;
            color: #636363;
            font-size: 38px;
            margin: -2px 0px 0px -4px;
            height: 0;
            font-style: normal;
        }

        #sss {
            position: absolute;
            list-style: none;
            top: 70px;
            z-index: 99;
            background-color: white;
            padding: 5px 15px;
            width: 100%;
            line-height: 35px;
            border: 1px solid #a1a1a1;
            max-height: 330px;
            overflow: scroll;
            overflow-x: hidden;
        }

        #angle-left {
            float: left;
            color: #636363;
            font-weight: bold;
            font-size: 22px;
            font-style: normal;
        }

        #sss li {
            cursor: pointer
        }

        .left {
            float: right;
            font-weight: bold;
            font-size: 22px;
            margin: 3px -2px 0px 5px;
            font-style: normal;
        }

        .form-group label {
            font-size: 15px;
        }

        .scrollme {
            overflow-x: auto;
        }

    </style>
@endsection

