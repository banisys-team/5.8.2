@extends('layouts.admin.admin')
@section('content')
    <div class="container mt-5 pb-5" id="area" style="position: relative">
        <span style="position: absolute;margin-right:10px;top: -14px;background-color: #f4f6f9;color: #9f9f9f;">افزودن کد پیگیری محصولات پستی</span>
        <div class="row" style="border: 1px #dedede solid;padding:35px 5px 0 5px;border-radius: 10px">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="name" class="col-md-12 control-label">تاریخ :</label>
                                <input id="name" type="text" class="form-control text-center" style="direction: ltr"
                                       v-model="form.date" autofocus>
                            </div>
                        </div>

                        <div class="col-md-5" style="margin-top: 32px">
                            <div class="form-group">
                                <label for="fileImage" id="label_file" class="btn btn-success"
                                       style="font-weight: lighter;margin-bottom: 0;position: relative">انتخاب فایل
                                </label>
                                <input type="file" style="display:none;" id="fileImage"
                                       name="fileImage" @change="onImageChange">
                                <button type="button" class="btn btn-primary mr-5" @click="formSubmit">
                                    ثبت
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-12">
                <table class="table table-striped">
                    <tbody>
                    <tr v-for="track in tracks.data">
                        <td>@{{track.date}}</td>
                        <td>
                            <a :href="`/images/pdf/${track.url}`" target="_blank">فایل</a>
                        </td>
                        <td>
                            <a @click="deleteTrack(track.id)" style="font-size: 20px;">
                                <i class="fa fa-times" style="color: #dc3545"></i>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="row mt-3">
            <pagination :limit="1" :data="tracks" @pagination-change-page="trackFetch" style="margin:auto"></pagination>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#area',
            data: {
                selected_cats: [],
                category: '',
                cats: [],
                showModal: false,
                form: {
                    date: '',
                    image: null,
                },
                search: {
                    id: '',
                    name: '',
                },
                tracks: [],
                pic: '',
                description: '',
                modalCats: false,
                brandHolder: '',
                editCat: '2',
                // tree
                categories: [],
                output: '',
                flag: false,
                flag1: true,
                flag2: false,
            },
            methods: {
                onImageChange(e) {
                    this.form.image = e.target.files[0]
                },
                empty() {
                    this.form.description = "";
                },
                trackFetch(page = 1) {
                    axios.get('/admin/track/fetch/all?page=' + page).then(res => {
                        this.$data.tracks = res.data
                    })
                },
                deleteTrack(id) {
                    axios.delete(`/admin/track/delete/${id}`).then(() => {
                        swal.fire(
                            {
                                text: "با موفقیت حذف شد !",
                                type: "success",
                                confirmButtonText: 'باشه',
                            }
                        )
                    })
                    this.trackFetch()
                },
                formSubmit() {
                    if (this.form.date === '' || this.form.image === null) {
                        swal.fire(
                            {
                                text: "تاریخ و فایل را وارد کنید!",
                                type: "warning",
                                confirmButtonText: 'باشه',
                            }
                        )
                    }
                    const config = {
                        headers: {'content-type': 'multipart/form-data'}
                    }
                    let _this = this
                    let formData = new FormData();
                    formData.append('date', this.form.date);
                    formData.append('pdf', this.form.image);

                    axios.post('/admin/track/store', formData, config).then(() => {
                        _this.trackFetch()
                        _this.form.date = ''
                        _this.form.image = null
                        $("#fileImage").val('')
                    })
                },
            },
            mounted() {
                this.trackFetch()
            },
        })
    </script>
    <script>
        $("#side_track").addClass("menu-open");
        $("#side_track_index").addClass("active");
    </script>
@endsection
@section('style')
    <style>
        .multiselect-container {
            direction: ltr !important;
            width: 100% !important;
        }

        .multiselect-container label {
            display: block !important;
            text-align: right !important;
            color: #717171 !important;
            margin-top: 5px !important;

        }

        .multiselect {
            width: 100% !important;
            text-align: right !important;
            background-color: white;
            min-height: 26px !important;
        }

        .dropdown-toggle::after {
            display: none !important;
        }

        .btn-group {
            width: 100% !important;
        }
    </style>
    <style>
        .modal-mask {
            position: fixed !important;
            z-index: 9998 !important;
            top: 0 !important;
            left: 0 !important;
            width: 100% !important;
            height: 100vh !important;
            background-color: rgba(0, 0, 0, .5) !important;
            display: table !important;
            transition: opacity .3s ease !important;
        }

        .modal-content {
            max-height: calc(100vh - -3.5rem) !important;
            height: 100vh
        }

        .fa {
            font-size: 1.1rem;
        }
    </style>
    <style>
        li {
            list-style: none
        }

        #btn-cat {
            position: relative;
            background-color: white;
            border: 1px solid #ced4da;
            width: 100%;
            padding: 8px 8px 0 8px;
            border-radius: 5px;
            text-align: right;
            color: #484848;
        }

        #angle-down {
            float: left;
            margin: 6px 2px;
            color: #636363;
        }

        #sss {
            position: absolute;
            list-style: none;
            top: 74px;
            z-index: 99;
            background-color: white;
            padding: 5px 15px;
            width: 100%;
            line-height: 35px;
            box-shadow: 0px 10px 21px 0px rgba(0, 0, 0, 0.75);
        }

        #angle-left {
            float: left;
            margin-top: 10px;
            color: #636363;
        }

        #sss li {
            cursor: pointer
        }

        .fa {
            font-size: 1.1rem
        }

        label {
            font-weight: unset !important;
        }

        button:focus {
            outline: rgba(122, 186, 255, 0.68) solid 2px !important;
        }
    </style>
@endsection
