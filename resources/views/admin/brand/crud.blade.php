@extends('layouts.admin.app')
@section('content')
  <div class="container mt-5 pb-5 relative" id="area">
    <span class="add-label">افزودن برند</span>
    <div class="row add-row mx-1">
      <div class="col-12">
        <div class="container">
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name_f" class="col-md-12 control-label">نام فارسی</label>
                <input id="name_f" type="text" class="form-control" autofocus v-model="form.name_f">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name_e" class="col-md-12 control-label">نام لاتین</label>
                <input id="name_e" type="text" class="form-control ltr" v-model="form.name_e">
              </div>
            </div>
            <div class="col-2 col-submit">
              <div class="form-group">
                <button type="button" class="btn btn-primary" @click="formSubmit">ثبت</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mt-5">
      <div class="col-12">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th scope="col">نام فارسی</th>
              <th scope="col">نام لاتین</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <div class="position-relative">
                  <input type="text" class="form-control col-12 d-inline-block" v-model="search.name_f"
                    @keyup="checkSearchLength_f" style="padding: 4px 10px;" @keyup.enter="searchName_f"
                    placeholder="جستجو...">
                  <a class="btn btn-success btn-sm btn-search" @click="searchName_f" style="border:unset;height: 100%">
                    <img src="/assets/images/search-white.svg">
                  </a>
                </div>
              </td>
              <td>
                <div class="position-relative">
                  <input type="text" class="form-control col-12 d-inline-block" v-model="search.name_e"
                    @keyup="checkSearchLength_e" style="padding: 4px 10px;" @keyup.enter="searchName_e"
                    placeholder="جستجو...">
                  <a class="btn btn-success btn-sm btn-search" @click="searchName_e" style="border:unset;height: 100%">
                    <img src="/assets/images/search-white.svg">
                  </a>
                </div>
              </td>
              <td style="position: relative">
                <div style="position: absolute;top: 17px">
                  <span class="d-none d-lg-inline-block" style="font-size: 13px;color: #808080;">مجموع:</span>
                  <span style="font-weight: bold;color: #808080">@{{ brands.total }}</span>
                </div>
              </td>
            </tr>
            <tr v-for="brand in brands.data">
              <td>@{{ brand.name_f }}</td>
              <td>@{{ brand.name_e }}</td>
              <td>
                <a @click="deleteBrand(brand.id)" class="delete">✖</a>
              </td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
    <div class="row mt-3 pagination-container">
      <pagination v-if="flagPaginateNormal" :limit="1" :data="brands"
        @pagination-change-page="fetchBrands">
      </pagination>

      <pagination v-if="flagPaginateName_e" :limit="1" :data="brands"
        @pagination-change-page="searchName_e">
      </pagination>

      <pagination v-if="flagPaginateName_f" :limit="1" :data="brands"
        @pagination-change-page="searchName_f">
      </pagination>
    </div>
  </div>
@endsection
@section('script')
  <script>
    new Vue({
      el: '#area',
      data: {
        form: {
          name_e: '',
          name_f: '',
        },
        search: {
          name_e: '',
          name_f: '',
        },
        brands: [],
        flagPaginateNormal: true,
        flagPaginateName_e: false,
        flagPaginateName_f: false,
      },
      methods: {
        fetchBrands(page = 1) {
          let vm = this
          axios.get('/admin/brand/fetch?page=' + page).then(res => {
            vm.brands = res.data
          })
        },
        deleteBrand(id) {
          let _this = this;
          swal.fire({
            text: "آیا از پاک کردن اطمینان دارید ؟",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'بله',
            cancelButtonText: 'لغو',
          }).then((result) => {
            if (result.value) {
              axios.get(`/admin/brand/delete/${id}`)
                .then((res) => {

                  if (res.data === "cant") {
                    swal.fire({
                      text: "این برند دارای وابستگی می باشد و نمی توان آن را حذف کرد",
                      type: "warning",
                      confirmButtonText: 'باشه',
                    })
                  } else {
                    swal.fire({
                      text: "با موفقیت حذف شد !",
                      type: "success",
                      confirmButtonText: 'باشه',
                    });
                    _this.fetchBrands();
                  }
                })
            }
          });
        },
        searchName_e(page = 1) {
          this.search.name_f = ''
          let vm = this
          if (this.search.name_e.length > 0) {
            axios.post('/admin/brand/search?page=' + page, this.search).then(res => {
              vm.brands = res.data
              if (!vm.brands.data.length) {
                alert('موردی یافت نشد!')
              } else {
                this.flagPaginateName_e = true
                this.flagPaginateNormal = false
                this.flagPaginateName_f = false
              }
            })
          }
          if (this.search.name_e.length === 0) {
            this.flagPaginateNormal = true
            this.flagPaginateName_e = false
            this.flagPaginateName_f = false
            this.fetchBrands()
          }
        },
        searchName_f(page = 1) {
          this.search.name_e = ''
          let vm = this
          if (this.search.name_f.length > 0) {
            axios.post('/admin/brand/search?page=' + page, this.search).then(res => {
              vm.brands = res.data
              if (!vm.brands.data.length) {
                alert('موردی یافت نشد!')
              } else {
                this.flagPaginateName_f = true
                this.flagPaginateNormal = false
                this.flagPaginateName_e = false
              }
            })
          }
          if (this.search.name_f.length === 0) {
            this.flagPaginateNormal = true
            this.flagPaginateName_e = false
            this.flagPaginateName_f = false
            this.fetchBrands()
          }
        },
        checkSearchLength_e() {
          if (this.search.name_e.length === 0) {
            this.fetchBrands()
          }
        },
        checkSearchLength_f() {
          if (this.search.name_f.length === 0) {
            this.fetchBrands()
          }
        },
        formSubmit() {
          if (this.form.name_f === '' || this.form.name_e === '') {
            swal.fire({
              text: "نام فارسی و لاتین را وارد کنید.",
              type: "warning",
              confirmButtonText: 'باشه',
            })
            return
          }

          let vm = this

          axios.post('/admin/brand/store', this.form)
            .then(() => {
              swal.fire({
                text: "با موفقیت ثبت شد.",
                icon: "success",
                confirmButtonText: 'باشه',
              })
              vm.form.name_f = ''
              vm.form.name_e = ''
              vm.fetchBrands()
            })
            .catch((error) => {
              let text = error.response.data.errors
              if (Array.isArray(text.name_f)) alert(text.name_f[0])
              if (Array.isArray(text.name_e)) alert(text.name_e[0])
            })
        },
      },
      mounted() {
        this.fetchBrands()
      }
    })
  </script>

  <script>
    $("#side_brand").addClass("menu-open");
    $("#side_brand_add").addClass("active");

    $(".pagination-container").click(function() {
      window.scroll({
        top: 250,
        left: 0,
        behavior: 'smooth'
      });
    })
  </script>
@endsection
@section('style')
  <style>
    .btn-search {
      position: absolute;
      top: 0px;
      left: 0px;
      border-radius: 5px 0 0 5px;
      padding: 7px 6px;
    }

    .btn-search img {
      width: 15px;
    }

    .btn-success {
      background-color: #b5b5b5;
      border-color: #b5b5b5;
    }


    .add-label {
      position: absolute;
      margin-right: 10px;
      top: 83px;
      background-color: #f4f6f9;
      color: #9f9f9f;
    }

    .add-row {
      border: 1px #dedede solid;
      padding: 35px 5px 15px 5px;
      border-radius: 10px
    }

    .col-submit {
      margin-top: 32px
    }

    .pagination {
      margin: auto
    }

    .delete {
      color: #dc3545 !important;
    }

    .form-group label {
      font-size: 15px;
    }
  </style>
@endsection
