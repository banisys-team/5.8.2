@extends('layouts.admin.app')
@section('content')
  <div class="container pb-5 pt-4" id="content">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleFormControlTextarea1">رویه‌های بازگرداندن کالا</label>
          <textarea class="form-control" name="editor1" rows="12"></textarea>
        </div>
      </div>
      <div class="col-md-12 my-5">
        <div class="form-group">
          <label for="exampleFormControlTextarea1">شرایط تحویل کالا</label>
          <textarea class="form-control" name="editor2" rows="12"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleFormControlTextarea1">قوانین استفاده از سایت</label>
          <textarea class="form-control" name="editor3" rows="12"></textarea>
        </div>
      </div>
      <div class="col-md-12">
        <button type="submit" @click="formSubmit" class="btn btn-primary d-inline-block float-left"
          style="border-bottom: 3px solid #1b639f">
          ثبت
        </button>
      </div>
    </div>

  </div>
@endsection
@section('script')
  <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
  <script>
    new Vue({
      el: '#content',
      data: {
        return: '',
        rules: '',
        delivery: '',
      },
      methods: {
        fetch() {
          axios.get('/admin/page/fetch').then(res => {
            CKEDITOR.instances["editor1"].setData(res.data.return)
            CKEDITOR.instances["editor2"].setData(res.data.rules)
            CKEDITOR.instances["editor3"].setData(res.data.delivery)
          })

        },
        formSubmit() {
          let vm = this
          let formData = new FormData

          let valReturn = CKEDITOR.instances["editor1"].getData()
          let rules = CKEDITOR.instances["editor2"].getData()
          let delivery = CKEDITOR.instances["editor3"].getData()

          formData.append('return', valReturn);
          formData.append('rules', rules);
          formData.append('delivery', delivery);

          axios.post('/admin/page/update', formData).then(res => {
            swal.fire({
              text: "با موفقیت ثبت شد.",
              icon: "success",
              confirmButtonText: 'باشه',
            })
            vm.fetch()
          })
        },
        ckeditor() {
          CKEDITOR.replace('editor1', {
            customConfig: "{{ asset('/js/ckeditor/config.js') }}",
            height: 250
          })
          CKEDITOR.replace('editor2', {
            customConfig: "{{ asset('/js/ckeditor/config.js') }}",
            height: 250,
          })
          CKEDITOR.replace('editor3', {
            customConfig: "{{ asset('/js/ckeditor/config.js') }}",
            height: 250,
          })
        }
      },
      async mounted() {
        await this.ckeditor()
        this.fetch()
      }
    })
  </script>

  <script>
    $("#side_page").addClass("menu-open")
    $("#side_page_edit").addClass("active")
  </script>
@endsection
@section('style')
@endsection
