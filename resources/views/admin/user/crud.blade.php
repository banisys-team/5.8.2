@extends('layouts.admin.app')
@section('content')
    <div class="container mt-5 pb-5 relative" id="content">
        <span class="add-label">افزودن کاربر</span>
        <div class="row add-row">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 mt-3 mt-lg-0">
                            <div class="form-group">
                                <label for="mobile" class="col-md-12 control-label">موبایل</label>
                                <input id="mobile" type="text" class="form-control ltr" autofocus maxlength="11"
                                       v-model="form.mobile" @keyup.enter="formSubmit">
                            </div>
                        </div>
                        <div class="col-lg-3 mt-3 mt-lg-0">
                            <div class="form-group">
                                <label for="mobile" class="col-md-12 control-label">نام کوچک</label>
                                <input id="mobile" type="text" class="form-control" autofocus maxlength="11"
                                       v-model="form.name" @keyup.enter="formSubmit">
                            </div>
                        </div>
                        <div class="col-lg-1 col-submit">
                            <div class="form-group">
                                <button type="button" class="btn btn-primary w-100" @click="formSubmit">ثبت</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-lg-6">
                <div class="scrollme">
                    <table class="table table-striped">
                        <tbody>
                        <tr v-for="user in users.data">
                            <td>
                                @{{user.mobile}}
                            </td>
                            <td>
                                @{{user.name}}
                            </td>
                            <td>
                                <a @click="deleteUser(user.id)">
                                    <i class="del">✖</i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row mt-3 pagination-container">
            <pagination :limit="1" :data="users" @pagination-change-page="fetchUsers"></pagination>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                form: {
                    mobile: '',
                    name: '',
                },
                search: {
                    mobile: '',
                },
                users: [],
            },
            methods: {
                fetchUsers(page = 1) {
                    let vm = this
                    axios.get(`/admin/user/fetch?page=${page}`).then(res => {
                        vm.users = res.data
                    })
                },
                deleteUser(id) {
                    let vm = this
                    axios.get(`/admin/user/delete/${id}`)
                        .then(() => {
                            swal.fire(
                                {
                                    text: "با موفقیت حذف شد !",
                                    icon: "success",
                                    confirmButtonText: 'باشه',
                                }
                            );
                            vm.fetchUsers()
                        })
                },
                searchName_e(page = 1) {
                    this.search.name_f = ''
                    let vm = this
                    if (this.search.name_e.length > 0) {
                        axios.post('/admin/brand/search?page=' + page, this.search).then(res => {
                            vm.brands = res.data
                            if (!vm.brands.data.length) {
                                alert('موردی یافت نشد!')
                            } else {
                                this.flagPaginateName_e = true
                                this.flagPaginateNormal = false
                                this.flagPaginateName_f = false
                            }
                        })
                    }
                    if (this.search.name_e.length === 0) {
                        this.flagPaginateNormal = true
                        this.flagPaginateName_e = false
                        this.flagPaginateName_f = false
                        this.fetchBrands()
                    }
                },
                searchName_f(page = 1) {
                    this.search.name_e = ''
                    let vm = this
                    if (this.search.name_f.length > 0) {
                        axios.post('/admin/brand/search?page=' + page, this.search).then(res => {
                            vm.brands = res.data
                            if (!vm.brands.data.length) {
                                alert('موردی یافت نشد!')
                            } else {
                                this.flagPaginateName_f = true
                                this.flagPaginateNormal = false
                                this.flagPaginateName_e = false
                            }
                        })
                    }
                    if (this.search.name_f.length === 0) {
                        this.flagPaginateNormal = true
                        this.flagPaginateName_e = false
                        this.flagPaginateName_f = false
                        this.fetchBrands()
                    }
                },
                checkSearchLength_e() {
                    if (this.search.name_e.length === 0) {
                        this.fetchBrands()
                    }
                },
                checkSearchLength_f() {
                    if (this.search.name_f.length === 0) {
                        this.fetchBrands()
                    }
                },
                formSubmit() {
                    if (this.form.mobile === '' || this.form.name === '') {
                        swal.fire({
                            text: "موبایل و نام کوچک کاربر را وارد کنید.",
                            type: "warning",
                            confirmButtonText: 'باشه',
                        })
                        return
                    }

                    let vm = this
                    axios.post('/admin/user/store', this.form).then(res => {
                        vm.fetchUsers()
                        if (res.data === 0) {
                            swal.fire({
                                text: "این کاربر قبلا ثبت شده است.",
                                icon: "warning",
                                confirmButtonText: 'باشه',
                            })
                        } else {

                            const Toast = swal.mixin({
                                toast: true,
                                position: 'bottom-start',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'با موفقیت ثبت شد'
                            })
                            vm.form.mobile = ''
                            vm.form.name = ''
                            document.getElementById('mobile').focus()
                        }
                    })
                },
            },
            mounted() {
                this.fetchUsers()
                document.getElementById('mobile').focus()
            }
        })
    </script>

    <script>
        $("#side_user").addClass("menu-open")
        $("#side_user_crud").addClass("active")
    </script>
@endsection
@section('style')
    <style>
        .btn-success {
            color: white !important
        }

        .add-label {
            position: absolute;
            margin-right: 10px;
            top: 83px;
            background-color: #f4f6f9;
            color: #9f9f9f;
        }

        .add-row {
            border: 1px #dedede solid;
            padding: 35px 5px 15px 5px;
            border-radius: 10px
        }

        .col-submit {
            margin-top: 32px
        }

        .pagination {
            margin: auto
        }

        .form-group label {
            font-size: 15px;
        }

        .del {
            font-style: normal;
            color: red;
            font-size: 16px;
        }

        .scrollme {
            overflow-x: auto;
        }
    </style>
@endsection
