@extends('layouts.admin.app')
@section('content')
    <div class="container pb-5" id="content">
        <div class="row">
            <div class="col-12">
                <div class="scrollme">
                    <table class="table table-bordered">
                        <tbody>
                        <tr v-for="item in slidesDemo">
                            <td>
                                <img style="width:200px" :src="'/images/slider/'+item.image">
                            </td>
                            <td>@{{ translateType(item.type) }}</td>
                            <td style="direction: ltr">@{{item.url}}</td>
                            <td>
                                <a @click="deleteSlide(item.id)">
                                    <i class="fa fa-times" style="color: #dc3545;font-size: 19px;">✖</i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row mb-4" v-if="progress">
            <progress :value="percent" max="100" style="width: 100%"></progress>
        </div>
        <div class="row mt-4">
            <div class="col-lg-6">
                <div class="row mt-3">
                    <div class="col-12 pl-lg-5">
                        <input type="text" v-model="url1"
                               placeholder="URL"
                               class="form-control col-12 mb-3"
                               style="direction: ltr">
                    </div>
                    <div class="col-12">
                        <input @change="thumbnailChange1"
                               type="file" name="file1" id="input1" class="inputfile">
                        <label style="font-weight: unset" class="btn btn-info mb-0" for="input1">
                            + اسلاید در حالت دسکتاپ
                        </label>

                        <button class="btn btn-primary mr-2" type="submit" @click="formSubmit1"
                                style="border-bottom: 3px solid #0061ca;">
                            ثبت
                        </button>
                    </div>
                    <div class="col-6 mt-4">
                        <img id="thumbnail1" style="width: 100%;border-radius: 5px">
                    </div>
                </div>
            </div>


            <div class="col-lg-6">
                <div class="row mt-3">
                    <div class="col-12 pl-lg-5">
                        <input type="text" v-model="url2"
                               placeholder="URL"
                               class="form-control col-12 mb-3"
                               style="direction: ltr">
                    </div>
                    <div class="col-12">
                        <input @change="thumbnailChange2"
                               type="file" name="file2" id="input2" class="inputfile">
                        <label style="font-weight: unset" class="btn btn-info mb-0" for="input2">
                            + اسلاید در حالت موبایل
                        </label>

                        <button class="btn btn-primary mr-2" type="submit" @click="formSubmit2"
                                style="border-bottom: 3px solid #0061ca;">
                            ثبت
                        </button>
                    </div>
                    <div class="col-6 mt-4">
                        <img id="thumbnail2" style="width: 100%;border-radius: 5px">
                    </div>
                </div>
            </div>


        </div>

        <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="img-container" style="height: 80vh">
                            <img id="image1" src="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary ml-3" data-dismiss="modal">انصراف</button>
                        <button type="button" class="btn btn-primary" id="crop" @click="cropThumbnail1">برش</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="img-container" style="height: 80vh">
                            <img id="image2" src="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary ml-3" data-dismiss="modal">انصراف</button>
                        <button type="button" class="btn btn-primary" id="crop" @click="cropThumbnail2">برش</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/cropper.min.js"></script>
    <script>
        let vm = new Vue({
            el: '#content',
            data: {
                slides: [],
                slides2: [],
                slidesDemo: [],
                files: [],
                files2: [],
                images: [],
                images2: [],
                progress: false,
                percent: 0,
                formData1: null,
                formData2: null,
                cropper1: null,
                cropper2: null,
                url1: '',
                url2: '',
            },
            methods: {
                thumbnailChange1(e) {
                    var image = document.getElementById('image1')
                    var input = document.getElementById('input1')
                    var files = e.target.files
                    var $modal = $('#modal1')
                    var done = function (url) {
                        input.value = ''
                        image.src = url
                        $modal.modal('show')
                    }

                    if (files && files.length > 0) {
                        done(URL.createObjectURL(files[0]))
                    }
                },
                thumbnailChange2(e) {
                    var image = document.getElementById('image2')
                    var input = document.getElementById('input2')
                    var files = e.target.files
                    var $modal = $('#modal2')
                    var done = function (url) {
                        input.value = ''
                        image.src = url
                        $modal.modal('show')
                    }

                    if (files && files.length > 0) {
                        done(URL.createObjectURL(files[0]));
                    }
                },
                async cropThumbnail1() {
                    var $modal = $('#modal1')
                    var thumbnail = document.getElementById('thumbnail1');
                    var canvas
                    $modal.modal('hide')

                    canvas = this.cropper1.getCroppedCanvas({
                        width: 1500,
                        height: 650,
                    })

                    thumbnail.src = canvas.toDataURL()
                    let finalImg = await this.getCanvasBlob(canvas)
                    this.formData1.append('image', finalImg)
                    window.scrollBy(0, 100)
                },
                async cropThumbnail2() {
                    var $modal = $('#modal2')
                    var thumbnail = document.getElementById('thumbnail2');
                    var canvas
                    $modal.modal('hide')

                    canvas = this.cropper2.getCroppedCanvas({
                        width: 624,
                        height: 532,
                    })

                    thumbnail.src = canvas.toDataURL()
                    let finalImg = await this.getCanvasBlob(canvas)
                    this.formData2.append('image', finalImg)
                    window.scrollBy(0, 100)
                },
                getCanvasBlob(canvas) {
                    return new Promise(function (resolve, reject) {
                        canvas.toBlob(function (blob) {
                            resolve(blob)
                        })
                    })
                },
                translateType(binary) {
                    return (binary === 0) ? 'دسکتاپ' : 'موبایل'
                },
                getSlider() {
                    let _this = this;
                    axios.get(`/admin/slider/fetch/slide`).then(res => {
                        _this.slidesDemo = res.data;
                    });
                },
                deleteSlide(id) {
                    let _this = this;
                    swal.fire({
                        text: "آیا از پاک کردن اطمینان دارید ؟",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'بله',
                        cancelButtonText: 'لغو',
                    }).then((result) => {
                        if (result.value) {
                            axios.get(`/admin/slider/delete/${id}`)
                                .then(() => {
                                    swal.fire(
                                        {
                                            text: "با موفقیت حذف شد !",
                                            icon: "success",
                                            confirmButtonText: 'باشه',
                                        }
                                    );
                                    _this.getSlider();
                                }).catch(() => {
                                swal.fire(
                                    {
                                        text: "درخواست شما انجام نشد !",
                                        type: "error",
                                        confirmButtonText: 'باشه',
                                    }
                                )
                            });
                        }
                    });
                },
                formSubmit1() {
                    if (document.getElementById('thumbnail1').src.length === 0) {
                        swal.fire({
                            text: "تصویر اسلاید را وارد کنید",
                            icon: "warning",
                            confirmButtonText: 'باشه',
                        })
                        return
                    }
                    this.progress = true
                    const config = {
                        headers: {'content-type': 'multipart/form-data'},
                        onUploadProgress: (uploadEvent) => {
                            this.percent = Math.round((uploadEvent.loaded / uploadEvent.total) * 100);
                        }
                    }
                    this.formData1.append('url', this.url1)

                    axios.post('/admin/slider/store1', this.formData1, config).then(() => {
                        swal.fire({
                            text: " با موفقیت ثبت شد !",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                        vm.getSlider()
                        vm.percent = 0
                        vm.progress = false
                        vm.formData1 = null
                        vm.url1 = ''
                        document.getElementById('thumbnail1').src = ''
                    })
                },
                formSubmit2() {
                    if (document.getElementById('thumbnail2').src.length === 0) {
                        swal.fire({
                            text: "تصویر اسلاید را وارد کنید",
                            icon: "warning",
                            confirmButtonText: 'باشه',
                        })
                        return
                    }
                    this.progress = true
                    const config = {
                        headers: {'content-type': 'multipart/form-data'},
                        onUploadProgress: function (uploadEvent) {
                            this.percent = Math.round((uploadEvent.loaded / uploadEvent.total) * 100);
                        }
                    }
                    this.formData2.append('url', this.url2)

                    axios.post('/admin/slider/store2', this.formData2, config).then(() => {
                        swal.fire({
                            text: " با موفقیت ثبت شد !",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                        vm.getSlider()
                        vm.percent = 0
                        vm.progress = false
                        vm.formData1 = null
                        vm.url1 = ''
                        document.getElementById('thumbnail1').src = ''
                    })
                },
            },
            mounted() {
                this.getSlider()

                this.formData1 = new FormData
                this.formData2 = new FormData

                var $modal1 = $('#modal1')
                var image1 = document.getElementById('image1')
                $modal1.on('shown.bs.modal', function () {
                    vm.cropper1 = new Cropper(image1, {
                        viewMode: 2,
                        aspectRatio: 1500 / 650,
                        data: {
                            width: 1500,
                            height: 650,
                        },
                    })
                }).on('hidden.bs.modal', function () {
                    vm.cropper1.destroy()
                    vm.cropper1 = null
                })


                var $modal2 = $('#modal2')
                var image2 = document.getElementById('image2')
                $modal2.on('shown.bs.modal', function () {
                    vm.cropper2 = new Cropper(image2, {
                        viewMode: 2,
                        aspectRatio: 634 / 532,
                        data: {
                            width: 634,
                            height: 532,
                        },
                    })
                }).on('hidden.bs.modal', function () {
                    vm.cropper2.destroy()
                    vm.cropper2 = null
                })
            }
        })
    </script>

    <script>
        $("#side_setting").addClass("menu-open");
        $("#side_slider_create").addClass("active");
    </script>
@endsection

@section('style')
    <link rel="stylesheet" href="/css/cropper.min.css">
    <style>
        .modal{
            z-index: 9999999999 !important;
        }
        i {
            font-style: unset;
        }

        input[type=file] {
            padding: 3px 5px;
        }

        .scrollme {
            overflow-x: auto;
        }

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile:focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }

        .inputfile + label * {
            pointer-events: none;
        }

        .modal-dialog {
            width: 100%;
            height: 100vh !important;
            margin: 0;
            padding: 0;
            max-width: 100% !important;
        }

        .modal-content {
            width: 100%;
            height: 100vh !important;
            min-height: 100%;
            border-radius: 0;
        }

        .modal-body {
            height: 100vh !important;
        }

        #modal {
            z-index: 99999999;
            padding-right: unset !important;
        }
    </style>
@endsection

