@extends('layouts.admin.app')
@section('content')
    <div class="container mt-5 pb-5 relative" id="content">
        <div class="row">
            <div class="col-lg-2 pt-2 px-lg-0">
                تعداد محصول کمتر از
            </div>
            <div class="col-lg-1 text-lg-center px-lg-0">
                <input type="text" class="form-control text-center" @keyup.enter="formSubmit"
                       v-model="number">
            </div>
            <div class="col-lg-2 pt-2 text-lg-center px-lg-0 mt-4 mt-lg-0">
                ، هزینه ارسال
            </div>
            <div class="col-lg-1 text-center px-lg-0">
                <input type="text" class="form-control text-center" @keyup.enter="formSubmit"
                       v-model="minPrice">
            </div>
            <div class="col-lg-2 pt-2 text-lg-center px-lg-0 mt-4 mt-lg-0">
                ، در غیر اینصورت
            </div>
            <div class="col-lg-1 px-lg-0">
                <input type="text" class="form-control text-center" @keyup.enter="formSubmit"
                       v-model="maxPrice">
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-1 mt-5 mt-lg-0">
                <button style="border-bottom: solid #0055b1 3px;padding: 4px 22px;"
                        type="button" class="btn btn-primary w-100" @click="formSubmit">
                    ثبت
                </button>
            </div>

        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#content',
            data: {
                number: null,
                minPrice: null,
                maxPrice: null,
            },
            methods: {
                fetch() {
                    let vm = this
                    axios.get('/admin/setting/delivery/fetch').then(res => {
                        vm.number = res.data.number
                        vm.minPrice = res.data.min
                        vm.maxPrice = res.data.max
                    })
                },
                formSubmit() {
                    if (this.number == null || this.minPrice == null || this.maxPrice == null) {
                        swal.fire({
                            text: "مقادیر را وارد کنید.",
                            icon: "warning",
                            confirmButtonText: 'باشه',
                        })
                        return
                    }
                    let vm = this
                    axios.post('/admin/setting/delivery/store', {
                        number: this.number,
                        min: this.minPrice,
                        max: this.maxPrice,
                    }).then(() => {
                        swal.fire({
                            text: "با موفقیت ثبت شد.",
                            icon: "success",
                            confirmButtonText: 'باشه',
                        })
                        vm.fetch()
                    })
                },
            },
            mounted() {
                this.fetch()
            }
        })
    </script>
    <script>
        $("#side_setting").addClass("menu-open")
        $("#side_delivery_update").addClass("active")
    </script>
@endsection
@section('style')

@endsection
