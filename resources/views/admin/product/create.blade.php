@extends('layouts.admin.app')
@section('content')
  <div class="container mt-0 mt-lg-4" id="content">
    <div class="row mb-4 px-2 px-lg-5" v-if="progress">
      <progress :value="percent" max="100" style="width: 100%"></progress>
    </div>
    <div class="row px-lg-3">
      <div class="col-12 p-0">
        <div class="card">
          <div class="card-header" style="padding-bottom: 0;background-color: #343a40;border-bottom: 0px;padding-left: 0;">
            <ul class="nav nav-pills yy relative d-inline-flex" style="padding-bottom:10px">
              <li class="nav-item ml-3 mb-2 mb-lg-0">
                <a class="nav-link active" href="#public" data-toggle="tab">عمومی</a>
              </li>
              <li class="nav-item ml-3 mb-2 mb-lg-0">
                <a class="nav-link" href="#short-desc" data-toggle="tab">
                  توضیحات کوتاه
                </a>
              </li>
              <li class="nav-item ml-3 mb-2 mb-lg-0">
                <a class="nav-link" href="#gallery" data-toggle="tab">
                  تصاویر</a>
              </li>
              <li class="nav-item ml-3 mb-2 mb-lg-0">
                <a class="nav-link" href="#specification" data-toggle="tab">
                  مشخصات محصول</a>
              </li>
              <li class="nav-item ml-3 mb-2 mb-lg-0">
                <a class="nav-link" href="#coloring" data-toggle="tab">
                  رنگ و سایز </a>
              </li>
              <li class="nav-item ml-3 mb-2 mb-lg-0">
                <a class="nav-link" href="#tbl-size" data-toggle="tab">
                  جدول سایزبندی</a>
              </li>
              <li class="nav-item ml-3 mb-2 mb-lg-0 d-lg-none">
                <button type="button" @click="formValidate" class="submit-btn">
                  ثبت
                </button>
              </li>
            </ul>
            <button type="button" @click="formValidate" class="submit-btn float-left ml-3 d-none d-lg-block">
              ثبت
            </button>
          </div>

          <div class="tab-content p-lg-3">
            <div class="active tab-pane" id="public">
              <div class="container pb-3">
                <div class="row mt-3">
                  <div class="col-md-3 mb-lg-0 mb-3">
                    <div class="form-group mt-2 mt-lg-0 mb-4 mb-lg-0">
                      <label>نوع محصول</label>
                      <br>
                      <div class="text-right">
                        <button @click="flag=!flag" id="btn-cat">
                          @{{ form.catName }}
                          <img id="angle-down" src="/assets/images/arrow-down.svg">
                        </button>

                        <div v-if="flag" id="combo-box">
                          <ul v-if="flag1">
                            <li id="root-cat" v-for="root in roots" @click="fetchChild(root.id,root.name)">
                              @{{ root.name }}
                              <img class="angle-left" src="/assets/images/left-arrow3.svg">
                            </li>
                          </ul>

                          <ul v-if="flag2">
                            <li id="back" @click="back">
                              <img src="/assets/images/right-arrow.svg" class="angle-right">
                              <span style="margin-right: 3px;padding-top: 3px;display: inline-block;">
                                @{{ holder.parentName }}
                              </span>
                            </li>
                            <li id="fix" @click="fixCat" v-if="!children.length">@{{ holder.selfName }}
                            </li>
                            <li class="children-item" v-for="child in children"
                              @click="fetchChild(child.id,child.name,child.children_recursive.length)">
                              @{{ child.name }}
                              <img v-if="child.children_recursive.length" class="angle-left"
                                src="/assets/images/left-arrow3.svg">
                            </li>
                          </ul>

                        </div>
                      </div>
                    </div>
                    <div class="d-lg-none">
                      <template v-for="(cat,index) in breadcrumbs">
                        <div class="mb-2 pr-2" style="display: inline-block">
                          <div class="d-flex flex-row-reverse">
                            <i @click="deleteBreadcrumb(index,cat.id)" class="remove-breadcrumb">✖</i>
                            <span>@{{ cat?.name }}</span>
                            <img v-if="cat?.parent_recursive[0]?.name" src="/assets/images/left-arrow3.svg"
                              class="angle-left mx-2 mt-0">
                            <span>
                              @{{ cat?.parent_recursive[0]?.name }}
                            </span>
                            <img v-if="cat?.parent_recursive[0]?.parent_recursive[0]?.name"
                              src="/assets/images/left-arrow3.svg" class="angle-left mx-2 mt-0">
                            <span>
                              @{{ cat?.parent_recursive[0]?.parent_recursive[0]?.name }}
                            </span>
                          </div>
                        </div>
                        <br>
                      </template>
                    </div>
                  </div>
                  <div class="col-md-3 mb-3 mb-lg-0">
                    <div class="form-group mb-4 mb-lg-0">
                      <label for="brand">برند</label>
                      <button @click="clickBrandBtn" id="btn-cat">
                        @{{ form.brand }}
                        <img id="angle-down" src="/assets/images/arrow-down.svg">
                      </button>
                      <div v-show="flagBrand" id="sss">
                        <input type="text" v-model="brandSearch" id="input-search" @keydown.enter="searchBrand"
                          @keyup="checkSearchBrand" placeholder="جستجو...">
                        <ul v-show="flagBrand1">
                          <li style="line-height: 35px" @click="selectBrand('ندارد')">
                            <span>ندارد</span>
                          </li>
                          <li style="line-height: 35px;" @click="selectBrand(brand.name_f)" v-for="brand in brands"
                            class="all-brand" :id="'brand-id' + brand.id">
                            <span>@{{ brand.name_f }}</span>
                          </li>
                          <li v-show="!brands.length">
                            <span style="color: red">موردی یافت نشد</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 mb-3 mb-lg-0">
                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 0">
                      <label class="col-md-4 control-label">نام محصول</label>
                      <input type="text" class="form-control" v-model="form.name">
                    </div>
                  </div>
                </div>
                <div class="row mt-lg-5">
                  <div class="col-md-3 mb-3 mb-lg-0">
                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 0">
                      <label class="col-md-4 control-label" style="max-width: 100%;">قیمت</label>
                      <input type="text" class="form-control" @keyup="changePrice" v-model="fmt_price">
                    </div>
                  </div>

                  <div class="col-lg-3 mb-3 mb-lg-0">
                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 0">
                      <label class="col-md-4 control-label" style="max-width: 100%;">قیمت پس
                        از تخفیف
                      </label>
                      <input @keyup="calDiscount" type="text" class="form-control" v-model="fmt_afterDiscount">
                    </div>
                  </div>

                  <div class="col-lg-3 mb-3 mb-lg-0">
                    <div class="form-group mb-4 mb-lg-0" style="margin-bottom: 4px">
                      <label class="col-md-4 control-label">تخفیف</label>
                      <input @keyup="calDiscountPrice" type="text" class="form-control" v-model="form.discount">
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="form-group" style="margin-bottom: 0">
                      <label for="exampleFormControlSelect3">وضعیت</label>
                      <select class="form-control" id="exampleFormControlSelect3" v-model="form.status">
                        <option value="" disabled hidden>انتخاب کنید...</option>
                        <option value="1">نمایش در سایت</option>
                        <option value="0">عدم نمایش در سایت</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row mt-lg-4">
                  <div class="col-lg-7 mt-4 mt-lg-2 pr-3 d-none d-lg-block">
                    <template v-for="(cat,index) in breadcrumbs">
                      <div class="mb-2" style="display: inline-block">
                        <div class="d-flex flex-row-reverse">
                          <i @click="deleteBreadcrumb(index,cat.id)" class="remove-breadcrumb">✖</i>
                          <span>@{{ cat?.name }}</span>
                          <img v-if="cat?.parent_recursive[0]?.name" src="/assets/images/left-arrow3.svg"
                            class="angle-left mx-2 mt-0">
                          <span>
                            @{{ cat?.parent_recursive[0]?.name }}
                          </span>
                          <img v-if="cat?.parent_recursive[0]?.parent_recursive[0]?.name"
                            src="/assets/images/left-arrow3.svg" class="angle-left mx-2 mt-0">
                          <span>
                            @{{ cat?.parent_recursive[0]?.parent_recursive[0]?.name }}
                          </span>
                        </div>
                      </div>
                      <br>
                    </template>
                  </div>
                  <div class="col-lg-3 mt-4 px-5 pt-2 px-lg-0 pt-lg-0">
                    <img id="thumbnail" style="width: 100%;border-radius: 8px">
                  </div>
                  <div class="col-lg-2 mt-4 text-left">
                    <input @change="thumbnailChange" type="file" name="file" id="input" class="inputfile">
                    <label style="font-weight: unset" class="thumbnail-img" for="input">تصویر شاخص</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="short-desc">
              <div class="row mb-4">
                <div class="col-12 p-0">
                  <div class="form-group">
                    <textarea name="short_desc"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="gallery">
              <div class="row mt-3 pb-3 px-2">
                <div class="col-12 text-center text-lg-right">
                  <input @change="galleryChange" type="file" name="file" id="input2" class="inputfile">
                  <label class="btn btn-primary" for="input2"
                    style="font-weight: unset;border-bottom: 3px solid #2269a3;">
                    + افزودن تصویر گالری
                  </label>
                </div>

                <div v-show="galleries.length" class="col-4 col-lg-2 mt-4 p-0 mx-2" v-for="(image, index) in galleries"
                  :key="index" style="position: relative">
                  <a @click="deleteImage(index)">
                    <i class="fa fa-times del">✖</i>
                  </a>
                  <img :src="image" style="width: 100%;border-radius: 8px">
                </div>


              </div>
            </div>
            <div class="tab-pane" id="specification">
              <div class="row mt-3">
                <div class="col-12 text-center text-lg-right">
                  <div class="form-group">

                    <button class="btn btn-info mb-4" @click.stop.prevent="addRowSpecification()">
                      <i style="font-weight: bold">+</i> افزودن مشخصه
                    </button>

                    <ul class="container-fluid" style="list-style:none;">
                      <li class="row" v-for="(specification, index) in specifications">
                        <input type="text" v-model="specification.key" placeholder="عنوان"
                          class="form-control col-5 col-lg-3 my-2 ml-lg-2" style="display: inline-block">
                        <input type="text" v-model="specification.value" placeholder="مقدار"
                          class="form-control col-6 col-lg-5 my-2 ml-lg-2" style="display: inline-block">
                        <a class="col-1 col-lg-3 p-0" @click="deleteRowSpecification(index)" style="font-size: 20px;">
                          <i class="mr-lg-2 d-inline-block pt-3"
                            style="color: #dc3545;font-weight: bold;font-size: 16px;">✖</i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="coloring">
              <div class="row mt-3 px-3">
                <div class="col-lg-6">
                  <div class="form-group">
                    <button class="btn btn-info mb-3" @click.stop.prevent="addRow()">
                      <i style="font-weight: bold">+</i> افزودن رنگ
                    </button>
                    <ul style="list-style:none;" class="container-fluid">
                      <li class="row" v-for="(color, index) in colors">
                        <input type="text" v-model="color.name" placeholder="نام رنگ"
                          class="form-control col-8 col-lg-4 my-2 ml-2" style="display: inline-block">
                        <input type="color" v-model="color.code" class="form-control col-2 col-lg-1 ml-2 mt-2"
                          style="height: 39px;display: inline-block;width: 40px;padding: 0;vertical-align: middle;">
                        <a class="col-1" @click="deleteRow(index)" style="font-size: 20px;">
                          <i class="mr-2 d-inline-block pt-3"
                            style="color: #dc3545;font-weight: bold;font-size: 16px;">✖</i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <button class="btn btn-info mb-3" @click.stop.prevent="addRowSize()">
                      <i style="font-weight: bold">+</i> افزودن سایز
                    </button>
                    <ul style="list-style:none;">
                      <li v-for="(size, index) in sizes">
                        <input type="text" v-model="size.name" placeholder="عنوان"
                          class="form-control col-10 col-lg-4 my-2 ml-2" style="display: inline-block">

                        <a @click="deleteRowSize(index)" style="font-size: 20px;">
                          <i class="mr-2" style="color: #dc3545;font-weight: bold;font-size: 16px;">✖</i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tbl-size">
              <div class="row mt-0 mt-lg-3">
                <div class="col-12">
                  <div class="form-group">
                    <textarea type="text" name="tbl_size"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="img-container" style="height: 80vh">
              <img id="image" src="">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary ml-3" data-dismiss="modal">انصراف</button>
            <button type="button" class="btn btn-primary" id="crop" @click="cropThumbnail">برش</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="img-container" style="height: 80vh">
              <img id="image2" src="">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary ml-3" data-dismiss="modal">انصراف</button>
            <button type="button" class="btn btn-primary" id="crop" @click="cropGallery">برش</button>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
  <script src="/js/cropper.min.js"></script>
  <script>
    let vm = new Vue({
      el: '#content',
      data: {
        form: {
          name: '',
          discount: 0,
          price: null,
          image: null,
          brand: 'انتخاب کنید...',
          catName: 'انتخاب کنید...',
          cats: [],
          status: '',
          afterDiscount: null,
          galleries: []
        },
        cats: [],
        specifications: [],
        sizes: [],
        colors: [],
        brands: [],
        isDragging: false,
        dragCount: 0,
        files: [],
        images: [],
        effect_prices: [],
        color_images: [],
        percent: 0,
        progress: false,
        holder2: '',
        // tree
        categories: [],
        output: '',
        flag: false,
        flag1: true,
        flag2: false,
        roots: [],
        children: [],
        flagBrand: false,
        brandSearch: '',
        flagBrand1: true,
        cropper: null,
        cropper2: null,
        galleries: [],
        formData: null,
        holder: {
          selfName: 'ریشه',
          selfId: '',
          parentName: '',
          parentId: '',
          grandName: '',
          grandId: '',
        },
        parents: '',
        parentName: [],
        breadcrumbs: [],
      },
      computed: {
        fmt_price: {
          get() {
            if (this.form.price) {
              return this.commafy(this.form.price); //.toLocaleString();
            } else
              return "";
          },
          set(newValue) {
            this.form.price = parseFloat(newValue.replace(/,/g, ""));
          }
        },
        fmt_afterDiscount: {
          get() {
            if (this.form.afterDiscount) {
              return this.commafy(this.form.afterDiscount); //.toLocaleString();
            } else
              return "";
          },
          set(newValue) {
            this.form.afterDiscount = parseFloat(newValue.replace(/,/g, ""));
          }
        }
      },
      watch: {
        cat: {
          immediate: true,
          handler(val, oldVal) {
            if (this.cat) {
              this.form.catName = this.cat.name
              this.holder.selfId = this.cat.id
            }
          }
        }
      },
      methods: {
        deleteBreadcrumb(index, id) {
          this.breadcrumbs.splice(index, 1)
          this.form.cats = this.form.cats.filter(item => (item !== id))
        },
        galleryChange(e) {
          var image = document.getElementById('image2')
          var input = document.getElementById('input2')
          var files = e.target.files
          var $modal = $('#modal2')
          var done = function(url) {
            input.value = ''
            image.src = url
            $modal.modal('show')
          }

          if (files && files.length > 0) {
            done(URL.createObjectURL(files[0]));
          }
        },
        thumbnailChange(e) {
          var image = document.getElementById('image')
          var input = document.getElementById('input')
          var files = e.target.files
          var $modal = $('#modal')
          var done = function(url) {
            input.value = ''
            image.src = url
            $modal.modal('show')
          }

          if (files && files.length > 0) {
            done(URL.createObjectURL(files[0]));
          }
        },
        getCanvasBlob(canvas) {
          return new Promise(function(resolve, reject) {
            canvas.toBlob(function(blob) {
              resolve(blob)
            })
          })
        },
        async cropGallery() {
          var $modal = $('#modal2')
          var canvas
          $modal.modal('hide')

          canvas = this.cropper2.getCroppedCanvas({
            width: 600,
            height: 700,
          })

          let finalImg = await this.getCanvasBlob(canvas)
          this.galleries.push(canvas.toDataURL())
          this.form.galleries.push(finalImg)

        },
        async cropThumbnail() {
          var $modal = $('#modal')
          var thumbnail = document.getElementById('thumbnail');
          var canvas
          $modal.modal('hide')

          canvas = this.cropper.getCroppedCanvas({
            width: 600,
            height: 700,
          })

          thumbnail.src = canvas.toDataURL()
          let finalImg = await this.getCanvasBlob(canvas)
          this.formData.append('image', finalImg)
        },
        changePrice() {
          this.form.afterDiscount = null
          this.form.discount = 0
        },
        commafy(num, prec, currSign) {
          if (prec == null) prec = 0;
          var str = parseFloat(num).toFixed(prec).toString().split('.');
          if (str[0].length >= 4) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
          }
          if (str[1] && str[1].length >= 4) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
          }
          return (currSign == null ? "" : currSign + " ") + str.join('.');
        },
        calDiscount() {
          if (!this.form.price) {
            swal.fire({
              text: 'لطفا ابتدا قیمت را وارد کنید .',
              icon: 'warning',
              confirmButtonText: 'باشه',
            })
            this.form.afterDiscount = ''
          } else {
            let per = this.form.price / 100
            let x = this.form.afterDiscount / per
            this.form.discount = 100 - x
          }
        },
        calDiscountPrice() {
          if (this.form.price === null) {
            swal.fire({
              text: 'لطفا ابتدا قیمت پایه را وارد کنید .',
              icon: 'warning',
              confirmButtonText: 'باشه',
            })
            this.form.discount = 0
          } else {
            let per = this.form.price / 100
            let x = 100 - this.form.discount
            let y = x * per


            this.form.afterDiscount = Math.round(y)
          }
        },
        OnDragEnter(e) {
          e.preventDefault();
          this.dragCount++;
          this.isDragging = true;
          return false;
        },
        OnDragLeave(e) {
          e.preventDefault();
          this.dragCount--;
          if (this.dragCount <= 0)
            this.isDragging = false;
        },
        onInputChange(e) {
          const files = e.target.files;
          Array.from(files).forEach(file => this.addImage(file));
        },
        onDrop(e) {
          e.preventDefault();
          e.stopPropagation();
          this.isDragging = false;
          const files = e.dataTransfer.files;
          Array.from(files).forEach(file => this.addImage(file));
        },
        addImage(file) {
          if (!file.type.match('image.*')) {
            this.$toastr.e(`${file.name} is not an image`);
            return;
          }
          this.files.push(file);
          const reader = new FileReader();
          reader.onload = (e) => this.images.push(e.target.result);
          reader.readAsDataURL(file);
        },
        addImage2(file) {
          this.color_images.push(file);
        },
        deleteImage(index) {
          this.galleries.splice(index, 1)
          this.form.galleries.splice(index, 1)
        },
        getFileSize(size) {
          const fSExt = ['Bytes', 'KB', 'MB', 'GB'];
          let i = 0;

          while (size > 900) {
            size /= 1024;
            i++;
          }
          return `${(Math.round(size * 100) / 100)} ${fSExt[i]}`;
        },
        addRow() {
          this.colors.push({});
        },
        addRowSpecification() {
          this.specifications.push({})
        },
        addRowSize() {
          this.sizes.push({})
        },
        deleteRow(index) {
          this.colors.splice(index, 1)
        },
        deleteRowSpecification(index) {
          this.specifications.splice(index, 1)
        },
        deleteRowSize(index) {
          this.sizes.splice(index, 1)
        },
        onImageChange(e) {
          this.form.image = e.target.files[0];
        },
        fetchBrands() {
          let data = this;
          axios.get(`/admin/product/fetch/brand/all`).then(res => {
            data.brands = res.data
          });
        },
        clickBrandBtn() {
          if (this.flagBrand === false) {
            this.flagBrand = true;
          } else if (this.flagBrand === true) {
            this.flagBrand = false
          }
        },
        searchBrand() {
          let vm = this
          if (this.brandSearch === '') {

          } else {
            axios.get(`/admin/product/search/brand/${this.brandSearch}`).then(res => {
              vm.brands = res.data
            })
          }
        },
        checkSearchBrand() {
          let vm = this
          if (this.brandSearch === '') {
            this.fetchBrands()
          }
        },
        selectBrand(name) {
          this.flagBrand = false
          this.form.brand = name
        },
        // tree
        clickCatBtn() {
          if (this.flag === false) {
            this.flag = true;
          } else if (this.flag === true) {
            this.flag = false
          }
        },
        formValidate() {
          if (!this.form.cats.length) {
            swal.fire({
              icon: 'warning',
              text: 'نوع محصول را انتخاب کنید.',
              confirmButtonText: 'باشه',
            })
            return
          }
          if (this.form.brand === 'انتخاب کنید...') {
            swal.fire({
              text: 'برند را انتخاب کنید.',
              icon: 'warning',
              confirmButtonText: 'باشه',
            })
            return
          }
          if (this.form.name === '') {
            swal.fire({
              text: 'نام محصول را وارد کنید.',
              icon: 'warning',
              confirmButtonText: 'باشه',
            })
            return
          }
          if (this.form.price === null) {
            swal.fire({
              text: 'قیمت را وارد کنید.',
              icon: 'warning',
              confirmButtonText: 'باشه',
            })
            return
          }
          if (this.formData.get('image') === null) {
            swal.fire({
              text: 'تصویر شاخص را وارد کنید.',
              icon: 'warning',
              confirmButtonText: 'باشه',
            })
            return
          }
          this.formSubmit()
        },
        formSubmit() {
          this.progress = true
          let config = {
            headers: {
              'content-type': 'multipart/form-data'
            },
            onUploadProgress: (e) => {
              this.percent = Math.round((e.loaded / e.total) * 100)
            }
          }

          let colors = JSON.stringify(this.colors)
          let specifications = JSON.stringify(this.specifications)
          let sizes = JSON.stringify(this.sizes)


          let shortDesc = CKEDITOR.instances["short_desc"].getData()
          let tblSize = CKEDITOR.instances["tbl_size"].getData()

          this.form.galleries.forEach(gallery => {
            this.formData.append('pics[]', gallery)
          })

          this.form.cats.forEach(cat => {
            this.formData.append('cats[]', cat)
          })

          this.formData.append('name', this.form.name)
          this.formData.append('brand', this.form.brand)
          this.formData.append('price', this.form.price)
          this.formData.append('status', this.form.status)
          this.formData.append('discount', this.form.discount)
          this.formData.append('colors', colors)
          this.formData.append('specifications', specifications)
          this.formData.append('short_desc', shortDesc)
          this.formData.append('tbl_size', tblSize)
          this.formData.append('sizes', sizes)

          axios.post('/admin/product/store', this.formData, config).then(() => {
            window.location.href = `/admin/product/index`
          })
        },
        fetchCategories() {
          let _this = this
          axios.get(`/admin/category/fetch`).then(res => {
            _this.categories = res.data
            _this.roots = res.data.filter(cat => cat.parent === null)
          })
        },
        fixRoot() {
          this.form.catName = 'ریشه'
          this.flag = false
          this.$emit('fixCat', 'ریشه')
        },
        fetchChild(id, name, length) {
          this.flag1 = false
          this.flag2 = true

          this.holder.grandName = this.holder.parentName
          this.holder.grandId = this.holder.parentId

          this.holder.parentName = this.holder.selfName
          this.holder.parentId = this.holder.selfId

          this.holder.selfName = name
          this.holder.selfId = id

          if (length === 0) {
            this.fixCat()
            this.holder.selfName = 'ریشه'
            // this.holder.selfId = ''
            this.holder.parentName = ''
            this.holder.parentId = ''
            this.holder.grandName = ''
            this.holder.grandId = ''
            this.flag1 = true
            this.flag2 = false
            this.fetchCategories()
            return
          }

          this.children = this.categories.filter(cat => cat.parent === id)
        },
        fixCat() {
          // this.form.catName = this.holder.selfName
          this.form.cats.push(this.holder.selfId)
          this.flag = false
          this.fetchBreadcrumb()

        },
        fetchBreadcrumb() {
          axios.post('/admin/product/fetch/breadcrumb', {
            catIds: this.form.cats
          }).then(res => {
            vm.breadcrumbs = res.data
          })
        },
        back() {
          if (this.holder.parentName === 'ریشه') {
            this.flag1 = true
            this.flag2 = false
            this.holder.selfName = 'ریشه'
            this.holder.selfId = ''
            this.holder.parentName = ''
            this.holder.parentId = ''
            this.holder.grandName = ''
            this.holder.grandId = ''
          } else {
            this.children = this.categories.filter(cat => cat.parent === this.holder.parentId)

            this.holder.selfName = this.holder.parentName
            this.holder.selfId = this.holder.parentId

            this.holder.parentName = this.holder.grandName
            this.holder.parentId = this.holder.grandId

            let x = this.categories.filter(cat => cat.id === this.holder.grandId)
            if (x[0]) {
              let y = this.categories.filter(cat => cat.id === x[0].parent)
              if (y[0]) {
                this.holder.grandName = y[0].name
                this.holder.grandId = y[0].id
              } else {
                this.holder.grandName = 'ریشه'
                this.holder.grandId = ''
              }
            }
          }
        },
      },
      mounted() {
        this.fetchCategories()
        this.fetchBrands()

        this.formData = new FormData
        var $modal = $('#modal')
        var image = document.getElementById('image')
        $modal.on('shown.bs.modal', function() {
          vm.cropper = new Cropper(image, {
            viewMode: 2,
            aspectRatio: 600 / 700,
            data: {
              width: 600,
              height: 700,
            },
          })
        }).on('hidden.bs.modal', function() {
          vm.cropper.destroy()
          vm.cropper = null
        })


        var $modal2 = $('#modal2')
        var image2 = document.getElementById('image2');
        $modal2.on('shown.bs.modal', function() {
          vm.cropper2 = new Cropper(image2, {
            viewMode: 2,
            aspectRatio: 600 / 700,
            data: {
              width: 600,
              height: 700,
            },
          })
        }).on('hidden.bs.modal', function() {
          vm.cropper2.destroy()
          vm.cropper2 = null
        })
      },
      updated() {
        if (!this.form.afterDiscount) this.form.discount = 0
      }
    })
  </script>
  <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>

  <script>
    CKEDITOR.replace('tbl_size', {
      customConfig: "{{ asset('/js/ckeditor/config.js') }}",
      height: 500
    })
    CKEDITOR.replace('short_desc', {
      customConfig: "{{ asset('/js/ckeditor/config.js') }}",
      height: 500,
    })
  </script>
  <script>
    $("#side_product").addClass("menu-open");
    $("#side_product_add").addClass("active");
  </script>
@endsection

@section('style')
  <link rel="stylesheet" href="/css/cropper.min.css">
  <style>
    .remove-breadcrumb {
      color: #dc3545;
      font-weight: bold;
      font-size: 14px;
      cursor: pointer;
      margin-right: 15px;
      margin-top: 5px;
    }


    .del {
      color: rgb(220, 53, 69);
      position: absolute !important;
      left: 3px !important;
      bottom: 4px !important;
      font-size: 23px !important;
    }

    .uploader {
      width: 100%;
      background: #ffffff;
      color: #a8a8a8;
      padding: 40px 15px;
      text-align: center;
      border-radius: 15px;
      border: 3px dashed #a8a8a8;
      font-size: 20px;
      position: relative;
    }

    .uploader.dragging {
      background: #fff;
      color: #cecece;
      border: 3px dashed #cecece;
    }

    .uploader.dragging .file-input label {
      background: #a8a8a8;
      color: #545454;
    }

    .uploader i {
      font-size: 85px;
    }

    .uploader .file-input {
      width: 200px;
      margin: auto;
      height: 68px;
      position: relative;
    }

    .uploader .file-input label,
    .uploader .file-input input {
      background: #a8a8a8;
      color: #ffffff;
      width: 100%;
      position: absolute;
      left: 0;
      top: 0;
      padding: 10px;
      border-radius: 4px;
      margin-top: 7px;
      cursor: pointer;
    }

    .uploader .file-input input {
      opacity: 0;
      z-index: -2;
    }

    .uploader .images-preview {
      display: flex;
      flex-wrap: wrap;
      margin-top: 20px;
    }

    .uploader .images-preview .img-wrapper {
      width: 160px;
      display: flex;
      flex-direction: column;
      margin: 10px;
      justify-content: space-between;
      background: #fff;
      box-shadow: 5px 5px 20px #3e3737;
    }

    .uploader .images-preview .details {
      font-size: 12px;
      background: #fff;
      color: #000;
      display: flex;
      flex-direction: column;
      align-items: self-start;
      padding: 3px 6px;
    }

    .uploader .images-preview .details .name {
      overflow: hidden;
      height: 18px;
    }

    .uploader .upload-control {
      position: absolute;
      width: 100%;
      background: #fff;
      top: 0;
      left: 0;
      border-top-left-radius: 7px;
      border-top-right-radius: 7px;
      padding: 10px;
      padding-bottom: 4px;
      text-align: right;
    }

    .uploader .upload-control button,
    .uploader .upload-control label {
      background: #cecece;
      border: 2px solid #cecece;
      border-radius: 3px;
      color: #fff;
      font-size: 15px;
      cursor: pointer;
    }

    .uploader .upload-control label {
      padding: 2px 5px;
      margin-right: 10px;
    }

    .nav-pills .nav-link:not(.active):hover {
      color: #ffffff !important;
    }
  </style>
  <style>
    li {
      list-style: none
    }

    #btn-cat {
      position: relative;
      background-color: white;
      border: 1px solid #ced4da;
      width: 100%;
      padding: 4px 8px 4px 8px;
      border-radius: 5px;
      text-align: right;
      color: #484848;
    }

    #btn-cat:focus {
      outline: unset;
      color: #495057;
      background-color: #fff;
      border-color: #80bdff;
      box-shadow: inset 0 0 0 transparent, 0 0 0 0.2rem rgb(0 123 255 / 25%);
    }

    #sss {
      position: absolute;
      list-style: none;
      top: 70px;
      z-index: 99;
      background-color: white;
      padding: 5px 15px;
      width: 100%;
      line-height: 35px;
      border: 1px solid #a1a1a1;
      max-height: 330px;
      overflow: scroll;
      overflow-x: hidden;
    }

    #sss li {
      cursor: pointer
    }

    .left {
      float: right;
      font-weight: bold;
      font-size: 22px;
      margin: 3px -2px 0px 5px;
      font-style: normal;
    }
  </style>
  <style>
    #input-search {
      display: block;
      border-bottom: 1px solid #afafaf;
      border-left: unset;
      border-right: unset;
      border-top: unset;
      width: 100%;
      margin-bottom: 10px;
    }

    .brand-select {
      line-height: 35px;
      padding: 0px 10px;
      border-radius: 5px;
      background-color: #358fdc;
      color: white;
      margin-top: 2px;
      margin-bottom: 2px;
      transition: all 0.3s;
    }

    .author-select {
      line-height: 35px;
      padding: 0px 10px;
      border-radius: 5px;
      background-color: #358fdc;
      color: white;
      margin-top: 2px;
      margin-bottom: 2px;
      transition: all 0.3s;
    }

    #categories {
      cursor: pointer;
      user-select: none
    }

    #categories>li {
      position: relative;
    }

    #categories>li ul>li {
      position: relative;
    }

    #area {
      text-align: right
    }

    #price {
      text-decoration-line: line-through;
      display: inline-block;
      float: left;
      font-size: 14px;
      margin-right: 12px;
    }

    .price {
      display: inline-block;
      float: right;
    }

    .pro_name {
      line-height: 28px;
    }

    .nav-link {
      padding: 5px 10px !important;
    }

    .dropdown-menu-right,
    .dropdown-menu {
      text-align: right
    }

    .thumbnail-img {
      font-size: 15px;
      font-weight: lighter !important;
      padding: 5px 18px;
      color: #ffffff;
      border-radius: 5px;
      vertical-align: top;
      cursor: pointer;
      background: #dc3545;
      border-bottom: 3px solid #ac1f2d;
    }

    .thumbnail-hint {
      position: absolute;
      top: 40px;
      left: 17px;
      color: #808080;
      font-size: 14px;
    }

    .btn-info {
      border-bottom: 3px solid #0c7384;
    }

    .submit-btn {
      background: rgb(9, 144, 32);
      border: unset;
      color: white;
      width: 70px;
      height: 31px;
      border-radius: 50px;
      cursor: pointer;
      font-size: 15px;
    }

    .nav-item {
      font-size: 15px;
    }

    .nav-item .active {
      border-radius: 50px !important;
    }

    .form-group label {
      font-size: 15px;
    }

    .nav-pills .nav-link.active {
      background-color: #ffffff !important;
      color: #4e4e4e !important;
      font-weight: bold;
    }

    #side_product .active {
      color: white !important;
    }
  </style>
  <style>
    .inputfile {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
    }

    .inputfile:focus+label {
      /* keyboard navigation */
      outline: 1px dotted #000;
      outline: -webkit-focus-ring-color auto 5px;
    }

    .inputfile+label * {
      pointer-events: none;
    }

    #modal2 {
      z-index: 99999999;
    }

    .modal-dialog {
      width: 100%;
      height: 100vh !important;
      margin: 0;
      padding: 0;
      max-width: 100% !important;
    }

    .modal-content {
      width: 100%;
      height: 100vh !important;
      min-height: 100%;
      border-radius: 0;
    }

    .modal-body {
      height: 100vh !important;
    }

    #modal {
      z-index: 99999999;
    }

    @media (min-width: 576px) {
      .container {
        max-width: unset !important;
      }
    }

    #cke_1_top {
      padding: 4px 0
    }
  </style>

  <style>
    .children-item {
      margin-right: 10px;
    }

    #fix {
      color: black;
      font-weight: bold;
      background-color: #f1f1f1;
      padding-right: 6px;
      border-radius: 6px
    }

    .right {
      float: right;
      margin: 4px 0px 0px 5px;
    }

    #back {
      color: #a0a0a0
    }

    #root-cat {
      line-height: 35px;
    }

    #root {
      color: black;
      font-weight: bold;
      background-color: #f1f1f1;
      padding-right: 6px;
      border-radius: 6px
    }

    li {
      list-style: none
    }

    #btn-cat {
      position: relative;
      background-color: white;
      border: 1px solid #ced4da;
      width: 100%;
      border-radius: 5px;
      text-align: right;
      color: #484848;
      height: 41px;
      overflow: hidden;
    }

    #angle-down {
      float: left;
      width: 11px;
      margin: 5px 2px 0px 5px;
    }

    #combo-box {
      position: absolute;
      list-style: none;
      z-index: 99;
      background-color: white;
      padding: 5px 10px;
      width: 97%;
      line-height: 35px;
      border: 1px solid #c8c8c8;
      overflow-x: hidden;
      max-height: 350px;
      border-radius: 0 0 8px 8px;
    }

    .angle-left {
      float: left;
      margin-top: 14px;
      color: #636363;
      width: 10px;
    }

    #combo-box li {
      cursor: pointer
    }

    .fa {
      font-size: 1.1rem
    }

    #input-search {
      display: block;
      border-bottom: 1px solid gray;
      border-left: unset;
      border-right: unset;
      border-top: unset;
      width: 100%;
      margin-bottom: 10px;
    }

    .brand-select {
      line-height: 35px;
      padding: 0px 10px;
      border-radius: 5px;
      background-color: #358fdc;
      color: white;
      margin-top: 2px;
      margin-bottom: 2px;
      transition: all 0.3s;
    }

    #categories {
      cursor: pointer;
      user-select: none
    }

    #categories>li {
      position: relative;
    }

    #categories>li ul>li {
      position: relative;
    }

    #area {
      text-align: right
    }

    #price {
      text-decoration-line: line-through;
      display: inline-block;
      float: left;
      font-size: 14px;
      margin-right: 12px;
    }

    .price {
      display: inline-block;
      float: right;
    }

    .pro_name {
      line-height: 28px;
    }

    .nav-link {
      padding: 5px 10px !important;
    }

    .dropdown-menu-right,
    .dropdown-menu {
      text-align: right
    }

    ul {
      padding: unset;
      margin-bottom: 3px;
    }

    .angle-right {
      width: 6px;
    }
  </style>
@endsection
