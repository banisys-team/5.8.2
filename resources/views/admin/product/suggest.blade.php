@extends('layouts.admin.app')
@section('content')
    <div class="container mt-4 mt-lg-5 pb-5" id="area" style="position: relative">
        <div class="row">
            <div class="col-12 col-lg-3">
                انقضا تا ساعت:
                <input type="number" v-model="time" style="width: 60px;text-align: center">
                <a @click="timerSubmit" class="btn-submit mr-3">ثبت</a>
            </div>
            <div class="col-12 col-lg-2 mt-4 mt-lg-0">
                <p id="demo" class="ltr text-center m-0"></p>
            </div>
        </div>
        <div class="row mt-4 mt-lg-5">
            <div class="col-12">
                <div class="form-group d-flex flex-row" style="position: relative">
                    <input id="name" type="text" class="col-lg-5 form-control" @keyup.enter="searchName"
                           placeholder="نام محصول را برای افزودن به پیشنهاد ویژه جستجو کنید..."
                           style="display: inline-block;border-bottom-left-radius: 0;border-top-left-radius: 0;font-size: 15px;"
                           v-model="name" autofocus>
                    <button type="button" class="btn btn-dark" @click="searchName"
                            style="border: unset;border-bottom-right-radius: 0;border-top-right-radius: 0;background-color: #343a40;padding: 0 9px;">
                        <img src="/assets/images/search-white.svg" style="width: 20px;">
                    </button>
                    <button v-if="product.name" class="result mr-3 d-none d-lg-block" @click="addSuggest()">
                        @{{ product.name }}
                    </button>
                </div>
            </div>
            <div class="col-12 d-lg-none">
                <button v-if="product.name" class="result mr-3" @click="addSuggest()">
                    @{{ product.name }}
                </button>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12">
                <div class="scrollme">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">ردیف</th>
                            <th scope="col">نام محصول</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr v-for="(suggest, index) in suggests" v-if="suggest.product">
                            <td>@{{index+1}}</td>
                            <td>@{{suggest.product.name}}</td>
                            <td>
                                <a @click="deleteSuggest(suggest.id)" style="font-size: 20px;">
                                    <i style="color: #dc3545;font-style: normal;font-size: 17px">✖</i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        new Vue({
            el: '#area',
            data: {
                name: '',
                product: [],
                suggests: [],
                time: '',
            },
            methods: {
                addSuggest() {
                    let vm = this
                    let x = this.suggests.filter(suggest => suggest.product_id === this.product.id)

                    if (x.length != 0) {
                        swal.fire(
                            {
                                text: "این محصول در لیست پیشنهاد ویژه وجود دارد!",
                                icon: "warning",
                                confirmButtonText: 'باشه',
                            }
                        )
                        return
                    }

                    axios.get(`/admin/product/suggest/add/${this.product.id}`).then(res => {
                        swal.fire(
                            {
                                text: "با موفقیت ثبت شد.",
                                icon: "success",
                                confirmButtonText: 'باشه',
                            }
                        )
                        vm.fetchSuggest()
                    })
                },
                fetchSuggest() {
                    let _this = this
                    axios.get(`/admin/product/suggests/fetch`).then(res => {
                        _this.suggests = res.data
                    })
                },
                async fetchTime() {
                    let vm = this
                    await axios.get(`/admin/product/suggest/timer/fetch`).then(res => {
                        vm.time = res.data
                    })
                    this.timer()
                },
                searchName() {
                    let _this = this
                    if (this.name.length > 0) {
                        axios.get('/admin/product/a/search/name', {
                            params: {name: this.name}
                        }).then(res => {
                            if (Object.keys(res.data).length) {
                                _this.product = res.data
                            } else {
                                swal.fire(
                                    {
                                        text: "محصول یافت نشد!",
                                        icon: "warning",
                                        confirmButtonText: 'باشه',
                                    }
                                )
                                _this.product = []
                            }
                        })
                    }
                },
                deleteSuggest(id) {
                    let vm = this
                    axios.get(`/admin/product/suggest/delete/${id}`).then(res => {
                        swal.fire(
                            {
                                text: "با موفقیت حذف شد.",
                                icon: "success",
                                confirmButtonText: 'باشه',
                            }
                        )
                        vm.fetchSuggest()
                    })
                },
                timer() {
                    const monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                    ]

                    const d = new Date()
                    let year = new Date().getFullYear()


                    var countDownDate =
                        new Date(monthNames[d.getMonth()] + " " + d.getDate() + ", " + year + " " + this.time + ":00:00 GMT+4:30").getTime()
                    var x = setInterval(() => {

                        var now = new Date().getTime()

                        var distance = countDownDate - now

                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        // Output the result in an element with id="demo"
                        document.getElementById("demo").innerHTML = hours + ": "
                            + minutes + ": " + seconds


                        if (distance < 0) {
                            clearInterval(x)
                            document.getElementById("demo").innerHTML = "!منقضی شد"
                        }
                    }, 1000);
                },
                timerSubmit() {
                    let vm = this
                    axios.get(`/admin/product/suggests/timer/store/${this.time}`).then(res => {
                        location.reload()
                    })
                },
            },
            mounted() {
                this.fetchSuggest()
                this.fetchTime()
            }
        })
    </script>

    <script>
        $("#side_product").addClass("menu-open");
        $("#side_product_suggest").addClass("active");
    </script>

@endsection
@section('style')
    <style>
        .result {
            display: block;
            background: #3490dc;
            color: white;
            padding: 5px 17px;
            border-radius: 5px;
            cursor: pointer;
            border: unset;
        }

        .btn-submit {
            background: #17a2b8;
            color: white !important;
            padding: 1px 11px;
            border-bottom: 3px solid #147f90;
            border-radius: 5px;
        }

        #demo {
            font-weight: bold;
            font-size: 20px;
            color: #5a5a5a;
        }

        .scrollme {
            overflow-x: auto;
        }
    </style>
@endsection
