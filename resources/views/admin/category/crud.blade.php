@extends('layouts.admin.app')
@section('content')
  <div class="container mt-5" id="content">
    <div class="row">
      <div class="col-md-12">
        <div class="container">
          <span class="add-label">افزودن دسته بندی</span>
          <div class="row add-row">
            <div class="col-md-3">
              <div class="form-group">
                <label class="px-2">عنوان</label>
                <input type="text" class="form-control" v-model="form.name" @keydown.enter="formSubmit">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="px-2" for="parent">دسته والد</label>
                <button @click="flag=!flag" id="btn-cat">
                  @{{ form.parent }}
                  <img id="angle-down" src="/assets/images/arrow-down.svg">
                </button>

                <div v-if="flag" id="combo-box">
                  <ul v-if="flag1">
                    <li id="root" @click="fixRoot">ریشه</li>
                    <li id="root-cat" v-for="root in roots" @click="fetchChild(root.id,root.name,'root')">
                      @{{ root.name }}
                      <img class="angle-left" src="/assets/images/left-arrow3.svg">
                    </li>
                  </ul>

                  <ul v-if="flag2">
                    <li id="back" @click="back">
                      <img src="/assets/images/right-arrow.svg" class="angle-right">
                      <span style="margin-right: 3px;padding-top: 3px;display: inline-block;">
                        @{{ holder.parentName }}
                      </span>

                    </li>
                    <li id="fix" @click="fixCat">@{{ holder.selfName }}</li>
                    <li class="children-item" v-for="child in children"
                      @click="fetchChild(child.id,child.name,child.children_recursive.length)">
                      @{{ child.name }}
                      <img v-if="child.children_recursive.length" class="angle-left" src="/assets/images/left-arrow3.svg">
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-md-2" v-if="flagPriority">
              <div class="form-group">
                <label class="px-2">جایگاه قرارگیری</label>
                <input type="text" class="form-control" v-model="form.priority" @keydown.enter="formSubmit">
              </div>
            </div>
            <div class="col-lg-2 col-submit">
              <div class="form-group">
                <button type="button" class="btn btn-primary" @click="formSubmit">
                  ثبت
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mt-4 pr-4 pr-lg-0 pb-5">
      <div class="col-1 mx-3"></div>
      <div class="col-8 p-0">

        <ul id="categories">
          <template v-for="(cat, key, index) in cats">

            <li class="mt-4" style="position: relative;list-style: none;">
              <input @keydown.enter="editPriority($event,cat.id)" :value="cat.priority" class="priority">

              <img src="/assets/images/left-arrow.svg" class="arrow">

              <input @keydown.enter="editCategory($event,cat.id)" :value="cat.name" class="root-edit">
              <ul class="mr-3" style="font-size: 14px;list-style: disc">
                <li v-for="item in cat.children_recursive" class="my-2" style="list-style: circle">
                  <input @keydown.enter="editCategory($event,item.id)" :value="item.name" class="sub-cat">

                  <ul class="mr-3" style="font-size: 14px">
                    <li v-for="item2 in item.children_recursive" class="my-2" style="list-style: disc">
                      <input @keydown.enter="editCategory($event,item2.id)" :value="item2.name" class="sub-cat">
                    </li>
                  </ul>

                </li>
              </ul>
            </li>
            <hr v-if="cats.length !== key+1">
          </template>
        </ul>

      </div>
    </div>
  </div>
@endsection

@section('script')
  <script>
    new Vue({
      el: '#content',
      data: {
        form: {
          parent: 'انتخاب کنید...',
          priority: null,
          name: '',
        },
        categories: [],
        roots: [],
        cats: [],
        flagPriority: false,
        holder: {
          selfName: 'ریشه',
          selfId: '',
          parentName: '',
          parentId: '',
          grandName: '',
          grandId: '',
        },
        flag: false,
        flag1: true,
        flag2: false,
        children: [],
        parents: '',
        parentName: [],
      },
      methods: {
        catsFetch() {
          let vm = this
          axios.get('/admin/category/fetch/list').then((res) => {
            vm.cats = res.data
          })
        },
        editCategory(e, id) {
          if (e.target.value === '') {
            this.deleteCategory(id)
          } else {
            let formData = new FormData()
            formData.append('name', e.target.value)
            formData.append('id', id)

            axios.post('/admin/category/edit', formData)
              .then(() => {
                location.reload()
              }).catch((error) => {
                let text = error.response.data.errors
                if (Array.isArray(text.name)) {
                  alert(text.name[0])
                }
              })
          }
        },
        deleteCategory(id) {
          axios.get(`/admin/category/delete/${id}`)
            .then(res => {
              if (res.data === "cant") {
                swal.fire({
                  text: "این دسته دارای وابستگی می باشد و نمی توان آن را حذف کرد",
                  icon: "warning",
                  confirmButtonText: 'باشه',
                })
                setTimeout(() => {
                  location.reload()
                }, 5000);
              } else {
                location.reload()
              }
            })
        },
        parentChange() {
          if (this.form.parent === 'null') {
            this.flagPriority = true
          } else {
            this.form.priority = null
            this.flagPriority = false
          }
        },
        editPriority(e, id) {
          let formData = new FormData()
          formData.append('priority', e.target.value)
          formData.append('id', id)

          axios.post('/admin/category/priority/edit', formData)
            .then(() => {
              location.reload()
            })
        },
        fetchCategories() {
          let _this = this
          axios.get(`/admin/category/fetch`).then(res => {
            _this.categories = res.data
            _this.roots = res.data.filter(cat => cat.parent === null)
          })
        },
        fixRoot() {
          this.flagPriority = true
          this.form.parent = 'ریشه'
          this.flag = false
        },
        fetchChild(id, name, length) {
          this.flagPriority = false
          this.holder.grandName = this.holder.parentName
          this.holder.grandId = this.holder.parentId

          this.holder.parentName = this.holder.selfName
          this.holder.parentId = this.holder.selfId

          this.holder.selfName = name
          this.holder.selfId = id

          if (length === 0) {
            this.fixCat()
            this.holder.selfName = 'ریشه'
            // this.holder.selfId = ''
            this.holder.parentName = ''
            this.holder.parentId = ''
            this.holder.grandName = ''
            this.holder.grandId = ''
            this.flag1 = true
            this.flag2 = false
            this.fetchCategories()
            return
          }

          this.flag1 = false
          this.flag2 = true


          this.children = this.categories.filter(cat => cat.parent === id)
        },
        fixCat() {
          this.form.parent = this.holder.selfName
          this.flag = false
        },
        back() {
          let http = axios.create({
            baseURL: 'http://localhost:8000/api/admin'
          })
          http.defaults.withCredentials = true

          if (this.holder.parentName === 'ریشه') {
            this.flag1 = true
            this.flag2 = false
            this.holder.selfName = 'ریشه'
            this.holder.selfId = ''
            this.holder.parentName = ''
            this.holder.parentId = ''
            this.holder.grandName = ''
            this.holder.grandId = ''
          } else {
            this.children = this.categories.filter(cat => cat.parent === this.holder.parentId)

            this.holder.selfName = this.holder.parentName
            this.holder.selfId = this.holder.parentId

            this.holder.parentName = this.holder.grandName
            this.holder.parentId = this.holder.grandId

            let x = this.categories.filter(cat => cat.id === this.holder.grandId)
            if (x[0]) {
              let y = this.categories.filter(cat => cat.id === x[0].parent)
              if (y[0]) {
                this.holder.grandName = y[0].name
                this.holder.grandId = y[0].id
              } else {
                this.holder.grandName = 'ریشه'
                this.holder.grandId = ''
              }
            }
          }
        },
        formSubmit() {
          if (this.form.name === '' || this.form.parent === '') {
            swal.fire({
              text: "دسته والد و نام دسته را وارد کنید",
              icon: "warning",
              confirmButtonText: 'باشه',
            })
            return
          }

          axios.post('/admin/category/store', {
            parent: this.holder.selfId,
            name: this.form.name,
            priority: this.form.priority,
          }).then(() => {
            location.reload()
          }).catch((error) => {
            let text = error.response.data.errors
            if (Array.isArray(text.name)) alert(text.name[0])
          })
        },
      },
      mounted() {
        this.catsFetch()
        this.fetchCategories()
      }
    })
  </script>

  <script>
    $("#side_category").addClass("menu-open");
    $("#side_category_add").addClass("active");
  </script>
@endsection

@section('style')
  <style>
    .sub-cat {
      border: unset;
      background-color: #f4f6f9;
    }

    hr {
      margin-top: .7rem;
      margin-bottom: .7rem
    }

    .add-label {
      position: absolute;
      margin-right: 10px;
      top: -14px;
      background-color: #f4f6f9;
      color: #9f9f9f;
    }

    .add-row {
      border: 1px #dedede solid;
      padding: 35px 5px 15px 5px;
      border-radius: 10px
    }

    .root-edit {
      color: #dc3545;
      font-weight: bold;
      border: unset;
      background-color: #f4f6f9;
    }

    .priority {
      position: absolute;
      right: -68px;
      padding: 5px 7px;
      border-radius: 50px;
      color: #868686;
      font-size: 15px;
      width: 35px;
      border: unset;
      text-align: center;
      top: -3px;
      font-weight: bold;
    }

    .arrow {
      width: 21px;
      position: absolute;
      top: 3px;
      right: -34px;
    }

    .col-submit {
      margin-top: 32px
    }

    .form-group label {
      font-size: 15px;
    }
  </style>
  <style>
    .children-item {
      margin-right: 10px;
    }

    #fix {
      color: black;
      font-weight: bold;
      background-color: #d1d1d1;
      padding-right: 6px;
      border-radius: 6px
    }

    .right {
      float: right;
      margin: 4px 0px 0px 5px;
    }

    #back {
      color: #a0a0a0
    }

    #root-cat {
      line-height: 35px;
    }

    #root {
      color: black;
      font-weight: bold;
      background-color: #d1d1d1;
      padding-right: 8px;
      border-radius: 6px
    }

    li {
      list-style: none
    }

    #btn-cat {
      position: relative;
      background-color: white;
      border: 1px solid #ced4da;
      width: 100%;
      padding: 2px 8px 0 8px;
      border-radius: 5px;
      text-align: right;
      color: #484848;
      height: 41px;
      overflow: hidden;
    }

    #angle-down {
      float: left;
      width: 11px;
      margin: 5px 2px 0px 5px;
    }

    #combo-box {
      position: absolute;
      list-style: none;
      top: 73px;
      z-index: 99;
      background-color: white;
      padding: 5px 10px;
      width: 100%;
      line-height: 35px;
      border: 1px solid #c8c8c8;
      overflow-x: hidden;
      max-height: 350px;
      border-radius: 0 0 8px 8px;
    }

    .angle-left {
      float: left;
      margin-top: 14px;
      color: #636363;
      width: 10px;
    }

    .angle-right {
      width: 7px;
    }

    #combo-box li {
      cursor: pointer
    }

    .fa {
      font-size: 1.1rem
    }

    #input-search {
      display: block;
      border-bottom: 1px solid gray;
      border-left: unset;
      border-right: unset;
      border-top: unset;
      width: 100%;
      margin-bottom: 10px;
    }

    .brand-select {
      line-height: 35px;
      padding: 0px 10px;
      border-radius: 5px;
      background-color: #358fdc;
      color: white;
      margin-top: 2px;
      margin-bottom: 2px;
      transition: all 0.3s;
    }

    #categories {
      cursor: pointer;
      user-select: none
    }

    #categories>li {
      position: relative;
    }

    #categories>li ul>li {
      position: relative;
    }

    #area {
      text-align: right
    }

    #price {
      text-decoration-line: line-through;
      display: inline-block;
      float: left;
      font-size: 14px;
      margin-right: 12px;
    }

    .price {
      display: inline-block;
      float: right;
    }

    .pro_name {
      line-height: 28px;
    }

    .dropdown-menu-right,
    .dropdown-menu {
      text-align: right
    }

    ul {
      margin-bottom: 3px;
    }
  </style>
@endsection
