<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="JcUKshTvp78igsVlwFYSZKMAV80Uvsp8Fkmc73x3">
    <title>صفحه ثبت نام</title>
    <script src="{{ asset('/js/dress/app.js')}}"></script>
    <link rel="stylesheet" href="/layout_kaj/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout_kaj/css/style.css">
    <link rel="stylesheet" href="/layout_kaj/css/swiper.css">

    <style>
        h3 {
            font-size: 25px
        }

        body {
            background-color: #eee !important;
            height: 100vh;
        }

        #swal2-content {
            text-align: center;
        }

        * {
            direction: rtl;
            text-align: right
        }

        .a-paragraph {
            font-size: 14px;
            color: #777;
            /*line-height:30px;*/
        }

        /*.a-h3 {*/
        /*    background: #e2e2e2;*/
        /*}*/

        .a-main-container {
            border: 1px solid #d4d4d4;
            border-radius: 15px;
            overflow: hidden;
            background-color: white;
        }

        .a-btn {
            color: white;
            border: 1px solid #00bf6f;
            background-color: #00bf6f;
            position: relative;
            padding: 12px 40px;
            cursor: pointer;
            border-radius: 8px;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            margin: 0 auto;
        }

        .a-btn:hover {
            background: #00a25c;
            color: white;
        }

        .a-btn2 {
            margin-right: 16px;
            padding: 12px;
            cursor: pointer;
        }

        .a-btn2:hover {
            color: #555;
        }

        h3 {
            font-size: 23px
        }

        .a-input {
            border: 1px solid #aaaaaa;
            direction: ltr;
        }

        .a-error {
            font-size: 13px;
            margin-right: 1px;
            color: #dc3545;
        }

        .anime {
            animation: mymove .8s;
        }

        @keyframes mymove {
            0% {
                margin-right: 25px
            }

            50% {
                margin-right: 60px
            }

            100% {
                margin-left: 25px
            }
        }

        body {
            background: white;
        }

        .home-page {
            background-color: #ffffff;
            font-size: 13px;
            border-radius: 8px;
            color: #444;
            font-weight: bold;
            padding: 15px 40px;
        }

        .change-number-btn {
            background-color: #ffffff;
            font-size: 13px;
            border-radius: 8px;
            color: #444;
            font-weight: bold;

        }

        .change-number-btn:hover, .home-page:hover {
            background-color: #ddd;
            color: #444;
        }

        .left-arrow {
            font-size: 28px;
            margin-right: 15px;
            font-style: normal;
            color: #fff;
        }

        .right-arrow {
            font-style: normal;
            font-size: 20px;
            color: #555;
            margin-left: 10px;
        }

        .sending {
            color: #28a745;
            text-align: center;
            margin-top: 5px;
            font-size: 12px;
            margin-bottom: 0;
        }

        .login-left {
            background-color: #00bf6f;
            position: relative;
        }

        .login-left > img {
            position: absolute;
            top: 0;
            right: 0;
            width: 80%;
        }

        .login-left > div {
            width: 80%;
            display: flex;
            justify-content: center;
        }

        input {
            outline: #ddd;
        }

        @media only screen and (max-width: 768px) {
            body {
                background-color: #fff !important;
            }

            .home-page {
                background-color: #eee;
                margin: 0 auto;
                display: block;
                text-align: center;
                padding: 15px 40px;
            }

            .change-number-btn {
                background-color: #eee;
                margin: 0 auto;
                display: block;
                text-align: center;
                padding: 15px 40px;
            }
        }
    </style>
</head>
<body>

<div id="area">
    <div class="px-0 mt-lg-5">
        <div class="d-none d-lg-flex justify-content-center align-items-center">
            <div class="col-md-7 d-flex flex-wrap px-0  a-main-container">
                <div class="col-lg-6 px-0 py-5">
                    <div class=" pt-5">
                        <div class="col-md-12 px-5">
                            <h3 class="text-end a-h3" style="font-weight: bold">ثبت‌ نام</h3>
                            <input id="auto-focus" class="text-end col-12 mt-3 mx-auto a-input"
                                   v-model="form.mobile" type="text"
                                   placeholder="شماره موبایل"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                            <input id="auto-focus" class="text-end col-12 mt-3 mx-auto a-input"
                                   v-model="form.password" type="password"
                                   placeholder="کلمه عبور"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                            <input id="auto-focus" class="text-end col-12 mt-3 mx-auto a-input"
                                   v-model="form.passwordConfirmation" type="password"
                                   placeholder="تکرار کلمه عبور"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        </div>
                    </div>
                    <div class="pb-4 px-5">
                        <div class="text-left mt-3 mx-auto d-block">
                            <a class="a-btn" @click="validate()">
                                <span>
                                    ثبت
                                </span>
                                <img src="/assets/images/left-arrow-white.svg" alt=""
                                     style="width: 10px;margin: 0px 8px 2px 0px;">
                            </a>
                        </div>
                    </div>
                    <div class="pb-4 px-5">
                        <div class="col-12 mt-2 px-1">
                            <p class="a-error">@{{ error.mobile }}</p>
                            <p class="a-error">@{{ error.password }}</p>
                            <p class="a-error">@{{ error.password_confirmation }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 px-0 login-left py-5 d-flex flex-wrap align-items-end justify-content-center">
                    <div class="mt-3 py-5">
                        <a href="/" class="home-page">صفحه اصلی</a>
                        <a href="/login" class="home-page mr-3">ورود</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-lg-none">
            <div class="d-flex justify-content-center align-items-center px-4"
                 style="background: #1ac977;height:100px">
                <div class="col-6">
                    {{--                    <img :src=`/images/${logo}` alt="">--}}
                </div>
            </div>
            <div style="border-radius: 25px;
                    margin-top: -21px !important;
                    background: white;">
                <div class="px-5 pt-5">
                    <a href="/" class="home-page">
                        <img src="/assets/images/home.svg" alt="" style="width: 18px;margin-left: 6px;opacity: 0.8 ">
                        صفحه اصلی
                    </a>
                    <a href="/login" class="home-page mt-3">
                        ورود ( قبلا ثبت نام کردم )
                    </a>
                </div>
                <div class="pt-5">
                    <div class="col-md-12 px-5">
                        <h3 class="text-end a-h3" style="font-weight: bold;font-size: 20px;">ثبت‌ نام سریع</h3>
                        <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                               v-model="form.mobile" type="text"
                               placeholder="شماره همراه"
                               style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                               v-model="form.password" type="password"
                               placeholder="کلمه عبور"
                               style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                               v-model="form.passwordConfirmation" type="password"
                               placeholder="تکرار کلمه عبور"
                               style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                    </div>

                </div>
                <div class="px-5">
                    <div class="text-left mt-3 mx-auto d-block">
                        <a class="a-btn" @click="validate()">
                            ثبت
                            <img src="/assets/images/left-arrow-white.svg" alt=""
                                 style="width: 10px;margin: 0px 8px 2px 0px;">
                        </a>
                    </div>
                </div>
                <div class="pb-4 px-5">
                    <div class="col-12 mt-2 px-1">
                        <p class="a-error">@{{ error.mobile }}</p>
                        <p class="a-error">@{{ error.password }}</p>
                        <p class="a-error">@{{ error.password_confirmation }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#area',
        data: {
            form: {
                mobile: '',
                password: '',
                passwordConfirmation: '',
            },
            error: {
                mobile: '',
                password: '',
            },
            error_register: [],
        },
        methods: {
            async validate() {
                let vm = this
                await axios.post('/validation/register', {
                    mobile: this.form.mobile,
                    password: this.form.password,
                    password_confirmation: this.form.passwordConfirmation,
                }).then(() => {
                    vm.register()
                }).catch(error => {
                    vm.error.mobile = ""
                    vm.error.password = ""
                    vm.error_register = error.response.data.errors
                    let x = error.response.data.errors
                    if (Array.isArray(x.mobile)) vm.error.mobile = vm.error_register.mobile[0]
                    if (Array.isArray(x.password)) vm.error.password = vm.error_register.password[0]
                })
            },
            register() {
                axios.post('/register/store', {
                    mobile: this.form.mobile,
                    password: this.form.password,
                }).then(() => {
                    window.location.href = '/shipping'
                })
            },
        },
        mounted() {

        }
    })

    document.getElementById("auto-focus").focus()
</script>

</body>
</html>
