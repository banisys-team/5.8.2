<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="JcUKshTvp78igsVlwFYSZKMAV80Uvsp8Fkmc73x3">
    <title>صفحه ورود</title>
    <script src="{{ asset('/js/dress/app.js')}}"></script>
    <link rel="stylesheet" href="/layout_kaj/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout_kaj/css/style.css">
    <link rel="stylesheet" href="/layout_kaj/css/swiper.css">

    <style>
        h3 {
            font-size: 25px
        }

        body {
            background-color: #eee !important;
            height: 100vh;
        }

        #swal2-content {
            text-align: center;
        }

        * {
            direction: rtl;
            text-align: right
        }

        .a-paragraph {
            font-size: 14px;
            color: #777;
            /*line-height:30px;*/
        }

        /*.a-h3 {*/
        /*    background: #e2e2e2;*/
        /*}*/

        .a-main-container {
            border: 1px solid #d4d4d4;
            border-radius: 15px;
            overflow: hidden;
            background-color: white;
        }

        .a-btn {
            color: white;
            border: 1px solid #00bf6f;
            background-color: #00bf6f;
            position: relative;
            padding: 12px 40px;
            cursor: pointer;
            border-radius: 8px;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            margin: 0 auto;
        }

        .a-btn:hover {
            background: #00a25c;
            color: white;
        }

        .a-btn2 {
            margin-right: 16px;
            padding: 12px;
            cursor: pointer;
        }

        .a-btn2:hover {
            color: #555;
        }

        h3 {
            font-size: 23px
        }

        .a-input {
            border: 1px solid #aaaaaa;
            direction: ltr;
        }

        .a-error {
            font-size: 13px;
            margin-right: 1px;
            color: #dc3545;
        }

        .anime {
            animation: mymove .8s;
        }

        @keyframes mymove {
            0% {
                margin-right: 25px
            }

            50% {
                margin-right: 60px
            }

            100% {
                margin-left: 25px
            }
        }

        body {
            background: white;
        }

        .home-page {
            background-color: #ffffff;
            font-size: 13px;
            border-radius: 8px;
            color: #444;
            font-weight: bold;
            padding: 15px 40px;
        }

        .change-number-btn {
            background-color: #ffffff;
            font-size: 13px;
            border-radius: 8px;
            color: #444;
            font-weight: bold;

        }

        .change-number-btn:hover, .home-page:hover {
            background-color: #ddd;
            color: #444;
        }

        .left-arrow {
            font-size: 28px;
            margin-right: 15px;
            font-style: normal;
            color: #fff;
        }

        .right-arrow {
            font-style: normal;
            font-size: 20px;
            color: #555;
            margin-left: 10px;
        }

        .sending {
            color: #28a745;
            text-align: center;
            margin-top: 5px;
            font-size: 12px;
            margin-bottom: 0;
        }

        .login-left {
            background-color: #00bf6f;
            position: relative;
        }

        .login-left > img {
            position: absolute;
            top: 0;
            right: 0;
            width: 80%;
        }

        .login-left > div {
            width: 80%;
            display: flex;
            justify-content: center;
        }

        input {
            outline: #ddd;
        }

        @media only screen and (max-width: 768px) {
            body {
                background-color: #fff !important;
            }

            .home-page {
                background-color: #eee;
                margin: 0 auto;
                display: block;
                text-align: center;
                padding: 15px 40px;
            }

            .change-number-btn {
                background-color: #eee;
                margin: 0 auto;
                display: block;
                text-align: center;
                padding: 15px 40px;
            }
        }
    </style>
</head>
<body>

<div id="area">
    <div v-show="flag1" class="px-0 mt-lg-5">
        <div class="d-none d-lg-flex justify-content-center align-items-center">
            <div class="col-md-7 d-flex flex-wrap px-0  a-main-container">
                <div class="col-lg-6 px-0 py-5">
                    <div class=" pt-5">
                        <div class="col-md-12 px-5">
                            <h3 class="text-end a-h3" style="font-weight: bold">ورود یا ثبت‌ نام</h3>
                            <p class="text-end a-paragraph mt-4" style="line-height: 26px">
                                برای ورود و یا ثبت ‌نام شماره موبایل خود
                                را
                                وارد
                                نمایید تا کد تایید برای شما پیامک
                                شود.
                            </p>
                            <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                                   @keyup.enter="getCode()"
                                   v-model="form.mobile" type="text"
                                   placeholder="شماره موبایل"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px"
                            >
                        </div>
                    </div>
                    <div class="pb-4 px-5">
                        <div class="col-12 mt-2 px-1">
                            <span class="a-error">@{{ error }}</span>
                        </div>
                        <div class="text-left mt-3 mx-auto d-block">
                            <a class="a-btn" @click="getCode()">
                                <span>
                                    ارسال کد
                                </span>
                                <img src="/assets/images/left-arrow-white.svg" alt=""
                                     style="width: 10px;margin: 0px 8px 2px 0px;">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 px-0 login-left py-5 d-flex flex-wrap align-items-end justify-content-center">
                    <!--<img src="/assets/images/profile-back.png" alt="">-->
                    <div class="col-6">
                        <!--<img src="/assets/images/white-logo.svg" alt="">-->
                    </div>
                    <div class="mt-3 py-5">
                        <a href="/" class="home-page">
                            <img src="/assets/images/home.svg" alt=""
                                 style="width: 18px;margin-left: 6px;opacity: 0.8 ">
                            صفحه اصلی
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-lg-none">
            <div class="d-flex justify-content-center align-items-center px-4"
                 style="background: #1ac977;height:150px">
                <!--<img src="/assets/images/profile-back.png" alt="banimode" class="profile-back-img">-->
                <div class="col-6">
                    <!--<img src="/assets/images/white-logo.svg" alt="">-->
                </div>
            </div>
            <div style="border-radius: 25px;
                    margin-top: -15px !important;
                    background: white;">
                <div class="pt-5">
                    <div class="col-md-12 px-5">
                        <h3 class="text-end a-h3" style="font-weight: bold">ورود یا ثبت‌ نام</h3>
                        <p class="text-end a-paragraph mt-3" style="line-height: 30px">
                            برای ورود و یا ثبت ‌نام شماره موبایل خود
                            را
                            وارد
                            نمایید تا کد تایید برای شما پیامک
                            شود
                        </p>
                        <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                               @keyup.enter="getCode()"
                               v-model="form.mobile" type="text"
                               placeholder="شماره همراه"
                               style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px"
                        >
                    </div>
                </div>
                <div class="px-5">
                    <div class="col-12 mt-2 px-1">
                        <span class="a-error">@{{ error }}</span>
                    </div>
                    <div class="text-left mt-3 mx-auto d-block">
                        <a class="a-btn" @click="getCode()">
                            ارسال کد
                            <img src="/assets/images/left-arrow-white.svg" alt=""
                                 style="width: 10px;margin: 0px 8px 2px 0px;">
                        </a>
                    </div>
                </div>
                <div class="px-5 mt-3">
                    <a href="/" class="home-page">
                        <img src="/assets/images/home.svg" alt="" style="width: 18px;margin-left: 6px;opacity: 0.8 ">
                        صفحه اصلی
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div v-show="flag2" class="mt-lg-5">
        <div class="d-none d-lg-flex justify-content-center align-items-center">
            <div class="col-md-7 d-flex flex-wrap px-0  a-main-container">
                <div class="col-lg-6 px-0 py-4">
                    <div class=" pt-5">
                        <div class="col-md-12 px-5">
                            <h3 class="text-end a-h3 " style="font-weight: bold">کد تایید</h3>
                            <p class="text-end a-paragraph mt-4" style="line-height: 26px">
                                کد پیامک شده برای شماره @{{ form.mobile }} را وارد نمایید.
                            </p>
                            <input id="auto-focus2" class="text-end col-12 mt-2 mx-auto a-input"
                                   @keyup.enter="login()"
                                   v-model="code"
                                   type="text"
                                   placeholder="کد تایید"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px"
                            >
                        </div>
                    </div>
                    <div class="pb-5 px-5">
                        <div class="col-md-12 px-0 ">
                            <div class="d-flex justify-content-between align-items-center mt-3 px-4">
                                <p v-if="flagCounter" class="sending">در حال ارسال...</p>
                                <span v-if="flagCounter"
                                      style="color: #575757;margin-top: 5px;">@{{ countdown }} : 00</span>

                            </div>
                            <span v-if="!flagCounter" style="font-size: 13px;color: rgb(255 95 111)" class="mt-3">
                                                    در صورت عدم ارسال پیامک شماره موبایل خود را بررسی کنید
                            </span>
                        </div>
                        <div class="text-left mt-3 mx-auto d-block">
                            <a class="a-btn" @click="login()">
                                <span>
                                    تایید
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 px-0 login-left p-5 d-flex flex-wrap align-items-end justify-content-center">
                    <!--<img src="/assets/images/profile-back.png" alt="">-->
                    <div class="col-6">
                        <!--<img src="/assets/images/white-logo.svg" alt="">-->
                    </div>
                    <div class="mb-5 change-number-btn">
                        <a v-if="!flagCounter" class="a-btn2" @click="back()">
                            <i class="right-arrow">‹</i>
                            اصلاح شماره موبایل
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-lg-none">
            <div class="d-flex justify-content-center align-items-center px-4"
                 style="background: #1ac977;height:150px">
                <!--<img src="/assets/images/profile-back.png" alt="banimode" class="profile-back-img">-->
                <div class="col-6">
                    <!--<img src="/assets/images/white-logo.svg" alt="">-->
                </div>
            </div>
            <div style="border-radius: 25px;
                    margin-top: -15px !important;
                    background: white;">
                <div class=" pt-5">
                    <div class="col-md-12 px-5">
                        <h3 class="text-end  a-h3 " style="font-weight: bold">کد تایید</h3>
                        <p class="text-end a-paragraph mt-3" style="line-height: 30px">
                            کد پیامک شده برای شماره @{{ form.mobile }} را وارد نمایید.
                        </p>
                        <input id="auto-focus2" class="text-end col-12 mt-4  mx-auto a-input"
                               @keyup.enter="login()"
                               v-model="code"
                               type="text"
                               placeholder="کد تایید"
                               style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px"
                        >
                    </div>
                </div>
                <div class="px-5">
                    <div class="text-left mt-3 mx-auto d-block">
                        <div class="col-md-12 px-0 ">
                            <div class="d-flex flex-row-reverse justify-content-between align-items-center mt-3">
                                <span v-if="flagCounter" style="color: #575757">@{{ countdown }} : 00</span>
                                <p v-if="flagCounter" class="sending">در حال ارسال...</p>
                            </div>
                            <span v-if="!flagCounter" style="font-size: 13px;color: rgb(255 95 111)" class="mt-3">
                                                    در صورت عدم ارسال پیامک شماره موبایل خود را بررسی کنید
                            </span>
                        </div>
                        <div class="text-left mt-4 mx-auto d-block">
                            <a class="a-btn" @click="login()">
                                تایید
                            </a>
                        </div>
                    </div>
                </div>
                <div class="px-5 mt-3">
                    <a v-if="!flagCounter" class="change-number-btn" @click="back()">
                        <i class="right-arrow">‹</i>
                        اصلاح شماره موبایل
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    new Vue({
        el: '#area',
        data: {
            form: {
                name: '',
                mobile: '',
                nationalCode: '',
            },
            code: '',
            flag1: true,
            flag2: false,
            error: '',
            countdown: 20,
            flagCounter: true,
        },
        methods: {
            async getCode() {
                let vm = this
                if (this.form.mobile.length !== 11) {
                    // let element = document.getElementsByClassName("a-error")
                    // element[0].classList.add("anime")
                    this.error = 'شماره همراه را به صورت صحیح وارد کنید'
                    return false
                }

                await axios.post('/login/send/code', {
                    mobile: this.form.mobile,
                }).then((res) => {
                    console.log(res.data)
                    vm.flag1 = false
                    vm.flag2 = true
                    vm.error = ''
                    let counter = setInterval(function () {
                        vm.countdown--
                    }, 1000)
                    setTimeout(function () {
                        clearInterval(counter)
                        vm.flagCounter = false
                    }, 20000)
                })
                document.getElementById("auto-focus2").focus()
            },
            login() {
                axios.post('/login/check/code', {
                    code: this.code,
                    mobile: this.form.mobile,
                }).then((res) => {
                    if (res.data === '111') {
                        window.location.href = '/shipping'
                    } else {
                        swal.fire(
                            {
                                text: "کد تایید صحیح نمی باشد!",
                                type: "error",
                                confirmButtonText: 'باشه',
                            }
                        )
                    }
                })
            },
            back() {
                this.flag1 = true
                this.flag2 = false
                this.flagCounter = true
                this.countdown = 20
            },
        }
    })

    document.getElementById("auto-focus").focus()
</script>

</body>
</html>
