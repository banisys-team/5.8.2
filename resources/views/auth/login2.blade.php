<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="JcUKshTvp78igsVlwFYSZKMAV80Uvsp8Fkmc73x3">
    <title>صفحه ورود</title>
    <script src="{{ asset('/js/dress/app.js')}}"></script>
    <link rel="stylesheet" href="/layout_kaj/css/bootstrap.min.css">
    <link rel="stylesheet" href="/layout_kaj/css/style.css">
    <link rel="stylesheet" href="/layout_kaj/css/swiper.css">

    <style>
        h3 {
            font-size: 25px
        }

        body {
            background-color: #eee !important;
            height: 100vh;
        }

        #swal2-content {
            text-align: center;
        }

        * {
            direction: rtl;
            text-align: right
        }

        .a-paragraph {
            font-size: 14px;
            color: #777;
            /*line-height:30px;*/
        }

        /*.a-h3 {*/
        /*    background: #e2e2e2;*/
        /*}*/

        .a-main-container {
            border: 1px solid #d4d4d4;
            border-radius: 15px;
            overflow: hidden;
            background-color: white;
        }

        .a-btn {
            color: white;
            border: 1px solid #00bf6f;
            background-color: #00bf6f;
            position: relative;
            padding: 12px 40px;
            cursor: pointer;
            border-radius: 8px;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            margin: 0 auto;
        }

        .back-btn {
            color: #909090;
            border: 1px solid #c9c9c9;
            position: relative;
            padding: 12px 40px;
            cursor: pointer;
            border-radius: 8px;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            margin: 0 auto;
            font-size: 14px;

        }


        .a-btn:hover {
            background: #00a25c;
            color: white;
        }

        .a-btn2 {
            margin-right: 16px;
            padding: 12px;
            cursor: pointer;
        }

        .a-btn2:hover {
            color: #555;
        }

        h3 {
            font-size: 23px
        }

        .a-input {
            border: 1px solid #aaaaaa;
            direction: ltr;
        }

        .a-error {
            font-size: 13px;
            margin-right: 1px;
            color: #dc3545;
        }

        .anime {
            animation: mymove .8s;
        }

        @keyframes mymove {
            0% {
                margin-right: 25px
            }

            50% {
                margin-right: 60px
            }

            100% {
                margin-left: 25px
            }
        }

        body {
            background: white;
        }

        .home-page {
            background-color: #ffffff;
            font-size: 13px;
            border-radius: 8px;
            color: #444;
            font-weight: bold;
            padding: 15px 40px;
        }

        .change-number-btn {
            background-color: #ffffff;
            font-size: 13px;
            border-radius: 8px;
            color: #444;
            font-weight: bold;

        }

        .change-number-btn:hover, .home-page:hover {
            background-color: #ddd;
            color: #444;
        }

        .left-arrow {
            font-size: 28px;
            margin-right: 15px;
            font-style: normal;
            color: #fff;
        }

        .right-arrow {
            font-style: normal;
            font-size: 20px;
            color: #555;
            margin-left: 10px;
        }

        .sending {
            color: #28a745;
            text-align: center;
            margin-top: 5px;
            font-size: 12px;
            margin-bottom: 0;
        }

        .login-left {
            background-color: #00bf6f;
            position: relative;
        }

        .login-left > img {
            position: absolute;
            top: 0;
            right: 0;
            width: 80%;
        }

        .login-left > div {
            width: 80%;
            display: flex;
            justify-content: center;
        }

        input {
            outline: #ddd;
        }

        @media only screen and (max-width: 768px) {
            body {
                background-color: #fff !important;
            }

            .home-page {
                background-color: #eee;
                margin: 0 auto;
                display: block;
                text-align: center;
                padding: 15px 40px;
            }

            .change-number-btn {
                background-color: #eee;
                margin: 0 auto;
                display: block;
                text-align: center;
                padding: 15px 40px;
            }
        }
    </style>
</head>
<body>

<div id="area">
    <div class="px-0 mt-lg-5">
        <div class="d-none d-lg-flex justify-content-center align-items-center">
            <div class="col-lg-7 d-flex flex-wrap px-0 a-main-container mt-4">
                <div class="col-lg-6 px-0">

                    <div v-show="flag1">
                        <div class="col-12 px-5 pt-5">
                            <h3 class="text-end a-h3" style="font-weight: bold">ورود</h3>
                            <input id="auto-focus" class="text-end col-12 my-3 mx-auto a-input"
                                   @keyup.enter="getCode()"
                                   v-model="form.mobile" type="text"
                                   placeholder="شماره موبایل"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                            <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                                   v-model="form.password" type="password"
                                   placeholder="کلمه عبور"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        </div>
                        <div class="text-left mt-4 mx-auto d-block pb-4 px-5">
                            <a class="a-btn" @click="login">
                                ورود
                                <img src="/assets/images/left-arrow-white.svg" alt=""
                                     style="width: 10px;margin: 2px 8px 2px 0;">
                            </a>
                            <a @click="forgetPass" class="d-block mt-4 px-2"
                               style="color:#7e7e7e;text-decoration: underline;cursor: pointer">
                                کلمه عبور خود را فراموش کرده اید؟
                            </a>
                        </div>
                        <div class="col-12 mt-2 pb-4 px-5">
                            <p class="a-error">@{{ error }}</p>
                        </div>
                    </div>

                    <div class="row" v-show="flag2">
                        <div class="col-12 px-5 pt-5">
                            <h3 class="text-end a-h3" style="font-weight: bold;font-size: 20px">درخواست رمز عبور
                                جدید</h3>
                            <p class="mt-3" style="line-height: 25px;color: #999999">
                                در صورتی که رمز عبور خود را فراموش کرده اید، لطفا شماره موبایل خود را وارد کنید.
                            </p>
                            <input id="auto-focus" class="text-end col-12 my-2 mx-auto a-input"
                                   @keyup.enter="getCode()"
                                   v-model="form.mobile" type="text"
                                   placeholder="شماره موبایل"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        </div>
                        <div class="col-5 pr-5 pb-4">
                            <div class="text-left mt-4 mx-auto pb-4">
                                <a class="btn back-btn mx-auto" @click="back">
                                    بازگشت
                                </a>
                            </div>
                        </div>
                        <div class="col-7 pl-5 pb-4">
                            <div class="text-left mt-4 mx-auto pb-4">
                                <a class="a-btn" @click="sendCode">
                                    ارسال درخواست
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row" v-show="flag3">
                        <div class="col-12 px-5 pt-5">
                            <p v-if="flagCounter" class="sending">در حال ارسال...</p>
                            <p v-if="flagCounter"
                                  style="color: #575757;margin-top: 10px;margin-bottom: 0;text-align: center">
                                @{{ countdown }} : 00
                            </p>
                        </div>
                        <div class="col-12 px-5">
                            <input id="auto-focus" class="text-end col-12 my-3 mx-auto a-input"
                                   @keyup.enter="getCode()"
                                   v-model="userCode" type="text"
                                   placeholder="کد ارسالی را وارد کنید"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        </div>
                        <div class="col-6 pr-5 pb-5">
                            <div class="text-left mt-2 mx-auto">
                                <a v-if="!flagCounter" class="btn back-btn mx-auto" @click="back2">
                                    اصلاح شماره
                                </a>
                            </div>
                        </div>
                        <div class="col-6 pl-5 pb-5">
                            <div class="text-left mt-2 mx-auto">
                                <a class="a-btn" @click="checkCode">
                                    بعدی
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row" v-show="flag4">
                        <div class="col-12 px-5 pt-5">
                            <h3 class="text-end a-h3" style="font-weight: bold;font-size: 17px">
                                کلمه عبور جدید خود را وارد کنید
                            </h3>
                            <input id="auto-focus" class="text-end col-12 mt-4 mx-auto a-input"
                                   type="password" @keyup.enter="getCode()" v-model="newPass"
                                   placeholder="کلمه عبور"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                            <input id="auto-focus" class="text-end col-12 mt-4 mx-auto a-input"
                                   @keyup.enter="getCode()" v-model="reNewPass" type="password"
                                   placeholder="تکرار کلمه عبور"
                                   style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        </div>

                        <div class="col-6 pr-5"></div>

                        <div class="col-6 pl-5">
                            <div class="text-left mt-4 mx-auto pb-5">
                                <a class="a-btn" @click="submitNewPass">
                                    ثبت
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 px-0 login-left d-flex flex-wrap align-items-end justify-content-center">
                    <div class="mt-3 py-5">
                        <a href="/" class="home-page">صفحه اصلی</a>
                        <a href="/register" class="home-page mr-3">
                            ثبت نام
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-lg-none">
            <div class="d-flex justify-content-center align-items-center px-4"
                 style="background: #1ac977;height:150px">
                <div class="col-6">
                    {{--                    <img :src=`/images/${logo}` alt="">--}}
                </div>
            </div>
            <div style="border-radius: 25px;
                    margin-top: -21px !important;
                    background: white;">
                <div class="px-5 pt-5">
                    <a href="/" class="home-page">
                        <img src="/assets/images/home.svg" alt="" style="width: 18px;margin-left: 6px;opacity: 0.8 ">
                        صفحه اصلی
                    </a>
                    <a href="/register" class="home-page mt-3">
                        ثبت نام
                    </a>
                </div>
                <div class="pt-5">
                    <div class="col-md-12 px-5">
                        <h3 class="text-end a-h3" style="font-weight: bold">ورود</h3>
                        <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                               v-model="form.mobile" type="text"
                               placeholder="شماره همراه"
                               style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                        <input id="auto-focus" class="text-end col-12 mt-2 mx-auto a-input"
                               v-model="form.password" type="password"
                               placeholder="کلمه عبور"
                               style="border-color: #ddd;padding: 15px !important;font-size: 14px;border-radius: 8px">
                    </div>
                </div>
                <div class="px-5">
                    <div class="text-left mt-4 mx-auto d-block">
                        <a class="a-btn" @click="login()">
                            ورود
                            <img src="/assets/images/left-arrow-white.svg" alt=""
                                 style="width: 10px;margin: 0px 8px 2px 0px;">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let vm = new Vue({
        el: '#area',
        data: {
            flag1: 1,
            flag2: 0,
            flag3: 0,
            flag4: 0,
            flagCounter: 0,
            form: {
                mobile: '',
                password: '',
            },
            error: '',
            code: null,
            userCode: null,
            newPass: null,
            reNewPass: null,
            countdown: 30,
        },
        methods: {
            login() {
                if (this.form.mobile === '' || this.form.password === '') {
                    alert('موبایل و کلمه عبور را وارد کنید.')
                    return
                }

                if (this.form.mobile.length !== 11) {
                    alert('شماره موبایل را بصورت صحیح وارد کنید.')
                    return
                }

                axios.post('/login/store', {
                    mobile: this.form.mobile,
                    password: this.form.password,
                }).then(res => {
                    if (res.data.key === 'not match') {
                        vm.error = 'موبایل یا کلمه عبور صحیح نمی باشند.'
                        return
                    }
                    window.location.href = '/shipping'
                })
            },
            forgetPass() {
                this.flag1 = 0
                this.flag2 = 1
            },
            back() {
                this.flag1 = 1
                this.flag2 = 0
            },
            back2() {
                this.flag2 = 1
                this.flag3 = 0
            },
            sendCode() {
                if (this.form.mobile === '') {
                    alert('شماره موبایل خود را وارد کنید.')
                    return
                }
                if (this.form.mobile.length !== 11) {
                    alert('شماره موبایل را بصورت صحیح وارد کنید.')
                    return
                }

                axios.post('/login/send/code', {
                    mobile: this.form.mobile,
                }).then(res => {
                    console.log(res.data)
                    vm.code = res.data
                    vm.flag2 = 0
                    vm.flag3 = 1
                    vm.flagCounter = 1

                    let counter = setInterval(() => {
                        vm.countdown--
                    }, 1000)
                    setTimeout(() => {
                        clearInterval(counter)
                        vm.flagCounter = 0
                    }, 30000)
                })
            },
            checkCode() {
                if (this.userCode == null) {
                    alert('لطفا کد پیامک شده را وارد نمایید.')
                    return
                }
                vm.flag3 = 0
                vm.flag4 = 1
            },
            submitNewPass() {
                if (this.newPass == null || this.reNewPass == null) {
                    alert('کلمه عبور را وارد کنید.')
                    return
                }

                if (this.newPass.length < 4) {
                    alert('کلمه عبور نباید کمتر از 4 کاراکتر باشد.')
                    return
                }

                if (this.newPass !== this.reNewPass) {
                    alert('کلمه عبور با تکرار آن برابر نمی باشد.')
                    return
                }



                axios.post('/new/password/store', {
                    mobile: this.form.mobile,
                    new_pass: this.newPass,
                }).then(() => {
                    window.location.href = '/shipping'
                })
            },
        }
    })
</script>

</body>
</html>
