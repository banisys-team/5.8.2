<?php

Route::prefix('admin')->group(function () {
    Route::get('login', 'Auth\Admin\LoginController@login')->name('admin.auth.login');
    Route::post('login', 'Auth\Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
    Route::post('logout', 'Auth\Admin\LoginController@logout')->name('admin.auth.logout');
});

Route::prefix('admin')->middleware('auth:admin')->namespace('Admin')->name('admin.')->group(function () {
    Route::get('register', 'AdminController@create')->name('register');
    Route::post('register', 'AdminController@store')->name('store');

    Route::get('/banisys', 'AdminController@dashboard');

//  ==================================== Permission =============================
    Route::get('/permission/create', 'PermissionController@create');
    Route::post('/permission/store', 'PermissionController@store');
    Route::get('/permission/fetch', 'PermissionController@fetch');
    Route::get('/permission/delete/{id}', 'PermissionController@delete');
    Route::get('/permission/search', 'PermissionController@search');

//  ==================================== Role ====================================
    Route::get('/role/create', 'RoleController@create');
    Route::post('/role/store', 'RoleController@store');
    Route::get('/role/fetch', 'RoleController@fetch');
    Route::get('/role/delete/{id}', 'RoleController@delete');
    Route::get('/role/search', 'RoleController@search');
    Route::get('/role/getPermission', 'RoleController@getPermission');
    Route::get('/role/fetchPermission/{id}', 'RoleController@fetchPermission');
    Route::get('/get-roles', 'AdminController@getRoles');
    Route::get('/fetch', 'AdminController@fetch');
    Route::get('/search', 'AdminController@search');
    Route::get('/fetchRole/{id}', 'AdminController@fetchRole');
    Route::get('/delete/{id}', 'AdminController@delete');

//  ==================================== Product ====================================
    Route::view('/product/index', 'admin.product.index');
    Route::view('/product/edit/{id}', 'admin.product.edit');
    Route::get('/product/a/search/name', 'ProductController@searchName');
    Route::get('/product/fetch/cat/id/{name}', 'ProductController@getCatId');
    Route::post('/product/edit/price', 'ProductController@editPrice');
    Route::view('/product/create', 'admin.product.create');
    Route::get('/product/fetch/cat/root', 'ProductController@fetchRootCat');
    Route::get('/product/fetch/brand/all', 'ProductController@fetchAll');
    Route::get('/product/search/brand/{brand}', 'ProductController@searchBrand');
    Route::post('/product/store', 'ProductController@store');
    Route::get('/product/all/fetch', 'ProductController@fetch');
    Route::get('/product/all/search', 'ProductController@search');
    Route::get('/product/fetch/{id}', 'ProductController@fetchProduct');
    Route::get('/product/fetch/brands/{product_id}', 'ProductController@fetchBrands');
    Route::get('/product/fetch/gallery/{pro}', 'ProductController@fetchGallery');
    Route::get('/product/specifications/fetch/{product_id}', 'ProductController@specificationsFetch');
    Route::get('/product/specifications/delete/{id}', 'ProductController@specificationsDelete');
    Route::get('/product/colors/fetch/{product_id}', 'ProductController@colorsFetch');
    Route::get('/product/sizes/fetch/{product_id}', 'ProductController@sizesFetch');
    Route::post('/product/update/{id}', 'ProductController@update');
    Route::get('/product/delete/gallery/{gallery_id}', 'ProductController@deleteGallery');
    Route::get('/product/color/delete/{color_id}', 'ProductController@colorDelete');
    Route::get('/product/size/delete/{size_id}', 'ProductController@sizeDelete');
    Route::delete('/product/delete/{product_id}', 'ProductController@delete');
    Route::view('/product/suggest/index', 'admin.product.suggest');
    Route::get('/product/suggests/fetch', 'ProductController@suggestFetch');
    Route::get('/product/suggest/add/{product_id}', 'ProductController@suggestAdd');
    Route::get('/product/suggest/delete/{id}', 'ProductController@suggestDelete');
    Route::get('/product/suggest/search/name', 'ProductController@SuggestSearchName');
    Route::get('/product/suggest/timer/fetch', 'ProductController@SuggestTimerFetch');
    Route::get('/product/suggests/timer/store/{time}', 'ProductController@SuggestTimeStore');
    Route::post('/product/fetch/breadcrumb', 'ProductController@breadcrumb');
    Route::post('/product/priority/update', 'ProductController@priorityUpdate');
    Route::get('/product/recharge/fetch', 'ProductController@rechargeFetch');

//  ==================================== Specification ====================================
    Route::get('/specification/create', 'SpecificationController@create');
    Route::get('/specification/catspec/{name}', 'SpecificationController@fechCatspec');
    Route::post('/specification/store', 'SpecificationController@store');
    Route::get('/specification/fetch', 'SpecificationController@fetch');
    Route::get('/specification/search', 'SpecificationController@search');
    Route::get('/specification/delete/{id}', 'SpecificationController@delete');


//  ==================================== Brand ====================================
    Route::post('/brand/store', 'BrandController@store');
    Route::get('/brand/fetch/all', 'BrandController@fetchAll');
    Route::get('/brand/fetch/{cat}', 'BrandController@fetchBrandCat');
    Route::get('/brand/image/{id}', 'BrandController@image');
    Route::get('/brand/description/{id}', 'BrandController@description');
    Route::post('/brand/edit/cat', 'BrandController@editCat');
    Route::get('/brand/fetch/cat/child/{id}', 'BrandController@fetchRootChild');
    Route::get('/brand/fetch/cat/root', 'BrandController@fetchRootCat');

    Route::view('/brand', 'admin.brand.crud');
    Route::get('/brand/fetch', 'BrandController@fetch');
    Route::post('/brand/search', 'BrandController@searchName');
    Route::get('/brand/delete/{id}', 'BrandController@delete');

//  ==================================== Category ====================================
    Route::view('/category/create', 'admin.category.create');
    Route::get('/category/search', 'CategoryController@search');
    Route::post('/category/update', 'CategoryController@update');
    Route::get('/category/fetch/cat/root/{type}', 'CategoryController@fetchRootCat');
    Route::get('/category/fetch/cat/child/{id}/{type}', 'CategoryController@fetchRootChild');
    Route::get('category/search/brand/{brand}', 'CategoryController@searchBrand');

    Route::view('/category', 'admin.category.crud');
    Route::get('/category/root/fetch', 'CategoryController@rootFetch');
    Route::post('/category/store', 'CategoryController@store');
    Route::get('/category/fetch', 'CategoryController@fetch');
    Route::get('/category/fetch/list', 'CategoryController@fetchList');
    Route::post('/category/edit', 'CategoryController@edit');
    Route::post('/category/priority/edit', 'CategoryController@priorityEdit');
    Route::get('/category/delete/{catId}', 'CategoryController@delete');

//  ==================================== Order ====================================
    Route::get('/order/index', 'OrderController@index');
    Route::get('/order/fetch', 'OrderController@fetch');
    Route::post('/change/status/{id}', 'OrderController@changeStatus');
    Route::get('/order/search', 'OrderController@search');
    Route::get('/order/delete/{id}', 'OrderController@delete');
    Route::get('/fetch/order/{order_id}', 'OrderController@fetchOrder');
    Route::get('/factor/sum/value/{id}', 'OrderController@fetchOrderValue');
    Route::get('/factor/confirm/{id}', 'OrderController@factorConfirm');
    Route::get('/order/not-confirm/fetch', 'OrderController@fetchNotConfirm');
    Route::get('/order/confirm/fetch', 'OrderController@fetchConfirm');
    Route::get('/order/note/fetch/{order_id}', 'OrderController@noteFetch');
    Route::post('/order/note/store', 'OrderController@noteStore');
    Route::post('/order/post/track/store', 'OrderController@postTrackStore');
    Route::get('/order/post/track/fetch/{order_id}', 'OrderController@postTrackFetch');

//  ==================================== Exist ====================================
    Route::get('/exist/index', 'ExistController@index');
    Route::get('/exist/set/{id}', 'ExistController@set');
    Route::get('/exist/fetch/colors/{id}', 'ExistController@fetchColors');
    Route::get('/exist/fetch/effects/{id}', 'ExistController@fetchEffects');
    Route::get('/exist/get/slug/{id}', 'ExistController@getSlug');
    Route::post('/exist/store/num', 'ExistController@storeNum');
    Route::get('/exist/fetch/{id}', 'ExistController@fetchExist');
    Route::get('/exist/delete/{id}', 'ExistController@delete');
    Route::get('/exist/product/code', 'ExistController@productCode');
    Route::post('/exist/search/product/code', 'ExistController@productCodeSearch');
    Route::post('/exist/product/code/change/num', 'ExistController@changeNum');
    Route::get('/exist/fetch/effect/{id}', 'ExistController@fetchEffect');
    Route::get('/exist/fetch/effect/price/{id}', 'ExistController@fetchEffectPrice');
    Route::get('/exist/fetch/effect/spec/{id}', 'ExistController@fetchEffectSpec');

    Route::get('/exist/fetch/color/{product_id}', 'ExistController@colorsFetch');
    Route::get('/exist/fetch/sizes/{product_id}', 'ExistController@sizesFetch');

//  ==================================== Slider ====================================
    Route::view('/slider/crud', 'admin.slider.crud');
    Route::post('/slider/product/store', 'IndexController@sliderProductStore');
    Route::get('/slider/product/fetch', 'IndexController@sliderProductFetch');
    Route::get('/slider/product/delete/{key_id}', 'IndexController@sliderProductDelete');
    Route::get('/slider/create', 'SliderController@create');
    Route::post('/slider/store1', 'SliderController@store1');
    Route::post('/slider/store2', 'SliderController@store2');
    Route::get('/slider/fetch/slide', 'SliderController@slideFetch');
    Route::get('/slider/delete/{id}', 'SliderController@delete');


//  ==================================== Complaint ====================================
    Route::get('/complaint/index', 'IndexController@showComplaint');
    Route::get('/complaint/fetch', 'IndexController@fetchComplaint');
    Route::get('/complaint/ticket/{id}', 'IndexController@showTicketComplaint');

//  ==================================== Reserve ====================================
    Route::get('/order/reserve/users', 'OrderController@userReserve');
    Route::get('/order/reserve/users/fetch', 'OrderController@userReserveFetch');
    Route::post('/order/reserve/change/status/{id}', 'OrderController@changeReserveStatus');
    Route::get('/order/reserve/history', 'OrderController@historyReserve');
    Route::post('/order/reserve/search/mobile', 'OrderController@reserveSearch');

//  ==================================== Track ====================================
    Route::get('/track/index', 'TrackController@index');
    Route::post('/track/store', 'TrackController@store');
    Route::get('/track/fetch/all', 'TrackController@fetchAll');
    Route::delete('/track/delete/{id}', 'TrackController@delete');

//  ==================================== Setting ====================================
    Route::get('/setting', 'SettingController@crud');
    Route::get('/setting/fetch', 'SettingController@fetch');
    Route::post('/setting/store', 'SettingController@store');
    Route::get('/setting/delivery', 'SettingController@deliveryShow');
    Route::get('/setting/delivery/fetch', 'SettingController@deliveryFetch');
    Route::post('/setting/delivery/store', 'SettingController@deliveryStore');

//  ==================================== User ====================================
    Route::get('/user/crud', 'UserController@crud');
    Route::get('/user/fetch', 'UserController@fetch');
    Route::post('/user/store', 'UserController@store');
    Route::get('/user/delete/{user_id}', 'UserController@delete');

//  ==================================== Page ====================================
    Route::get('/page/edit', 'PageController@edit');
    Route::get('/page/fetch', 'PageController@fetch');
    Route::post('/page/update', 'PageController@update');

});

//  ==================================== Other =================================
Route::get('/factor/{id}', 'Admin\OrderController@factor')->middleware('factor');
Route::get('/admin/fetch/order/value/{id}', 'Admin\OrderController@fetchOrderValue');
Route::get('/admin/order/sum/price/{id}', 'Admin\OrderController@sumPrice');
Route::get('/admin/order/fetch/number/{id}', 'Admin\OrderController@fetchNumber');

//  ==================================== Index ====================================
Route::get('/admin/index/create', 'Admin\IndexController@indexCrud');
Route::post('/admin/index/image/store', 'Admin\IndexController@imageStore');
Route::post('/admin/index/url/store', 'Admin\IndexController@urlStore');
Route::get('/admin/index/fetch/image', 'Admin\IndexController@fetchImage');

Route::get('/admin/product/fetch/cat/child/{id}', 'Admin\ProductController@fetchRootChild');
