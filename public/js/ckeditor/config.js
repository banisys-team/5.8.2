/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    config.toolbarGroups = [
        // {name: 'clipboard', groups: ['clipboard', 'undo']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
        // {name: 'document', groups: ['mode', 'document', 'doctools']},
        // {name: 'forms', groups: ['forms']},
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'paragraph', groups: ['list', 'blocks', 'align', 'bidi']},
        {name: 'insert', groups: ['insert']},
        {name: 'colors', groups: ['colors']},
        // {name: 'tools', groups: ['tools']},
        // {name: 'others', groups: ['others']},
        {name: 'about', groups: ['about']},
    ];

    config.removeButtons =
        'About,Maximize,Image,SpecialChar,HorizontalRule,Blockquote,Superscript,Subscript,Strike,Underline,Italic,Scayt';
    CKEDITOR.config.contentsLangDirection = 'rtl';

    config.resize_enabled = true;
    config.resize_dir = 'both';
    config.resize_minWidth = 10;

    // config.extraPlugins = 'maximize,justify,colorbutton,colordialog,div,bidi,font,maximize,lineheight';
    config.extraPlugins = 'justify,colorbutton,colordialog';
    config.defaultLanguage = 'fa';
    config.language = 'fa';

};
